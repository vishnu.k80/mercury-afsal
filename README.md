# MercuryAccessFrontend

## Install Dependencies

```bash
# install dependencies
npm i
```

## Commands

```bash
# start dev server
ng serve --open

# build in proudction mode
ng build --prod

# lint
ng lint

# lint with fixing
ng lint --fix

# create mock data
# data already created in `src/assets/json` directory
# so do not create mock data if unnecessary
npm run data:create
```

## JSON

All json files are placed in `src/assets/json` directory.

## Environments

- `api.host`: Api host url
- `api.delay`: Api delay for services. It should be used for development mode only.

## Constants

There are some constants in the `src/app/utils/constants.util.ts`.

## Login Users

The Login User data is `src/assets/json/users.json`.\
The password for the users is `password`.

- `mark_code@example.com`

  - Can read/edit all sections
  - Has toolkit

- `lord_code@example.com`

  - Can read all section except Access History
  - Doesn't have toolkit

- `lord_johnson@example.com`
  - Cannot read/edit all sections
  - Doesn't have toolkit

## Verification

See `docs/verification.md`.
