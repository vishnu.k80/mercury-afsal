/**
 * get random number within specific range
 * @param min min number
 * @param max max number
 */
function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

/**
 * pick random item from array
 * @param items random item candidates
 */
function randomPick(items) {
  return items[randomNumber(0, items.length)];
}

/**
 * return random key
 */
function randomKey() {
  return Math.random().toString(32).split('.')[1];
}

/**
 * return random date
 */
function randomDate(start, end) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

/**
 * return random single number
 */
function randomSingleNumber() {
  return randomPick([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
}

/**
 * return random pin number
 */
function randomPinNumber() {
  return [randomSingleNumber(), randomSingleNumber(), randomSingleNumber(), randomSingleNumber()].join('');
}

/**
 * return random first name
 */
function randomFirstName() {
  return randomPick([
    'Mary',
    'Jane',
    'James',
    'John',
    'Grey',
    'Bill',
    'Will',
    'Mike',
    'Chris',
    'Owen',
    'Luis',
    'Prince',
    'Lord',
    'Yoda',
    'Fabian',
    'Fernando',
    'Tony',
    'Anthony',
    'Mark',
    'Usher',
    'Alberto'
  ]);
}

/**
 * return random last name
 */
function randomLastName() {
  return randomPick([
    'Right',
    'Reyes',
    'Willis',
    'Ramirez',
    'Sullivan',
    'Gates',
    'Reid',
    'Junior',
    'Smith',
    'Code',
    'Johnson',
    'Jackson',
    'Allen',
    'Young',
    'Hernandez',
    'King',
    'Lopez',
    'Adams',
    'Nelson',
    'Mitchell',
    'Turner'
  ]);
}

/**
 * return random activity
 */
function randomActivity() {
  return randomPick(['Removed Keyholder SN: K042012', 'Edited Space ID: S042076', 'Registered new Device SN: D042072']);
}

module.exports = {
  randomPick,
  randomDate,
  randomNumber,
  randomSingleNumber,
  randomKey,
  randomPinNumber,
  randomFirstName,
  randomLastName,
  randomActivity
};
