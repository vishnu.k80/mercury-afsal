const { randomKey } = require('./random.util');

const locationCounts = 5;
const locationStartId = 10492;

const locations = [];

for (let i = 0; i < locationCounts; i++) {
  const location = {
    id: 'L0' + (locationStartId + i),
    location: randomKey().substr(0, 3)
  };

  locations.push(location);
}

module.exports = {
  locations
};
