import { createAction, props } from '@ngrx/store';
import { AppLocation } from '../../models/data/app-location';
import { AppUser } from '../../models/data/app-user';
import { AppStatus } from '../../models/data/app-status';

export const AppStoreKey = 'AppStore';

// actions
export const appStoreActions = {
  // set user
  setUser: createAction(`${AppStoreKey}:setUser`, props<{ user: AppUser }>()),
  // set location
  setLocation: createAction(`${AppStoreKey}:setLocation`, props<{ location: AppLocation }>()),
  // set locations
  setLocations: createAction(`${AppStoreKey}:setLocations`, props<{ locations: AppLocation[] }>()),
  // set statuses
  setStatuses: createAction(`${AppStoreKey}:setStatuses`, props<{ statuses: AppStatus[] }>())
};
