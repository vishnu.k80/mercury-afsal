import { AppLocation } from '../../models/data/app-location';
import { Action, createReducer, on } from '@ngrx/store';
import { appStoreActions } from './app-store.actions';
import { AppAdmin } from '../../models/data/app-user';
import { AppStatus } from '../../models/data/app-status';

export interface AppStore {
  user: AppAdmin;
  location: AppLocation;
  locations: AppLocation[];
  statuses: AppStatus[];
}

// init state of app store
export const appStoreInit: AppStore = {
  user: null,
  location: null,
  locations: [],
  statuses: []
};

const reducer = createReducer(
  appStoreInit,
  on(appStoreActions.setUser, (state, { user }) => {
    return {
      ...state,
      user
    };
  }),
  on(appStoreActions.setLocation, (state, { location }) => {
    return {
      ...state,
      location
    };
  }),
  // set first location as selected when locations are loaded
  on(appStoreActions.setLocations, (state, { locations }) => {
    return {
      ...state,
      location: locations[0],
      locations
    };
  }),
  on(appStoreActions.setStatuses, (state, { statuses }) => {
    return {
      ...state,
      statuses
    };
  })
);

export function appReducer(state: AppStore, action: Action) {
  return reducer(state, action);
}
