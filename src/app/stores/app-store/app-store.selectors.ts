import { createSelector } from '@ngrx/store';
import { AppStoreKey } from './app-store.actions';
import { AppStore } from './app-store.reducer';

export const getAppStore = (state) => state[AppStoreKey];

// selectors
export const appStoreSelectors = {
  // get user
  user: createSelector(getAppStore, (state: AppStore) => state.user),
  // get location
  location: createSelector(getAppStore, (state: AppStore) => state.location),
  // get locations
  locations: createSelector(getAppStore, (state: AppStore) => state.locations),
  // get statuses
  statuses: createSelector(getAppStore, (state: AppStore) => state.statuses),
  // admin edit permission
  adminEditPermission: createSelector(getAppStore, (state: AppStore) => {
    if (state.user) {
      return state.user.permissions.administrators.edit;
    }
  }),
  // device edit permission
  deviceEditPermission: createSelector(getAppStore, (state: AppStore) => {
    if (state.user) {
      return state.user.permissions.devices.edit;
    }
  }),
  // config edit permission
  configEditPermission: createSelector(getAppStore, (state: AppStore) => {
    if (state.user) {
      return state.user.permissions.configurations.edit;
    }
  }),
  // keyholder edit permission
  keyholderEditPermission: createSelector(getAppStore, (state: AppStore) => {
    if (state.user) {
      return state.user.permissions.keyholders.edit;
    }
  }),
  // access history read permission
  accessHistoryReadPermission: createSelector(getAppStore, (state: AppStore) => {
    if (state.user) {
      return state.user.permissions.accessHistory.read;
    }
  }),
  // space edit permission
  spaceEditPermission: createSelector(getAppStore, (state: AppStore) => {
    if (state.user) {
      return state.user.permissions.spaces.edit;
    }
  })
};
