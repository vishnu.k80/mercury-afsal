import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaptchaComponent } from './captcha.component';
import { IconReloadModule } from '../../../common/icons/icon-reload/icon-reload.module';

@NgModule({
  declarations: [CaptchaComponent],
  exports: [CaptchaComponent],
  imports: [CommonModule, IconReloadModule]
})
export class CaptchaModule {}
