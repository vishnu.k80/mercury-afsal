import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecoveryEmailSentComponent } from './recovery-email-sent.component';
import { CheckboxModule } from '../../../common/checkbox/checkbox.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { CardModule } from '../../../common/card/card.module';
import { RouterModule } from '@angular/router';
import { IconVerifiedModule } from '../../../common/icons/icon-verified/icon-verified.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { LogoModule } from 'src/app/components/common/logo/logo.module';

@NgModule({
  declarations: [RecoveryEmailSentComponent],
  exports: [RecoveryEmailSentComponent],
  imports: [
    CommonModule,
    CardModule,
    CheckboxModule,
    FlatButtonModule,
    RouterModule,
    IconVerifiedModule,
    SpinnerModule,
    LogoModule
  ]
})
export class RecoveryEmailSentModule {}
