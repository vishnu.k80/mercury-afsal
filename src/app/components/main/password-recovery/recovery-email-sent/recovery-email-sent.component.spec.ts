import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoveryEmailSentComponent } from './recovery-email-sent.component';

describe('RecoveryEmailSentComponent', () => {
  let component: RecoveryEmailSentComponent;
  let fixture: ComponentFixture<RecoveryEmailSentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RecoveryEmailSentComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoveryEmailSentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
