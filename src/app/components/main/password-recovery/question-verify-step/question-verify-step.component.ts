import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { UserService } from '../../../../services/user.service';
import { MessageService } from '../../../common/message/services/message.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../utils/subscribe.util';

interface QuestionVerifyStepSubscriptions extends SubscriptionKeys {
  verifySecretAnswer: SubscriptionItem;
}

@Component({
  selector: 'app-question-verify-step',
  templateUrl: './question-verify-step.component.html',
  styleUrls: ['../common/password-recovery-base.component.scss', './question-verify-step.component.scss']
})
export class QuestionVerifyStepComponent implements OnInit, OnDestroy {
  // set secret question
  @Input() set secretQuestion(question: string) {
    this.question.setValue(question);
  }
  // user email
  @Input() email: string;
  // back button click emitter
  @Output() backClick: EventEmitter<void> = new EventEmitter<void>();
  // verified emitter
  @Output() verified: EventEmitter<void> = new EventEmitter<void>();
  // loading state
  loading = false;
  // question
  question: FormControl = new FormControl('');
  // answer
  answer: FormControl = new FormControl('', Validators.required);
  // answer errors
  answerErrors = {
    required: 'The answer is required'
  };
  // inventory
  private inventory: SubscriptionInventory<QuestionVerifyStepSubscriptions> = new SubscriptionInventory<
    QuestionVerifyStepSubscriptions
  >();

  constructor(private userService: UserService, private messageService: MessageService) {}

  ngOnInit(): void {
    this.question.disable();
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * emit back button click
   */
  emitBackClick() {
    this.backClick.emit();
  }

  /**
   * verify secret answer
   */
  verifySecretAnswer() {
    // check validation
    this.answer.markAsTouched();

    if (this.answer.invalid) {
      return;
    }

    this.inventory.unSubscribe('verifySecretAnswer');
    this.loading = true;

    const sub = this.userService.verifyUserAnswer(this.email, this.answer.value).subscribe({
      next: () => {
        this.emitVerified();
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('verifySecretAnswer', sub);
  }

  /**
   * emit verified
   */
  emitVerified() {
    this.verified.emit();
  }
}
