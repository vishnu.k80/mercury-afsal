import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionVerifyStepComponent } from './question-verify-step.component';
import { CardModule } from '../../../common/card/card.module';
import { IconArrowLeftModule } from '../../../common/icons/icon-arrow-left/icon-arrow-left.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { CharacterIconFieldModule } from '../../../common/materialized-fields/character-icon-field/character-icon-field.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { FormNeutralizerModule } from '../../../common/form-neutralizer/form-neutralizer.module';
import { LogoModule } from 'src/app/components/common/logo/logo.module';

@NgModule({
  declarations: [QuestionVerifyStepComponent],
  exports: [QuestionVerifyStepComponent],
  imports: [
    CommonModule,
    CardModule,
    IconArrowLeftModule,
    FlatButtonModule,
    CharacterIconFieldModule,
    SpinnerModule,
    FormNeutralizerModule,
    LogoModule
  ]
})
export class QuestionVerifyStepModule {}
