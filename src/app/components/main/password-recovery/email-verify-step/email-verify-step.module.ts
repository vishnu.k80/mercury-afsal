import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailVerifyStepComponent } from './email-verify-step.component';
import { CardModule } from '../../../common/card/card.module';
import { IconArrowLeftModule } from '../../../common/icons/icon-arrow-left/icon-arrow-left.module';
import { EmailIdFieldModule } from '../../../common/materialized-fields/email-id-field/email-id-field.module';
import { CheckboxModule } from '../../../common/checkbox/checkbox.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { RouterModule } from '@angular/router';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { FormNeutralizerModule } from '../../../common/form-neutralizer/form-neutralizer.module';
import { LogoModule } from 'src/app/components/common/logo/logo.module';

@NgModule({
  declarations: [EmailVerifyStepComponent],
  exports: [EmailVerifyStepComponent],
  imports: [
    CommonModule,
    CardModule,
    IconArrowLeftModule,
    EmailIdFieldModule,
    CheckboxModule,
    FlatButtonModule,
    RouterModule,
    SpinnerModule,
    FormNeutralizerModule,
    LogoModule
  ]
})
export class EmailVerifyStepModule {}
