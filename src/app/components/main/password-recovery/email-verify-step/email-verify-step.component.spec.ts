import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailVerifyStepComponent } from './email-verify-step.component';

describe('EmailVerifyStepComponent', () => {
  let component: EmailVerifyStepComponent;
  let fixture: ComponentFixture<EmailVerifyStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmailVerifyStepComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailVerifyStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
