import { Component } from '@angular/core';
import { ModalBaseComponent } from '../common/modal-base.component';

@Component({
  selector: 'app-modal-header',
  templateUrl: './modal-header.component.html',
  styleUrls: ['./modal-header.component.scss']
})
export class ModalHeaderComponent extends ModalBaseComponent<null> {}
