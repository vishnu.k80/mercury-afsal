import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal.component';
import { ModalHeaderComponent } from './modal-header/modal-header.component';
import { ModalBodyComponent } from './modal-body/modal-body.component';
import { ModalFooterComponent } from './modal-footer/modal-footer.component';
import { IconCloseModule } from '../icons/icon-close/icon-close.module';

@NgModule({
  declarations: [ModalComponent, ModalHeaderComponent, ModalBodyComponent, ModalFooterComponent],
  exports: [ModalComponent, ModalHeaderComponent, ModalBodyComponent, ModalFooterComponent],
  imports: [CommonModule, IconCloseModule]
})
export class ModalModule {}
