import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { OptionItem } from '../../../models/common/option-item';
import { AppUserPermissions, AppUserPermissionType } from '../../../models/data/app-user-permissions';
import { AppUser } from '../../../models/data/app-user';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent implements OnInit {
  // permission
  @Input() permissions: AppUserPermissions;
  // label for permissions
  @Input() label: string;
  // readonly state
  @Input() @HostBinding('class.readonly') readOnly = false;
  // permission
  permissionList: OptionItem<keyof AppUserPermissions>[] = [
    new OptionItem<keyof AppUserPermissions>('Administrators', 'administrators'),
    new OptionItem<keyof AppUserPermissions>('Keyholders', 'keyholders'),
    new OptionItem<keyof AppUserPermissions>('Devices', 'devices'),
    new OptionItem<keyof AppUserPermissions>('Spaces', 'spaces'),
    new OptionItem<keyof AppUserPermissions>('Access History', 'accessHistory'),
    new OptionItem<keyof AppUserPermissions>('Configuration', 'configurations')
  ];

  constructor() {}

  ngOnInit(): void {}

  /**
   * change permission
   * @param key permission key
   * @param type permission type (read | edit)
   */
  changePermission(key: keyof AppUserPermissions, type: AppUserPermissionType) {
    if (!this.readOnly) {
      if (type === 'read' && this.permissions[key].read) {
        this.permissions[key].read = false;
        this.permissions[key].edit = false;
      } else if (type === 'edit' && !this.permissions[key].edit) {
        this.permissions[key].read = true;
        this.permissions[key].edit = true;
      } else {
        this.permissions[key][type] = !this.permissions[key][type];
      }
    }
  }
}
