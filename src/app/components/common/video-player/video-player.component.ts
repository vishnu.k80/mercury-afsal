import { Component, ElementRef, HostBinding, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { getElement } from '../../../utils/element.util';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit {
  // video source url
  @Input() set url(url: string) {
    this.safeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
  }
  // width
  @Input() width: number;
  // height
  @Input() height: number;
  // video ref
  @ViewChild('video') videoRef: ElementRef<HTMLVideoElement>;
  // host width
  @HostBinding('style.width') get hostWidth() {
    return this.width ? this.width + 'px' : null;
  }
  // host height
  @HostBinding('style.height') get hostHeight() {
    return this.height ? this.height + 'px' : null;
  }

  // safe url
  safeUrl: SafeUrl;

  constructor(private domSanitizer: DomSanitizer) {}

  ngOnInit(): void {}

  @HostListener('click')
  onHostClicked() {
    this.toggleVideoPlay();
  }

  /**
   * toggle video play state
   */
  private toggleVideoPlay() {
    const video = getElement(this.videoRef);

    if (video) {
      if (video.paused) {
        video.play().catch((e) => console.error(e.message));
      } else {
        video.pause();
      }
    }
  }
}
