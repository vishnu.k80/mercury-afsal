import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card.component';
import { CardTitleComponent } from './card-title/card-title.component';
import { CardSubTitleComponent } from './card-sub-title/card-sub-title.component';

@NgModule({
  declarations: [CardComponent, CardTitleComponent, CardSubTitleComponent],
  exports: [CardComponent, CardTitleComponent, CardSubTitleComponent],
  imports: [CommonModule]
})
export class CardModule {}
