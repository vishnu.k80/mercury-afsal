import { Injectable } from '@angular/core';

export type MessageType = 'default' | 'error';

export interface MessageItem {
  type: MessageType;
  content: string;
  undoable?: boolean;
  onUndo?: () => void;
}

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  // message list
  private static list: MessageItem[] = [];

  /**
   * get messages
   */
  get messages(): MessageItem[] {
    return MessageService.list;
  }

  /**
   * set messages
   * @param list message list
   */
  set messages(list: MessageItem[]) {
    MessageService.list = list;
  }

  /**
   * open new message
   * @param type message type
   * @param content content
   * @param undoable undoable
   * @param onUndo callback for undo
   */
  open(type: MessageType, content: string, undoable?: boolean, onUndo?: () => void) {
    const list = this.messages;

    // add new item on first
    list.splice(0, 0, {
      type,
      content,
      undoable,
      onUndo
    });

    // remove after 1
    list.splice(1);
  }

  /**
   * close specific message
   * @param message message
   */
  close(message: MessageItem) {
    const list = MessageService.list;

    this.messages = list.filter((item) => item !== message);
  }
}
