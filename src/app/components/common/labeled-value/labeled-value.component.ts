import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-labeled-value',
  templateUrl: './labeled-value.component.html',
  styleUrls: ['./labeled-value.component.scss']
})
export class LabeledValueComponent implements OnInit {
  // label for field
  @Input() label: string;

  constructor() {}

  ngOnInit(): void {}
}
