import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})
export class RadioComponent implements OnInit {
  // checked state
  @Input() @HostBinding('class.checked') checked = false;

  constructor() {}

  ngOnInit(): void {}
}
