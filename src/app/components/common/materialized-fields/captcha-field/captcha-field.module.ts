import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaptchaFieldComponent } from './captcha-field.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FieldErrorModule } from '../field-error/field-error.module';
import { IconShieldModule } from '../../icons/icon-shield/icon-shield.module';

@NgModule({
  declarations: [CaptchaFieldComponent],
  exports: [CaptchaFieldComponent],
  imports: [CommonModule, ReactiveFormsModule, FieldErrorModule, IconShieldModule]
})
export class CaptchaFieldModule {}
