import { Component, OnInit } from '@angular/core';
import { MaterializedFieldBaseComponent } from '../common/materialized-field-base.component';

@Component({
  selector: 'app-captcha-field',
  templateUrl: './captcha-field.component.html',
  styleUrls: ['../common/materialized-field-base.component.scss', './captcha-field.component.scss']
})
export class CaptchaFieldComponent extends MaterializedFieldBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
