import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterIconFieldComponent } from './character-icon-field.component';

describe('CharacterIconFieldComponent', () => {
  let component: CharacterIconFieldComponent;
  let fixture: ComponentFixture<CharacterIconFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CharacterIconFieldComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterIconFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
