import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharacterIconFieldComponent } from './character-icon-field.component';
import { FieldErrorModule } from '../field-error/field-error.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CharacterIconFieldComponent],
  exports: [CharacterIconFieldComponent],
  imports: [CommonModule, FieldErrorModule, ReactiveFormsModule]
})
export class CharacterIconFieldModule {}
