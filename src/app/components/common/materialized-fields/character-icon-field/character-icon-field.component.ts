import { Component, Input, OnInit } from '@angular/core';
import { MaterializedFieldBaseComponent } from '../common/materialized-field-base.component';

@Component({
  selector: 'app-character-icon-field',
  templateUrl: './character-icon-field.component.html',
  styleUrls: ['../common/materialized-field-base.component.scss', './character-icon-field.component.scss']
})
export class CharacterIconFieldComponent extends MaterializedFieldBaseComponent implements OnInit {
  // character for icon
  @Input() character: string;

  constructor() {
    super();
  }

  ngOnInit(): void {}
}
