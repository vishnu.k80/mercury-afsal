import { Component, OnInit } from '@angular/core';
import { MaterializedFieldBaseComponent } from '../common/materialized-field-base.component';

@Component({
  selector: 'app-email-id-field',
  templateUrl: './email-id-field.component.html',
  styleUrls: ['../common/materialized-field-base.component.scss', './email-id-field.component.scss']
})
export class EmailIdFieldComponent extends MaterializedFieldBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
