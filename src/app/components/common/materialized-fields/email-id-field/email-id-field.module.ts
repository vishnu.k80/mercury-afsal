import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailIdFieldComponent } from './email-id-field.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FieldErrorModule } from '../field-error/field-error.module';
import { IconEmailModule } from '../../icons/icon-email/icon-email.module';

@NgModule({
  declarations: [EmailIdFieldComponent],
  exports: [EmailIdFieldComponent],
  imports: [CommonModule, ReactiveFormsModule, FieldErrorModule, IconEmailModule]
})
export class EmailIdFieldModule {}
