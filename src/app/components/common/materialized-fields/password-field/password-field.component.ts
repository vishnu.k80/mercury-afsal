import { Component, OnInit } from '@angular/core';
import { MaterializedFieldBaseComponent } from '../common/materialized-field-base.component';

@Component({
  selector: 'app-password-field',
  templateUrl: './password-field.component.html',
  styleUrls: ['../common/materialized-field-base.component.scss', './password-field.component.scss']
})
export class PasswordFieldComponent extends MaterializedFieldBaseComponent implements OnInit {
  // password visible state
  passwordVisible = false;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  /**
   * return input type
   */
  get type() {
    return this.passwordVisible ? 'text' : 'password';
  }

  /**
   * toggle password visible state
   */
  togglePasswordVisible() {
    this.passwordVisible = !this.passwordVisible;
  }
}
