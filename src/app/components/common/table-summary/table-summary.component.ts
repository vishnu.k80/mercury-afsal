import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-table-summary',
  templateUrl: './table-summary.component.html',
  styleUrls: ['./table-summary.component.scss']
})
export class TableSummaryComponent implements OnInit {
  // menu opened state
  direction = true;
  // title
  @Input() title: string;
  // sub title
  @Input() subTitle: string;
  // total
  @Input() total: number;
  // last update
  @Input() lastUpdate: Date;
  // reload
  @Output() reload: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  /**
   * emit reload
   */
  emitReload() {
    this.reload.emit();
  }

  dropDownToggle() {
    this.direction = !this.direction;
  }
}
