import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {
  // set steps
  @Input() set steps(count: number) {
    this.createSteps(count);
  }
  // current step
  @Input() step: number;
  // step change
  @Output() stepChange: EventEmitter<number> = new EventEmitter<number>();
  // step list
  stepList: number[] = [];

  constructor() {}

  ngOnInit(): void {}

  /**
   * create steps
   * @param count the number of steps
   */
  private createSteps(count: number) {
    if (count) {
      count = typeof count !== 'number' ? parseInt(count, null) : count;

      for (let i = 0; i < count; i++) {
        this.stepList.push(i + 1);
      }
    }
  }

  /**
   * emit step change
   * @param index index
   */
  emitStepChange(index: number) {
    this.stepChange.emit(index);
  }
}
