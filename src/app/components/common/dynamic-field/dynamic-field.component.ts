import { Component, EventEmitter, HostBinding, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { OptionItem } from '../../../models/common/option-item';
import { FormControl } from '@angular/forms';
import { SubscriptionInventory } from '../../../utils/subscribe.util';

export interface AppDynamicFieldConfig {
  label: string;
  type: 'input' | 'select' | 'date' | 'start-date' | 'end-date';
  options?: OptionItem[];
  placeholder?: string;
  width?: number;
  tabfield?: number;
  mobilefield?: number;
}

@Component({
  selector: 'app-dynamic-field',
  templateUrl: './dynamic-field.component.html',
  styleUrls: ['./dynamic-field.component.scss']
})
export class DynamicFieldComponent implements OnInit, OnDestroy {
  // config for dynamic field
  @Input() config: AppDynamicFieldConfig;
  // max date
  @Input() maxDate: Date;
  // min date
  @Input() minDate: Date;
  // start date change
  @Output() startDateChange: EventEmitter<Date> = new EventEmitter<Date>();
  // end date change
  @Output() endDateChange: EventEmitter<Date> = new EventEmitter<Date>();
  // set host width
  @HostBinding('style.width') get width() {
    if (window.innerWidth > 1024) {
      return this.config.width ? this.config.width + 'px' : null;
    } else if (window.innerWidth > 767 && window.innerWidth < 1024) {
      return this.config.width ? this.config.tabfield + 'px' : null;
    } else if (window.innerWidth <= 767) {
      return this.config.mobilefield ? this.config.mobilefield + '%' : null;
    }
  }

  // control
  control: FormControl = new FormControl('');
  // inventory
  private inventory: SubscriptionInventory = new SubscriptionInventory();

  constructor() {}

  ngOnInit(): void {
    this.subscribeDateControl();
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  private subscribeDateControl() {
    const sub = this.control.valueChanges.subscribe({
      next: (value) => {
        if (this.config.type === 'start-date') {
          this.startDateChange.emit(new Date(value));
        } else if (this.config.type === 'end-date') {
          this.endDateChange.emit(new Date(value));
        }
      }
    });

    this.inventory.store('valueChanges', sub);
  }
}
