import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconRefreshDocComponent } from './icon-refresh-doc.component';

@NgModule({
  declarations: [IconRefreshDocComponent],
  exports: [IconRefreshDocComponent],
  imports: [CommonModule]
})
export class IconRefreshDocModule {}
