import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-refresh-doc',
  templateUrl: './icon-refresh-doc.component.html',
  styleUrls: ['./icon-refresh-doc.component.scss']
})
export class IconRefreshDocComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
