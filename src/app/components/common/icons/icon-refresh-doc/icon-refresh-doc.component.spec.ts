import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconRefreshDocComponent } from './icon-refresh-doc.component';

describe('IconRefreshDocComponent', () => {
  let component: IconRefreshDocComponent;
  let fixture: ComponentFixture<IconRefreshDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconRefreshDocComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconRefreshDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
