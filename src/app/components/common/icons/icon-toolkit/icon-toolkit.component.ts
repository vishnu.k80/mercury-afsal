import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-toolkit',
  templateUrl: './icon-toolkit.component.html',
  styleUrls: ['./icon-toolkit.component.scss']
})
export class IconToolkitComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
