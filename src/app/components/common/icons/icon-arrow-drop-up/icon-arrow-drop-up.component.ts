import { Component } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-arrow-drop-up',
  templateUrl: './icon-arrow-drop-up.component.html',
  styleUrls: ['./icon-arrow-drop-up.component.scss']
})
export class IconArrowDropUpComponent extends IconBaseComponent {}
