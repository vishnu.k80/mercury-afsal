import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-lock-small',
  templateUrl: './icon-lock-small.component.html',
  styleUrls: ['./icon-lock-small.component.scss']
})
export class IconLockSmallComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
