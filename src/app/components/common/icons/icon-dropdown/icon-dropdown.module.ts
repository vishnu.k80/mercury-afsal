import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconDropdownComponent } from './icon-dropdown.component';

@NgModule({
  declarations: [IconDropdownComponent],
  exports: [IconDropdownComponent],
  imports: [CommonModule]
})
export class IconDropdownModule {}
