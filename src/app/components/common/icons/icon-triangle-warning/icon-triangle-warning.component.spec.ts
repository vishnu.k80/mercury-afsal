import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconTriangleWarningComponent } from './icon-triangle-warning.component';

describe('IconTriangleWarningComponent', () => {
  let component: IconTriangleWarningComponent;
  let fixture: ComponentFixture<IconTriangleWarningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconTriangleWarningComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconTriangleWarningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
