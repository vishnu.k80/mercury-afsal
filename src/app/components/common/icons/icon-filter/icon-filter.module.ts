import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconFilterComponent } from './icon-filter.component';

@NgModule({
  declarations: [IconFilterComponent],
  exports: [IconFilterComponent],
  imports: [CommonModule]
})
export class IconFilterModule {}
