import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconMobileComponent } from './icon-mobile.component';

@NgModule({
  declarations: [IconMobileComponent],
  exports: [IconMobileComponent],
  imports: [CommonModule]
})
export class IconMobileModule {}
