import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconArrowLeftComponent } from './icon-arrow-left.component';

@NgModule({
  declarations: [IconArrowLeftComponent],
  exports: [IconArrowLeftComponent],
  imports: [CommonModule]
})
export class IconArrowLeftModule {}
