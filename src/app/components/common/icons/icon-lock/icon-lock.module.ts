import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconLockComponent } from './icon-lock.component';

@NgModule({
  declarations: [IconLockComponent],
  exports: [IconLockComponent],
  imports: [CommonModule]
})
export class IconLockModule {}
