import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconChevronRightDoubleComponent } from './icon-chevron-right-double.component';

@NgModule({
  declarations: [IconChevronRightDoubleComponent],
  exports: [IconChevronRightDoubleComponent],
  imports: [CommonModule]
})
export class IconChevronRightDoubleModule {}
