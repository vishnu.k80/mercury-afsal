import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconEmailComponent } from './icon-email.component';

@NgModule({
  declarations: [IconEmailComponent],
  imports: [CommonModule],
  exports: [IconEmailComponent]
})
export class IconEmailModule {}
