import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconHelpComponent } from './icon-help.component';

@NgModule({
  declarations: [IconHelpComponent],
  exports: [IconHelpComponent],
  imports: [CommonModule]
})
export class IconHelpModule {}
