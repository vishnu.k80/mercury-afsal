import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-home',
  templateUrl: './icon-home.component.html',
  styleUrls: ['./icon-home.component.scss']
})
export class IconHomeComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
