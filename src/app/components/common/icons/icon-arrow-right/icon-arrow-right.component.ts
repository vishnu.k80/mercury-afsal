import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-arrow-right',
  templateUrl: './icon-arrow-right.component.html',
  styleUrls: ['./icon-arrow-right.component.scss']
})
export class IconArrowRightComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
