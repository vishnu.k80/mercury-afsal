import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconToolkitReverseComponent } from './icon-toolkit-reverse.component';

describe('IconToolkitReverseComponent', () => {
  let component: IconToolkitReverseComponent;
  let fixture: ComponentFixture<IconToolkitReverseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconToolkitReverseComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconToolkitReverseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
