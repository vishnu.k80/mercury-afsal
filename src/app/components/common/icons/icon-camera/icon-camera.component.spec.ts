import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconCameraComponent } from './icon-camera.component';

describe('IconCameraComponent', () => {
  let component: IconCameraComponent;
  let fixture: ComponentFixture<IconCameraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconCameraComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconCameraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
