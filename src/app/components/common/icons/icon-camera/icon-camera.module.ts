import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconCameraComponent } from './icon-camera.component';

@NgModule({
  declarations: [IconCameraComponent],
  exports: [IconCameraComponent],
  imports: [CommonModule]
})
export class IconCameraModule {}
