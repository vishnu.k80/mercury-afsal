import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconCheckComponent } from './icon-check.component';

@NgModule({
  declarations: [IconCheckComponent],
  exports: [IconCheckComponent],
  imports: [CommonModule]
})
export class IconCheckModule {}
