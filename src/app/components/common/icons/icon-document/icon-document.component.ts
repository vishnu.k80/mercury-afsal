import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-document',
  templateUrl: './icon-document.component.html',
  styleUrls: ['./icon-document.component.scss']
})
export class IconDocumentComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
