import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconDocumentComponent } from './icon-document.component';

@NgModule({
  declarations: [IconDocumentComponent],
  exports: [IconDocumentComponent],
  imports: [CommonModule]
})
export class IconDocumentModule {}
