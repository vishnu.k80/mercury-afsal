import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconShieldComponent } from './icon-shield.component';

@NgModule({
  declarations: [IconShieldComponent],
  exports: [IconShieldComponent],
  imports: [CommonModule]
})
export class IconShieldModule {}
