import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-shield',
  templateUrl: './icon-shield.component.html',
  styleUrls: ['./icon-shield.component.scss']
})
export class IconShieldComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
