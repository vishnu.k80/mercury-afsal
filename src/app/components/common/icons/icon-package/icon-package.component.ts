import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-package',
  templateUrl: './icon-package.component.html',
  styleUrls: ['./icon-package.component.scss']
})
export class IconPackageComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
