import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconAdminComponent } from './icon-admin.component';

@NgModule({
  declarations: [IconAdminComponent],
  exports: [IconAdminComponent],
  imports: [CommonModule]
})
export class IconAdminModule {}
