import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconMoreVerticalComponent } from './icon-more-vertical.component';

@NgModule({
  declarations: [IconMoreVerticalComponent],
  exports: [IconMoreVerticalComponent],
  imports: [CommonModule]
})
export class IconMoreVerticalModule {}
