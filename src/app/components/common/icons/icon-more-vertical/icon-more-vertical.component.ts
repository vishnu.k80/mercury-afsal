import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-more-vertical',
  templateUrl: './icon-more-vertical.component.html',
  styleUrls: ['./icon-more-vertical.component.scss']
})
export class IconMoreVerticalComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
