import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-eye',
  templateUrl: './icon-eye.component.html',
  styleUrls: ['./icon-eye.component.scss']
})
export class IconEyeComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
