import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconLineChartComponent } from './icon-line-chart.component';

@NgModule({
  declarations: [IconLineChartComponent],
  exports: [IconLineChartComponent],
  imports: [CommonModule]
})
export class IconLineChartModule {}
