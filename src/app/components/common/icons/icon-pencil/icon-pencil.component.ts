import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-pencil',
  templateUrl: './icon-pencil.component.html',
  styleUrls: ['./icon-pencil.component.scss']
})
export class IconPencilComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
