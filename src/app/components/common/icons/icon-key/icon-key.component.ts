import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-key',
  templateUrl: './icon-key.component.html',
  styleUrls: ['./icon-key.component.scss']
})
export class IconKeyComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
