import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconGridComponent } from './icon-grid.component';

@NgModule({
  declarations: [IconGridComponent],
  exports: [IconGridComponent],
  imports: [CommonModule]
})
export class IconGridModule {}
