import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthCodeGeneratorComponent } from './auth-code-generator.component';
import { OutlinedButtonModule } from '../buttons/outlined-button/outlined-button.module';
import { SpinnerModule } from '../spinner/spinner.module';
import { InlineButtonModule } from '../buttons/inline-button/inline-button.module';
import { IconEmailModule } from '../icons/icon-email/icon-email.module';
import { IconShieldModule } from '../icons/icon-shield/icon-shield.module';
import { IconReloadModule } from '../icons/icon-reload/icon-reload.module';

@NgModule({
  declarations: [AuthCodeGeneratorComponent],
  exports: [AuthCodeGeneratorComponent],
  imports: [
    CommonModule,
    OutlinedButtonModule,
    SpinnerModule,
    InlineButtonModule,
    IconEmailModule,
    IconShieldModule,
    IconReloadModule
  ]
})
export class AuthCodeGeneratorModule {}
