import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthCodeGeneratorComponent } from './auth-code-generator.component';

describe('ToolkitCodeGeneratorComponent', () => {
  let component: AuthCodeGeneratorComponent;
  let fixture: ComponentFixture<AuthCodeGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuthCodeGeneratorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthCodeGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
