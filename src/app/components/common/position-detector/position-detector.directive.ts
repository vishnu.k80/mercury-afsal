import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  HostBinding,
  Inject,
  Input,
  Output,
  Renderer2
} from '@angular/core';
import { getElement } from '../../../utils/element.util';
import { DOCUMENT } from '@angular/common';

export type AppVerticalPosition = 'top' | 'bottom';
export type AppHorizontalPosition = 'left' | 'right';

@Directive({
  selector: '[appPositionDetector]',
  exportAs: 'appPositionDetector'
})
export class PositionDetectorDirective implements AfterViewInit {
  // vertical position priority
  @Input() set verticalPriority(verticalPriority: AppVerticalPosition) {
    this.vertical = verticalPriority;
    this.top = verticalPriority === 'top';
    this.bottom = !this.top;
  }
  // horizontal position priority
  @Input() set horizontalPriority(horizontalPriority: AppHorizontalPosition) {
    this.horizontal = horizontalPriority;
    this.left = horizontalPriority === 'left';
    this.right = !this.left;
  }
  // container for element
  @Input() positionDetectorContainer: ElementRef<HTMLElement> | HTMLElement;
  // vertical detected event
  @Output() verticalPriorityChange: EventEmitter<AppVerticalPosition> = new EventEmitter<AppVerticalPosition>();
  // horizontal detected event
  @Output() horizontalPriorityChange: EventEmitter<AppHorizontalPosition> = new EventEmitter<AppHorizontalPosition>();

  // positions
  // top
  @HostBinding('class.top') top = false;
  // bottom
  @HostBinding('class.bottom') bottom = false;
  // left
  @HostBinding('class.left') left = false;
  // right
  @HostBinding('class.right') right = false;
  // scrollable container
  scrollableContainer: HTMLElement;
  // vertical position
  private vertical: AppVerticalPosition = 'bottom';
  // horizontal position
  private horizontal: AppHorizontalPosition = 'right';

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private elementRef: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {}

  ngAfterViewInit(): void {
    this.hideElement();
    // set timeout to prevent error
    setTimeout(() => {
      this.setContainer();
      this.detectPosition();
    });
  }

  /**
   * set scrollable container
   * if no scrollable container, parent which has max height will be container
   */
  private setContainer() {
    const el = getElement(this.elementRef);

    if (el) {
      let parent: HTMLElement = el.parentElement;
      let mostLongParent: HTMLElement = parent;

      while (parent) {
        // get computed style
        const computedStyle = getComputedStyle(parent);
        const overflow =
          computedStyle.getPropertyValue('overflow') ||
          computedStyle.getPropertyValue('overflow-x') ||
          computedStyle.getPropertyValue('overflow-y');

        // set most long parent if parent's offset height is longer than previous most long parent
        if (mostLongParent.offsetHeight < parent.offsetHeight) {
          mostLongParent = parent;
        }

        if (this.document.body === parent) {
          // set most long parent as container
          this.scrollableContainer = mostLongParent;
        } else if (overflow !== '' && overflow !== 'visible') {
          // set current parent as container when scrollable
          this.scrollableContainer = parent;
          break;
        }

        parent = parent.parentElement;
      }
    }
  }

  /**
   * detect element position
   */
  private detectPosition() {
    const el = getElement(this.elementRef);
    const boundary = getElement(this.positionDetectorContainer);
    const container = this.scrollableContainer;

    if (el && boundary && container) {
      const elRect = el.getBoundingClientRect();
      const boundaryRect = boundary.getBoundingClientRect();
      const containerRect = container.getBoundingClientRect();
      const boundaryBottom = boundaryRect.top + boundaryRect.height;
      const boundaryRight = boundaryRect.left + boundaryRect.width;

      // when element on bottom side,
      const bottom = boundaryBottom + elRect.height;
      const bottomAvailable = containerRect.height + containerRect.top > bottom;

      // when element on top side,
      const top = boundaryRect.top - elRect.height;
      const topAvailable = containerRect.top < top;

      // when element on right side,
      const right = boundaryRect.left + elRect.width;
      const rightAvailable = containerRect.width + containerRect.left > right;

      // when element on left side,
      const left = boundaryRight - elRect.width;
      const leftAvailable = boundaryRect.left > left;

      // emit detected vertical position
      if (bottomAvailable === topAvailable) {
        this.verticalPriorityChange.emit(this.vertical);
      } else if (bottomAvailable) {
        this.verticalPriorityChange.emit('bottom');
      } else {
        this.verticalPriorityChange.emit('top');
      }

      // emit detected horizontal position
      if (leftAvailable === rightAvailable) {
        this.horizontalPriorityChange.emit(this.horizontal);
      } else if (leftAvailable) {
        this.horizontalPriorityChange.emit('left');
      } else {
        this.horizontalPriorityChange.emit('right');
      }

      setTimeout(() => {
        this.showElement();
      });
    }
  }

  private hideElement() {
    const el = getElement(this.elementRef);

    if (el) {
      this.renderer.setStyle(el, 'visibility', 'hidden');
    }
  }

  private showElement() {
    const el = getElement(this.elementRef);

    if (el) {
      this.renderer.setStyle(el, 'visibility', 'visible');
    }
  }
}
