import { Component, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';

@Component({
  selector: 'app-custom-scrollbar',
  templateUrl: './custom-scrollbar.component.html',
  styleUrls: ['./custom-scrollbar.component.scss']
})
export class CustomScrollbarComponent implements OnInit {
  @Input() config;

  @ViewChild(PerfectScrollbarComponent) perfectScrollbar: PerfectScrollbarComponent;

  constructor() {}

  ngOnInit(): void {}

  @HostListener('window:resize')
  onWindowResize() {
    this.perfectScrollbar.ngOnInit();
  }
}
