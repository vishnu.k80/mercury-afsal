import { Directive, ElementRef, HostBinding, HostListener, Input, OnDestroy } from '@angular/core';
import { FieldBaseComponent } from '../common/field-base.component';
import { FormControl } from '@angular/forms';
import { SubscriptionInventory } from '../../../../utils/subscribe.util';
import { getElement } from '../../../../utils/element.util';

@Directive({
  selector: '[appInput]'
})
export class InputDirective extends FieldBaseComponent<string> implements OnDestroy {
  // set form control
  @Input() set formControl(formControl: FormControl) {
    if (formControl) {
      this.control = formControl;
      this.sliceValueWithMaxLength();
      this.subscribeControlChanges();
      this.updateInputValue(this.control.value);
    }
  }
  // field class
  @HostBinding('class.app-field') class = true;
  // spellcheck false
  @HostBinding('attr.spellcheck') spellCheck = 'false';
  // autocomplete off
  @HostBinding('attr.autocomplete') autoComplete = 'off';
  // return true when input is disabled
  @HostBinding('class.app-disabled') get isDisabled() {
    const input = getElement(this.elementRef);

    if (input) {
      return input.disabled;
    }
  }
  // set error class
  @HostBinding('class.app-error') get error() {
    return this.control.touched && this.control.invalid;
  }
  // form control
  control: FormControl = new FormControl('');
  // subscription inventory
  private inventory: SubscriptionInventory = new SubscriptionInventory();
  // subscription keys
  private keys = {
    controlChanges: 'controlChanges'
  };

  constructor(private elementRef: ElementRef<HTMLInputElement>) {
    super();
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * subscribe control changes
   */
  private subscribeControlChanges() {
    this.inventory.unSubscribe(this.keys.controlChanges);

    const sub = this.control.valueChanges.subscribe({
      next: (value) => {
        this.updateInputValue(value);
      }
    });

    this.inventory.store(this.keys.controlChanges, sub);
  }

  /**
   * update input value with form control's value
   * @param value form control value
   */
  private updateInputValue(value: string) {
    const input = getElement(this.elementRef);

    if (input) {
      input.value = value;
    }
  }

  /**
   * listen input event from the input element
   * @param value input value
   */
  @HostListener('input', ['$event.target.value'])
  onInput(value: string) {
    this.control.markAsTouched();
    this.control.setValue(value);
    this.sliceValueWithMaxLength();
  }

  /**
   * splice value with maxlength when control has maxlength error
   */
  private sliceValueWithMaxLength() {
    if (this.control.hasError('maxlength')) {
      this.control.setValue(this.control.value.substr(0, this.control.getError('maxlength').requiredLength));
    }
  }
}
