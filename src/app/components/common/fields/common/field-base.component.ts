import { HostBinding, HostListener, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

export class FieldBaseComponent<T> {
  // form control for field
  @Input() formControl: FormControl;
  // field focus class
  @HostBinding('class.app-focus') focus = false;
  // field disabled class
  @HostBinding('class.app-disabled') disabled = false;

  /**
   * set focus true when focused
   */
  @HostListener('focus')
  onFocus() {
    this.focus = true;
  }

  /**
   * set focus false when blurred
   */
  @HostListener('blur')
  onBlur() {
    this.focus = false;
  }
}
