import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { OptionItem } from '../../../../models/common/option-item';

export type AppFormFieldType = 'input' | 'select' | 'text';

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss']
})
export class FormFieldComponent implements OnInit {
  // label
  @Input() label: string;
  // type
  @Input() type: AppFormFieldType;
  // control
  @Input() control: FormControl;
  // options
  @Input() options: OptionItem[] = [];
  // errors
  @Input() errors: { [key: string]: string } = {};
  // max length
  @Input() maxLength: number;
  // additional label
  @Input() additionalLabel: string;
  // mask
  @Input() mask: string;
  // placeholder
  @Input() placeholder: string;

  constructor() {}

  ngOnInit(): void {}

  /**
   * return error code list
   */
  get errorCodes() {
    return Object.keys(this.errors);
  }

  /**
   * return true when control has error
   * @param code error code
   */
  hasError(code: string) {
    return this.control.hasError(code) && (!this.control.pristine || this.control.touched);
  }
}
