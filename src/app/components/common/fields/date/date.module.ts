import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateComponent } from './date.component';
import { InputModule } from '../input/input.module';
import { IconCalendarTodayModule } from '../../icons/icon-calendar-today/icon-calendar-today.module';
import { CalendarModule } from '../../calendar/calendar.module';
import { OutsideDetectorModule } from '../../outside-detector/outside-detector.module';
import { PositionDetectorModule } from '../../position-detector/position-detector.module';

@NgModule({
  declarations: [DateComponent],
  exports: [DateComponent],
  imports: [
    CommonModule,
    InputModule,
    IconCalendarTodayModule,
    CalendarModule,
    OutsideDetectorModule,
    PositionDetectorModule
  ]
})
export class DateModule {}
