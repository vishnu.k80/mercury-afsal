import { Directive, HostBinding } from '@angular/core';
import { ButtonBaseComponent } from '../common/button-base.component';

@Directive({
  selector: '[appOutlinedButton]'
})
export class OutlinedButtonDirective extends ButtonBaseComponent {
  // outlined button class
  @HostBinding('class.app-outlined-button') outlinedButtonClass = true;
}
