import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OutlinedButtonDirective } from './outlined-button.directive';

@NgModule({
  declarations: [OutlinedButtonDirective],
  exports: [OutlinedButtonDirective],
  imports: [CommonModule]
})
export class OutlinedButtonModule {}
