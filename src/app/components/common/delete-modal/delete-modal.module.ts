import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteModalComponent } from './delete-modal.component';
import { CardModule } from '../card/card.module';
import { FlatButtonModule } from '../buttons/flat-button/flat-button.module';
import { InlineButtonModule } from '../buttons/inline-button/inline-button.module';
import { SpinnerModule } from '../spinner/spinner.module';

@NgModule({
  declarations: [DeleteModalComponent],
  exports: [DeleteModalComponent],
  imports: [CommonModule, CardModule, FlatButtonModule, InlineButtonModule, SpinnerModule]
})
export class DeleteModalModule {}
