import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss']
})
export class DeleteModalComponent implements OnInit {
  // loading state
  @Input() loading = false;
  // confirm click
  @Output() confirmClick: EventEmitter<void> = new EventEmitter<void>();
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  /**
   * emit confirm click
   */
  emitConfirmClick() {
    this.confirmClick.emit();
  }

  /**
   * emit close click
   */
  emitCloseClick() {
    this.closeClick.emit();
  }
}
