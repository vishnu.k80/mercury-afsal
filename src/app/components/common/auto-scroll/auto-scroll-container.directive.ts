import { AfterContentInit, ContentChildren, Directive, ElementRef, QueryList } from '@angular/core';
import { AutoScrollItemDirective } from './auto-scroll-item.directive';
import { getElement } from '../../../utils/element.util';
import { getOffsetPoints } from '../../../utils/position.util';

@Directive({
  selector: '[appAutoScrollContainer]'
})
export class AutoScrollContainerDirective implements AfterContentInit {
  // auto scroll items
  @ContentChildren(AutoScrollItemDirective, { descendants: true }) autoScrollItems: QueryList<AutoScrollItemDirective>;

  constructor(public elementRef: ElementRef<HTMLElement>) {}

  ngAfterContentInit(): void {
    this.scrollToFocus();
  }

  /**
   * scroll to focused element
   */
  scrollToFocus() {
    const el = getElement(this.elementRef);

    if (el && el.offsetHeight < el.scrollHeight && this.autoScrollItems) {
      // find focused item
      const focusedItem = this.autoScrollItems.find((item) => item.focused);

      if (focusedItem) {
        const focusedElement = getElement(focusedItem.elementRef);

        if (focusedElement) {
          // get offset points of element
          const elementPoints = getOffsetPoints(el);
          const focusedPoints = getOffsetPoints(focusedElement);

          // calculate to set element in center of container
          el.scrollTop = focusedPoints.y + focusedElement.offsetHeight / 2 - (elementPoints.y + el.offsetHeight / 2);
          el.scrollLeft = focusedPoints.x + focusedElement.offsetWidth / 2 - (elementPoints.x + el.offsetWidth / 2);
        }
      }
    }
  }
}
