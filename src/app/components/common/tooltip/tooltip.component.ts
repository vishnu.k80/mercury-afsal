import { Component, Input, OnInit } from '@angular/core';

export type AppTooltipPosition = 'top' | 'bottom' | 'left' | 'right';

@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent implements OnInit {
  // tooltip text
  @Input() tooltip: string;
  // tooltip position
  @Input() position: AppTooltipPosition = 'bottom';
  // tooltip visible state
  @Input() tooltipVisible = true;

  constructor() {}

  ngOnInit(): void {}
}
