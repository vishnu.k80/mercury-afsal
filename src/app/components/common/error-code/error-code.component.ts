import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { AppAccessHistoryErrorInfo } from '../../../models/data/app-access-history-error-info';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { AppVerticalPosition } from '../position-detector/position-detector.directive';

@Component({
  selector: 'app-error-code',
  templateUrl: './error-code.component.html',
  styleUrls: ['./error-code.component.scss']
})
export class ErrorCodeComponent implements OnInit {
  // set error info
  @Input() set errorInfo(errorInfo: AppAccessHistoryErrorInfo) {
    if (errorInfo) {
      this.info = errorInfo;
      this.solution = this.domSanitizer.bypassSecurityTrustHtml(this.info.solution);
    }
  }
  // error info
  info: AppAccessHistoryErrorInfo;
  // solution html
  solution: SafeHtml;
  // opened state
  opened = false;
  // vertical position
  vertical: AppVerticalPosition = 'bottom';

  constructor(public elementRef: ElementRef<HTMLElement>, private domSanitizer: DomSanitizer) {}

  ngOnInit(): void {}

  open() {
    this.opened = true;
  }

  close() {
    this.opened = false;
  }

  toggle() {
    if (this.opened) {
      this.close();
    } else {
      this.open();
    }
  }
}
