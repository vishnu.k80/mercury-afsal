import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { AppVerticalPosition } from '../position-detector/position-detector.directive';

@Component({
  selector: 'app-disabled-reason',
  templateUrl: './disabled-reason.component.html',
  styleUrls: ['./disabled-reason.component.scss']
})
export class DisabledReasonComponent implements OnInit {
  // reason
  @Input() reason: string;

  // reason opened state
  opened = false;
  // vertical position
  vertical: AppVerticalPosition = 'bottom';

  constructor(public elementRef: ElementRef<HTMLElement>) {}

  ngOnInit(): void {}

  /**
   * open reason
   */
  open() {
    this.opened = true;
  }

  /**
   * close reason
   */
  close() {
    this.opened = false;
  }

  /**
   * toggle reason
   */
  toggle() {
    if (this.opened) {
      this.close();
    } else {
      this.open();
    }
  }
}
