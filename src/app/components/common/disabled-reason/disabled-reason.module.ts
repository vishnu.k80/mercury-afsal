import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisabledReasonComponent } from './disabled-reason.component';
import { IconInfoModule } from '../icons/icon-info/icon-info.module';
import { CardModule } from '../card/card.module';
import { OutsideDetectorModule } from '../outside-detector/outside-detector.module';
import { PositionDetectorModule } from '../position-detector/position-detector.module';

@NgModule({
  declarations: [DisabledReasonComponent],
  exports: [DisabledReasonComponent],
  imports: [CommonModule, IconInfoModule, CardModule, OutsideDetectorModule, PositionDetectorModule]
})
export class DisabledReasonModule {}
