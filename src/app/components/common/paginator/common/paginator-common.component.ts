import { EventEmitter, Input, Output } from '@angular/core';
import { PagingChangeEvent } from '../models/paging-change-event';
import { OptionItem } from '../../../../models/common/option-item';

export class PaginatorCommonComponent {
  @Input() page = 0;
  @Input() size = 10;
  @Input() total = 0;

  @Input() set sizes(options: number[]) {
    this.sizeOptions = options.map((option) => {
      return {
        label: option.toString(),
        value: option
      };
    });
  }

  @Output() pagingChange: EventEmitter<PagingChangeEvent> = new EventEmitter<PagingChangeEvent>();

  sizeOptions: OptionItem<number>[] = [
    {
      label: '10',
      value: 10
    },
    {
      label: '20',
      value: 20
    },
    {
      label: '50',
      value: 50
    }
  ];

  hasPrev = false;
  hasNext = false;

  get totalPage() {
    return Math.ceil(this.total / this.size);
  }

  /**
   * to next if next page is exists
   */
  toNext() {
    if (this.hasNext) {
      this.emitPageChange(this.page + 1, this.size);
    }
  }

  /**
   * to prev if prev page is exists
   */
  toPrev() {
    if (this.hasPrev) {
      this.emitPageChange(this.page - 1, this.size);
    }
  }

  /**
   * emit page change event
   * @param page page
   * @param size size
   */
  emitPageChange(page: number, size: number) {
    this.pagingChange.emit(new PagingChangeEvent(page, size));
  }

  /**
   * define existence of prev next pages
   */
  protected defineExistenceOfPrevNext() {
    this.hasPrev = this.page !== 0;
    this.hasNext = this.page + 1 < this.totalPage;
  }
}
