import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CookiePolicyComponent } from './cookie-policy.component';
import { IconInfoModule } from '../icons/icon-info/icon-info.module';
import { IconCloseModule } from '../icons/icon-close/icon-close.module';

@NgModule({
  declarations: [CookiePolicyComponent],
  exports: [CookiePolicyComponent],
  imports: [CommonModule, IconInfoModule, IconCloseModule]
})
export class CookiePolicyModule {}
