import { Directive, EventEmitter, HostListener, Output } from '@angular/core';
import { neutralizeEvent } from '../../../utils/event.util';

@Directive({
  selector: 'form[appFormNeutralizer]'
})
export class FormNeutralizerDirective {
  // form submitted
  @Output() appSubmitted: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  /**
   * neutralize event
   * @param event event
   */
  @HostListener('submit', ['$event'])
  onSubmit(event: Event) {
    neutralizeEvent(event);
    this.appSubmitted.emit();
  }
}
