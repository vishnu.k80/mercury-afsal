import {
  AfterViewInit,
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList
} from '@angular/core';
import { TableBodyCellDirective } from './custom-contents/table-body-cell.directive';
import { TableHeaderCellDirective } from './custom-contents/table-header-cell.directive';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../utils/subscribe.util';
import { TableColumn } from '../../../models/common/table-column';
import { PagingChangeEvent } from '../paginator/models/paging-change-event';

interface TableSubscriptions extends SubscriptionKeys {
  tableBodyCells: SubscriptionItem;
  tableHeaderCells: SubscriptionItem;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent<T> implements OnInit, AfterViewInit, OnDestroy {
  // page
  @Input() page: number;
  // size
  @Input() size: number;
  // total
  @Input() total: number;
  // table rows
  @Input() rows: T[] = [];
  // table columns
  @Input() columns: TableColumn[] = [];
  // loading state
  @Input() loading = false;
  // Load more value
  @Input() loadLabel: string;
  // sort change emitter
  @Output() sortChange: EventEmitter<void> = new EventEmitter<void>();
  // paging change emitter
  @Output() pagingChange: EventEmitter<PagingChangeEvent> = new EventEmitter<PagingChangeEvent>();
  // table body cells
  @ContentChildren(TableBodyCellDirective) tableBodyCells: QueryList<TableBodyCellDirective>;
  // table body cells
  @ContentChildren(TableHeaderCellDirective) tableHeaderCells: QueryList<TableHeaderCellDirective>;
  // map for body cells
  bodyCells: { [key: string]: TableBodyCellDirective } = {};
  // map for header cells
  headerCells: { [key: string]: TableHeaderCellDirective } = {};
  // inventory
  private inventory: SubscriptionInventory<TableSubscriptions> = new SubscriptionInventory<TableSubscriptions>();

  mobileHide = true;
  constructor() {}

  ngOnInit(): void {
    this.permissionHide();
  }

  ngAfterViewInit(): void {
    this.createBodyCellsMap();
    this.createHeaderCellsMap();
    this.subscribeBodyCellChanges();
    this.subscribeHeaderCellChanges();
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * subscribe body cell changes
   */
  private subscribeBodyCellChanges() {
    this.inventory.unSubscribe('tableBodyCells');

    const sub = this.tableBodyCells.changes.subscribe({
      next: () => {
        this.createBodyCellsMap();
      }
    });

    this.inventory.store('tableBodyCells', sub);
  }

  /**
   * subscribe header cell changes
   */
  private subscribeHeaderCellChanges() {
    this.inventory.unSubscribe('tableHeaderCells');

    const sub = this.tableHeaderCells.changes.subscribe({
      next: () => {
        this.createHeaderCellsMap();
      }
    });

    this.inventory.store('tableHeaderCells', sub);
  }

  /**
   * create map for body cells
   */
  private createBodyCellsMap() {
    this.bodyCells = {};
    this.tableBodyCells.forEach((cell) => (this.bodyCells[cell.columnProperty] = cell));
  }

  /**
   * create map for header cells
   */
  private createHeaderCellsMap() {
    this.headerCells = {};
    this.tableHeaderCells.forEach((cell) => (this.headerCells[cell.columnProperty] = cell));
  }

  /**
   * sortable header clicked
   * @param column column
   */
  onSortableHeaderClicked(column: TableColumn) {
    if (column.useSort) {
      column.changeSortDirection();
      this.emitSortChange();
    }
  }

  /**
   * emit sort change
   */
  emitSortChange() {
    this.sortChange.emit();
  }

  /**
   * emit paging change event
   * @param event event
   */
  emitPagingChange(event: PagingChangeEvent) {
    this.pagingChange.emit(event);
  }
  permissionHide() {
    if (window.innerWidth < 768) {
      this.mobileHide = false;
    } else {
      this.mobileHide = true;
    }
  }
}
