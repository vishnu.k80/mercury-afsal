import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table.component';
import { TableBodyCellDirective } from './custom-contents/table-body-cell.directive';
import { TableHeaderCellDirective } from './custom-contents/table-header-cell.directive';
import { IconArrowDropDownModule } from '../icons/icon-arrow-drop-down/icon-arrow-drop-down.module';
import { IconArrowDropUpModule } from '../icons/icon-arrow-drop-up/icon-arrow-drop-up.module';
import { TableRowDataPipeModule } from '../../../pipes/table-row-data-pipe/table-row-data-pipe.module';
import { SpinnerModule } from '../spinner/spinner.module';
import { PaginatorModule } from '../paginator/paginator.module';
import { CustomScrollbarModule } from '../custom-scrollbar/custom-scrollbar.module';

@NgModule({
  declarations: [TableComponent, TableBodyCellDirective, TableHeaderCellDirective],
  exports: [TableComponent, TableBodyCellDirective],
  imports: [
    CommonModule,
    IconArrowDropDownModule,
    IconArrowDropUpModule,
    TableRowDataPipeModule,
    SpinnerModule,
    PaginatorModule,
    CustomScrollbarModule
  ]
})
export class TableModule {}
