import { Component, EventEmitter, HostBinding, Input, OnChanges, Output } from '@angular/core';
import { AppGroupComponent, AppGroupDirection } from '../../../../interfaces/app-group.component';
import { AppCheckboxAppearance } from '../checkbox.component';

export class AppCheckboxOption<T = string> {
  label: string;
  value: T;
  selected = false;

  constructor(label: string, value: T) {
    this.label = label;
    this.value = value;
  }
}

@Component({
  selector: 'app-checkbox-group',
  templateUrl: './checkbox-group.component.html',
  styleUrls: ['./checkbox-group.component.scss']
})
export class CheckboxGroupComponent<T = string> implements OnChanges, AppGroupComponent {
  // value
  @Input() value: T[] = [];
  // options
  @Input() options: AppCheckboxOption<T>[] = [];
  // checkbox appearance
  @Input() appearance: AppCheckboxAppearance = 'default';
  // disabled state
  @Input() @HostBinding('class.disabled') disabled = false;
  // group direction
  @Input() @HostBinding('attr.app-direction') direction: AppGroupDirection = 'horizontal';
  // value change event
  @Output() valueChange: EventEmitter<T[]> = new EventEmitter<T[]>();

  ngOnChanges(): void {
    this.setSelectedCheckbox();
  }

  /**
   * emit value change when checkbox clicked
   * @param item checkbox option item
   */
  onClickCheckbox(item: AppCheckboxOption<T>) {
    if (!this.disabled) {
      // clone values
      const values = [...this.value];

      if (!item.selected) {
        // add value to values
        values.push(item.value);
      } else {
        // remove value from values
        values.splice(values.indexOf(item.value), 1);
      }

      this.valueChange.emit(values);
    }
  }

  /**
   * set selected state of each option
   */
  private setSelectedCheckbox() {
    // clear all selected state
    this.options.forEach((option) => (option.selected = false));

    this.value.forEach((value) => {
      const selectedOption = this.options.find((option) => option.value === value);

      if (selectedOption) {
        selectedOption.selected = true;
      }
    });
  }
}
