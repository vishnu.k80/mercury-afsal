import { Component, HostBinding, Input } from '@angular/core';

export type AppCheckboxAppearance = 'default' | 'filled';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent {
  // active status
  @Input() @HostBinding('class.active') active = false;
  // checkbox appearance
  @Input() @HostBinding('attr.app-appearance') appearance: AppCheckboxAppearance = 'default';
}
