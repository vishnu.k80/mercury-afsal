import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoreOptionsComponent } from './more-options.component';
import { IconMoreVerticalModule } from '../icons/icon-more-vertical/icon-more-vertical.module';
import { CardModule } from '../card/card.module';
import { OutsideDetectorModule } from '../outside-detector/outside-detector.module';
import { PositionDetectorModule } from '../position-detector/position-detector.module';

@NgModule({
  declarations: [MoreOptionsComponent],
  exports: [MoreOptionsComponent],
  imports: [CommonModule, IconMoreVerticalModule, CardModule, OutsideDetectorModule, PositionDetectorModule]
})
export class MoreOptionsModule {}
