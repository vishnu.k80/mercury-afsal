import { NgModule } from '@angular/core';
import { AccordionComponent } from './accordion.component';
import { AccordionHeaderComponent } from './accordion-header/accordion-header.component';
import { AccordionHeaderAdditionalComponent } from './accordion-header-additional/accordion-header-additional.component';
import { AccordionContentComponent } from './accordion-content/accordion-content.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AccordionComponent,
    AccordionHeaderComponent,
    AccordionHeaderAdditionalComponent,
    AccordionContentComponent
  ],
  imports: [CommonModule],
  exports: [AccordionComponent, AccordionHeaderComponent, AccordionHeaderAdditionalComponent, AccordionContentComponent]
})
export class AccordionModule {}
