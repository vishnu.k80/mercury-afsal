import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccordionHeaderAdditionalComponent } from './accordion-header-additional.component';

describe('AccordionHeaderAdditionalComponent', () => {
  let component: AccordionHeaderAdditionalComponent;
  let fixture: ComponentFixture<AccordionHeaderAdditionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccordionHeaderAdditionalComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionHeaderAdditionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
