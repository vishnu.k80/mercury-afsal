import { Component, ElementRef } from '@angular/core';
import { AccordionBaseComponent } from '../common/accordion-base.component';

@Component({
  selector: 'app-accordion-header-additional',
  templateUrl: './accordion-header-additional.component.html',
  styleUrls: ['./accordion-header-additional.component.scss']
})
export class AccordionHeaderAdditionalComponent extends AccordionBaseComponent {
  constructor(protected elementRef: ElementRef<HTMLElement>) {
    super(elementRef);
  }
}
