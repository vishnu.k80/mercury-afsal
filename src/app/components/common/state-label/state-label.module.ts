import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StateLabelComponent } from './state-label.component';
import { DisabledReasonModule } from '../disabled-reason/disabled-reason.module';

@NgModule({
  declarations: [StateLabelComponent],
  exports: [StateLabelComponent],
  imports: [CommonModule, DisabledReasonModule]
})
export class StateLabelModule {}
