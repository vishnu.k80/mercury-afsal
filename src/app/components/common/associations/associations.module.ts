import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssociationsComponent } from './associations.component';
import { IconLockModule } from '../icons/icon-lock/icon-lock.module';
import { IconPackageModule } from '../icons/icon-package/icon-package.module';
import { IconMobileModule } from '../icons/icon-mobile/icon-mobile.module';
import { CardModule } from '../card/card.module';
import { CustomScrollbarModule } from '../custom-scrollbar/custom-scrollbar.module';
import { OutlinedButtonModule } from '../buttons/outlined-button/outlined-button.module';
import { StateLabelModule } from '../state-label/state-label.module';
import { OutsideDetectorModule } from '../outside-detector/outside-detector.module';
import { PositionDetectorModule } from '../position-detector/position-detector.module';
import { UserInfoModule } from '../user-info/user-info.module';

@NgModule({
  declarations: [AssociationsComponent],
  exports: [AssociationsComponent],
  imports: [
    CommonModule,
    IconLockModule,
    IconPackageModule,
    IconMobileModule,
    CardModule,
    CustomScrollbarModule,
    OutlinedButtonModule,
    StateLabelModule,
    OutsideDetectorModule,
    PositionDetectorModule,
    UserInfoModule
  ]
})
export class AssociationsModule {}
