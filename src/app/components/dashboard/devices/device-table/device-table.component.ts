import { Component, OnInit, ViewChild } from '@angular/core';
import { TableBaseComponent } from '../../common/table/table-base.component';
import { AppDevice } from '../../../../models/data/app-device';
import { TableColumn } from '../../../../models/common/table-column';
import { AppAssociatedSpace } from '../../../../models/data/app-space';
import { cloneDeep } from '../../../../utils/lodash.util';
import { AppAssociationType } from '../../../../models/data/app-associations';
import { SideModalDeviceInfoComponent } from '../side-modal-device-info/side-modal-device-info.component';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { appStoreSelectors } from '../../../../stores/app-store/app-store.selectors';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-device-table',
  templateUrl: './device-table.component.html',
  styleUrls: ['../../common/table/table-base.component.scss', './device-table.component.scss']
})
export class DeviceTableComponent extends TableBaseComponent<AppDevice> implements OnInit {
  // device info modal
  @ViewChild(SideModalDeviceInfoComponent) sideModalDeviceInfo: SideModalDeviceInfoComponent;
  // selected device
  selectedDevice: AppDevice;
  // associated spaces
  associations: AppAssociatedSpace[] = [];
  // add association visible state
  addAssociationVisible = false;
  // access history visible state
  accessHistoryVisible = false;
  // device info visible state
  deviceInfoVisible = false;
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.deviceEditPermission);
  // Load more label
  loadMoreLable = 'device';
  // access history read permission
  accessHistoryReadPermission$: Observable<boolean> = this.store.select(appStoreSelectors.accessHistoryReadPermission);
  constructor(private store: Store) {
    super();

    // init columns
    this.columns = [
      new TableColumn('sn', 'SN'),
      new TableColumn('name', 'NAME', true),
      new TableColumn('associatedSpaces', 'ASSOCIATED SPACE'),
      new TableColumn('state', 'STATE'),
      new TableColumn('lastAccess', 'LAST ACCESSED BY'),
      new TableColumn('map', '')
    ];
  }

  ngOnInit(): void {}

  get readOnly$() {
    return this.editPermission$.pipe(map((permission) => !permission));
  }

  /**
   * open add associations
   * @param device device
   * @param associations associations
   */
  openAddAssociation(device: AppDevice, associations: AppAssociatedSpace[]) {
    this.selectedDevice = device;
    this.associations = associations;
    this.addAssociationVisible = true;
  }

  /**
   * close add associations
   */
  closeAddAssociation() {
    this.selectedDevice = null;
    this.associations = [];
    this.addAssociationVisible = false;
  }

  /**
   * apply changed associations to table
   * @param associations changed associations
   */
  onAssociationApplied(associations: AppAssociationType[]) {
    this.selectedDevice.associatedSpaces = associations as AppAssociatedSpace[];
    // clone data to update table
    this.data = cloneDeep(this.data);
    this.closeAddAssociation();
  }

  /**
   * open access history
   * @param device device
   */
  openAccessHistory(device: AppDevice) {
    this.selectedDevice = device;
    this.accessHistoryVisible = true;
  }

  /**
   * close access history
   */
  closeAccessHistory() {
    this.selectedDevice = null;
    this.accessHistoryVisible = false;
  }

  /**
   * open device info
   * @param device device
   */
  openDeviceInfo(device: AppDevice) {
    this.selectedDevice = device;
    this.deviceInfoVisible = true;
  }

  /**
   * close device info
   */
  closeDeviceInfo() {
    this.selectedDevice = null;
    this.deviceInfoVisible = false;
  }

  /**
   * open device info
   * @param device device
   */
  openDeviceEdit(device: AppDevice) {
    this.openDeviceInfo(device);

    setTimeout(() => {
      if (this.sideModalDeviceInfo) {
        this.sideModalDeviceInfo.toEdit();
      }
    });
  }
}
