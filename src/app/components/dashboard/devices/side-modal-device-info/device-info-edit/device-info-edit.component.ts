import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppDevice } from '../../../../../models/data/app-device';
import { FormControl, Validators } from '@angular/forms';
import { DeviceService } from '../../../../../services/device.service';
import { MessageService } from '../../../../common/message/services/message.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../../utils/subscribe.util';

interface DeviceEditSubscription extends SubscriptionKeys {
  editDevice: SubscriptionItem;
}

@Component({
  selector: 'app-device-info-edit',
  templateUrl: './device-info-edit.component.html',
  styleUrls: ['../../../common/info/info-base.component.scss', './device-info-edit.component.scss']
})
export class DeviceInfoEditComponent implements OnInit {
  // device
  @Input() set device(device: AppDevice) {
    this.setFieldValues(device);
  }
  // saved
  @Output() saved: EventEmitter<AppDevice> = new EventEmitter<AppDevice>();
  // cancel click
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();
  // loading
  loading = false;
  // name
  name: FormControl = new FormControl('', Validators.required);
  // name errors
  nameErrors = {
    required: 'The name is required'
  };
  // inventory
  private inventory: SubscriptionInventory<DeviceEditSubscription> = new SubscriptionInventory<
    DeviceEditSubscription
  >();

  constructor(private deviceService: DeviceService, private messageService: MessageService) {}

  ngOnInit(): void {}

  /**
   * save changed data
   */
  onSaveClicked() {
    this.name.markAsTouched();

    if (this.name.invalid) {
      return;
    }

    this.inventory.unSubscribe('editDevice');
    this.loading = true;

    const sub = this.deviceService.updateDevice(this.name.value).subscribe({
      next: (device) => {
        this.messageService.open('default', 'Device updated');
        this.saved.emit(device as AppDevice);
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('editDevice', sub);
  }

  /**
   * set field values
   */
  private setFieldValues(device: AppDevice) {
    if (device) {
      this.name.setValue(device.name);
    }
  }
}
