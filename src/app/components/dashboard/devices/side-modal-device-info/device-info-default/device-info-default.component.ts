import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppDevice } from '../../../../../models/data/app-device';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { appStoreSelectors } from '../../../../../stores/app-store/app-store.selectors';

@Component({
  selector: 'app-device-info-default',
  templateUrl: './device-info-default.component.html',
  styleUrls: ['../../../common/info/info-base.component.scss', './device-info-default.component.scss']
})
export class DeviceInfoDefaultComponent implements OnInit {
  // device
  @Input() device: AppDevice;
  // edit click
  @Output() editClick: EventEmitter<void> = new EventEmitter<void>();
  // cancel click
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.deviceEditPermission);

  constructor(private store: Store) {}

  ngOnInit(): void {}
}
