import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { AppUser } from '../../../../../models/data/app-user';
import { Store } from '@ngrx/store';
import { appStoreSelectors } from '../../../../../stores/app-store/app-store.selectors';
import { map } from 'rxjs/operators';
import { DeviceService } from '../../../../../services/device.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../../utils/subscribe.util';
import { MessageService } from '../../../../common/message/services/message.service';

interface DeviceRegisterSubscriptions extends SubscriptionKeys {
  verify: SubscriptionItem;
}

@Component({
  selector: 'app-device-register-toolkit',
  templateUrl: './device-register-toolkit.component.html',
  styleUrls: ['../common/register-device-base.component.scss', './device-register-toolkit.component.scss']
})
export class DeviceRegisterToolkitComponent implements OnInit, OnDestroy {
  // auth code
  @Input() code: string;
  // back click
  @Output() backClick: EventEmitter<void> = new EventEmitter<void>();
  // code change
  @Output() codeChange: EventEmitter<string> = new EventEmitter<string>();
  // verified
  @Output() verified: EventEmitter<void> = new EventEmitter<void>();
  // user stream
  user$: Observable<AppUser> = this.store.select(appStoreSelectors.user);
  // loading state
  loading = false;
  // inventory
  private inventory: SubscriptionInventory<DeviceRegisterSubscriptions> = new SubscriptionInventory<
    DeviceRegisterSubscriptions
  >();

  constructor(private store: Store, private deviceService: DeviceService, private messageService: MessageService) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * get toolkit
   */
  get toolkit$() {
    return this.user$.pipe(
      map((user) => {
        if (user) {
          return user.toolkit;
        }
      })
    );
  }

  /**
   * go to previous step when step is 1
   * @param step changed step
   */
  onStepChanged(step: number) {
    if (step === 1) {
      this.emitBackClick();
    }
  }

  /**
   * emit back click
   */
  emitBackClick() {
    this.backClick.emit();
  }

  /**
   * verify toolkit
   */
  verifyToolkit() {
    if (!this.code) {
      this.messageService.open('error', 'Please generate auth code');
      return;
    }

    this.inventory.unSubscribe('verify');
    this.loading = true;

    const sub = this.deviceService.verifyToolkit().subscribe({
      next: () => {
        this.verified.emit();
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('verify', sub);
  }
}
