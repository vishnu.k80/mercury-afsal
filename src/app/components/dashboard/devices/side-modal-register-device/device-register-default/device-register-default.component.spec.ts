import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceRegisterDefaultComponent } from './device-register-default.component';

describe('DeviceRegisterDefaultComponent', () => {
  let component: DeviceRegisterDefaultComponent;
  let fixture: ComponentFixture<DeviceRegisterDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceRegisterDefaultComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceRegisterDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
