import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppDevice } from '../../../../../models/data/app-device';
import { DeviceRegisterData } from '../device-register-default/device-register-default.component';

@Component({
  selector: 'app-device-register-success',
  templateUrl: './device-register-success.component.html',
  styleUrls: ['../common/register-device-base.component.scss', './device-register-success.component.scss']
})
export class DeviceRegisterSuccessComponent implements OnInit {
  // created device
  @Input() device: DeviceRegisterData;
  // open detail
  @Output() openDetail: EventEmitter<AppDevice> = new EventEmitter<AppDevice>();
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  /**
   * emit open detail
   */
  emitOpenDetail() {
    this.openDetail.emit(this.device as any);
  }

  /**
   * emit close click
   */
  emitCloseClick() {
    this.closeClick.emit();
  }
}
