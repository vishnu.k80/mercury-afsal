import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceRegisterSuccessComponent } from './device-register-success.component';

describe('DeviceRegisterSuccessComponent', () => {
  let component: DeviceRegisterSuccessComponent;
  let fixture: ComponentFixture<DeviceRegisterSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceRegisterSuccessComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceRegisterSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
