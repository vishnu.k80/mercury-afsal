import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DeviceRegisterData } from './device-register-default/device-register-default.component';
import { AppDevice } from '../../../../models/data/app-device';

export type SideModalRegisterDeviceView = 'default' | 'toolkit' | 'success';

@Component({
  selector: 'app-side-modal-register-device',
  templateUrl: './side-modal-register-device.component.html',
  styleUrls: ['./side-modal-register-device.component.scss']
})
export class SideModalRegisterDeviceComponent implements OnInit {
  // succeeded to register
  @Output() succeeded: EventEmitter<void> = new EventEmitter<void>();
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // open detail
  @Output() openDetail: EventEmitter<AppDevice> = new EventEmitter<AppDevice>();
  // device data
  data: DeviceRegisterData;
  // auth code
  code: string;
  // view type
  view: SideModalRegisterDeviceView = 'default';

  constructor() {}

  ngOnInit(): void {}

  /**
   * emit close click
   */
  emitCloseClick() {
    this.closeClick.emit();
  }

  /**
   * emit succeeded
   */
  emitSucceeded() {
    this.succeeded.emit();
  }

  /**
   * go to next step
   * @param data data
   */
  toNextStep(data: DeviceRegisterData) {
    this.data = data;
    this.view = 'toolkit';
  }

  /**
   * go to default page
   */
  toDefault() {
    this.view = 'default';
  }

  /**
   * go to success page
   */
  toSuccess() {
    this.view = 'success';
    this.emitSucceeded();
  }
}
