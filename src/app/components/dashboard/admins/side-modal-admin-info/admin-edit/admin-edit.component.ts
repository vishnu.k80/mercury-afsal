import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppAdmin } from '../../../../../models/data/app-user';
import { OptionItem } from '../../../../../models/common/option-item';
import {
  ADMIN_ACTIVE_STATE,
  ADMIN_DISABLED_STATE,
  ADMIN_INACTIVE_STATE,
  ADMIN_INVITED_STATE
} from '../../../../../utils/constants.util';
import { cloneDeep } from '../../../../../utils/lodash.util';
import { AdminService } from '../../../../../services/admin.service';
import { MessageService } from '../../../../common/message/services/message.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../../utils/subscribe.util';
import { Observable } from 'rxjs';
import { AppStatus } from '../../../../../models/data/app-status';
import { Store } from '@ngrx/store';
import { appStoreSelectors } from '../../../../../stores/app-store/app-store.selectors';

interface AdminEditSubscriptions extends SubscriptionKeys {
  updateAdmin: SubscriptionItem;
  statuses$: SubscriptionItem;
  serialValueChanges: SubscriptionItem;
}

@Component({
  selector: 'app-admin-edit',
  templateUrl: './admin-edit.component.html',
  styleUrls: ['../common/admin-info-base.component.scss', './admin-edit.component.scss']
})
export class AdminEditComponent implements OnInit, OnDestroy {
  // set user
  @Input() set user(user: AppAdmin) {
    if (user) {
      this.userData = cloneDeep(user);
      this.setFieldValues();
    }
  }
  // saved emitter
  @Output() saved: EventEmitter<AppAdmin> = new EventEmitter<AppAdmin>();
  // cancel click
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();
  // statuses stream
  statuses$: Observable<AppStatus[]> = this.store.select(appStoreSelectors.statuses);
  // user data
  userData: AppAdmin;
  // loading state
  loading = false;
  // toolkit
  toolkit = false;
  // serial
  serial: FormControl = new FormControl('');
  // email
  email: FormControl = new FormControl('', [Validators.required, Validators.email]);
  // name
  name: FormControl = new FormControl('', Validators.required);
  // state
  state: FormControl = new FormControl('');
  // disabled reason
  disabledReason: FormControl = new FormControl('');
  // status
  status: FormControl = new FormControl('');
  // state options
  stateOptions: OptionItem[] = [
    new OptionItem<string>(ADMIN_ACTIVE_STATE, ADMIN_ACTIVE_STATE),
    new OptionItem<string>(ADMIN_INVITED_STATE, ADMIN_INVITED_STATE),
    new OptionItem<string>(ADMIN_INACTIVE_STATE, ADMIN_INACTIVE_STATE),
    new OptionItem<string>(ADMIN_DISABLED_STATE, ADMIN_DISABLED_STATE)
  ];
  // status options
  statusOptions: OptionItem[] = [];
  // email errors
  emailErrors = {
    required: 'The email is required',
    email: 'The email is invalid'
  };
  // name errors
  nameErrors = {
    required: 'The name is required'
  };
  // disabled reason errors
  disabledReasonErrors = {
    required: 'The disabled reason is required'
  };
  // form group
  private formGroup: FormGroup = new FormGroup({
    email: this.email,
    name: this.name,
    state: this.state,
    disabledReason: this.disabledReason,
    status: this.status,
    serial: this.serial
  });
  // inventory
  private inventory: SubscriptionInventory<AdminEditSubscriptions> = new SubscriptionInventory<
    AdminEditSubscriptions
  >();

  constructor(private store: Store, private adminService: AdminService, private messageService: MessageService) {
    this.disabledReasonValidator = this.disabledReasonValidator.bind(this);
    this.disabledReason.setValidators(this.disabledReasonValidator);
    this.getStatuses();
  }

  ngOnInit(): void {
    this.subscribeStateChange();
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * get statuses
   */
  getStatuses() {
    const sub = this.statuses$.subscribe({
      next: (statuses) => {
        this.statusOptions = statuses
          .filter((status) => status.group === 'admin' && status.use)
          .map((status) => new OptionItem(status.label, status.label));
      }
    });

    this.inventory.store('statuses$', sub);
  }

  /**
   * disabled reason validator
   * @param control control
   */
  disabledReasonValidator(control: AbstractControl): null | any {
    if (this.state.value === ADMIN_DISABLED_STATE) {
      if (!control.value) {
        return {
          required: {
            value: control.value
          }
        };
      }
    }
  }

  /**
   * subscribe state change
   */
  private subscribeStateChange() {
    this.state.valueChanges.subscribe({
      next: () => {
        this.validateDisabledReason();
      }
    });
  }

  /**
   * set field values with user data
   */
  setFieldValues() {
    this.email.setValue(this.userData.email);
    this.name.setValue(`${this.userData.firstName} ${this.userData.lastName}`);
    this.state.setValue(this.userData.state);
    this.disabledReason.setValue(this.userData.disabledReason);
    this.status.setValue(this.userData.status);
    this.toolkit = !!this.userData.toolkit;
  }

  /**
   * save user data
   */
  onSaveClicked() {
    // check validations
    this.formGroup.markAllAsTouched();
    this.validateDisabledReason();

    if (this.formGroup.invalid) {
      return;
    }

    this.inventory.unSubscribe('updateAdmin');
    this.loading = true;

    const sub = this.adminService
      .updateAdminUser(
        this.email.value,
        this.name.value,
        this.state.value,
        this.disabledReason.value,
        this.status.value,
        this.userData.permissions
      )
      .subscribe({
        next: (user) => {
          this.messageService.open('default', 'Administrator profile updated');
          this.emitSaved(user as AppAdmin);
        },
        error: (err) => {
          this.messageService.open('error', err.message);
          this.loading = false;
        }
      });

    this.inventory.store('updateAdmin', sub);
  }

  /**
   * emit saved
   * @param user new user data
   */
  emitSaved(user: AppAdmin) {
    this.saved.emit(user);
  }

  /**
   * emit cancel click
   */
  emitCancelClick() {
    this.cancelClick.emit();
  }

  /**
   * validate disabled reason
   */
  private validateDisabledReason() {
    if (this.state.value === ADMIN_DISABLED_STATE) {
      if (!this.disabledReason.value) {
        this.disabledReason.setErrors({
          required: {
            value: this.disabledReason.value
          }
        });
      }
    } else {
      this.disabledReason.setErrors(null);
    }
  }

  /**
   * enable toolkit
   */
  enableToolkit() {
    this.toolkit = true;
  }

  /**
   * disable toolkit
   */
  disableToolkit() {
    this.toolkit = false;
  }
}
