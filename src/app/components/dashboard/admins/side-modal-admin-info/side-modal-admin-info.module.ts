import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalAdminInfoComponent } from './side-modal-admin-info.component';
import { SideModalModule } from '../../common/side-modal/side-modal.module';
import { SideUserInfoModule } from '../../common/side-user-info/side-user-info.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { LabeledValueModule } from '../../../common/labeled-value/labeled-value.module';
import { StateLabelModule } from '../../../common/state-label/state-label.module';
import { PermissionsModule } from '../../../common/permissions/permissions.module';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { AdminEditComponent } from './admin-edit/admin-edit.component';
import { InputModule } from '../../../common/fields/input/input.module';
import { FormFieldModule } from '../../../common/fields/form-field/form-field.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { InlineButtonModule } from '../../../common/buttons/inline-button/inline-button.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { ToolkitInfoModule } from '../../../common/toolkit-info/toolkit-info.module';
import { IconToolkitReverseModule } from '../../../common/icons/icon-toolkit-reverse/icon-toolkit-reverse.module';
import { RadioModule } from '../../../common/radio/radio.module';

@NgModule({
  declarations: [SideModalAdminInfoComponent, AdminProfileComponent, AdminEditComponent],
  exports: [SideModalAdminInfoComponent],
  imports: [
    CommonModule,
    SideModalModule,
    SideUserInfoModule,
    CustomScrollbarModule,
    LabeledValueModule,
    StateLabelModule,
    PermissionsModule,
    InputModule,
    FormFieldModule,
    FlatButtonModule,
    InlineButtonModule,
    SpinnerModule,
    ToolkitInfoModule,
    IconToolkitReverseModule,
    RadioModule
  ]
})
export class SideModalAdminInfoModule {}
