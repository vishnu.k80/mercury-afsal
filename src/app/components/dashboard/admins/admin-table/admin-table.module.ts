import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminTableComponent } from './admin-table.component';
import { TableModule } from '../../../common/table/table.module';
import { UserInfoModule } from '../../../common/user-info/user-info.module';
import { StateLabelModule } from '../../../common/state-label/state-label.module';
import { IconToolkitReverseModule } from '../../../common/icons/icon-toolkit-reverse/icon-toolkit-reverse.module';
import { PermissionEditorModule } from '../../../common/permission-editor/permission-editor.module';
import { MoreOptionsModule } from '../../../common/more-options/more-options.module';
import { IconButtonModule } from '../../../common/buttons/icon-button/icon-button.module';
import { IconLineChartModule } from '../../../common/icons/icon-line-chart/icon-line-chart.module';
import { IconLockSmallModule } from '../../../common/icons/icon-lock-small/icon-lock-small.module';
import { IconPencilModule } from '../../../common/icons/icon-pencil/icon-pencil.module';
import { IconDeleteModule } from '../../../common/icons/icon-delete/icon-delete.module';
import { TooltipModule } from '../../../common/tooltip/tooltip.module';
import { SideModalAdminInfoModule } from '../side-modal-admin-info/side-modal-admin-info.module';
import { SideModalAccessHistoryModule } from '../../common/side-modal-access-history/side-modal-access-history.module';
import { DeleteModalModule } from '../../../common/delete-modal/delete-modal.module';
import { SideModalActivityHistoryModule } from '../../common/side-modal-activity-history/side-modal-activity-history.module';

@NgModule({
  declarations: [AdminTableComponent],
  exports: [AdminTableComponent],
  imports: [
    CommonModule,
    TableModule,
    UserInfoModule,
    StateLabelModule,
    IconToolkitReverseModule,
    PermissionEditorModule,
    MoreOptionsModule,
    IconButtonModule,
    IconLineChartModule,
    IconLockSmallModule,
    IconPencilModule,
    IconDeleteModule,
    TooltipModule,
    SideModalAdminInfoModule,
    SideModalAccessHistoryModule,
    DeleteModalModule,
    SideModalActivityHistoryModule
  ]
})
export class AdminTableModule {}
