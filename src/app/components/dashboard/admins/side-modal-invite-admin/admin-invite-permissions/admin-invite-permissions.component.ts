import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AppUserPermissions } from '../../../../../models/data/app-user-permissions';
import { AdminService } from '../../../../../services/admin.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../../utils/subscribe.util';
import { AppAdminInviteFields } from '../admin-invite-default/admin-invite-default.component';
import { MessageService } from '../../../../common/message/services/message.service';

interface AdminInviteSubscriptions extends SubscriptionKeys {
  sendInvite: SubscriptionItem;
}

@Component({
  selector: 'app-admin-invite-permissions',
  templateUrl: './admin-invite-permissions.component.html',
  styleUrls: ['../common/admin-invite-base.component.scss', './admin-invite-permissions.component.scss']
})
export class AdminInvitePermissionsComponent implements OnInit, OnDestroy {
  // data
  @Input() data: AppAdminInviteFields;
  // permissions
  @Input() permissions: AppUserPermissions;
  // email sent
  @Output() emailSent: EventEmitter<void> = new EventEmitter<void>();
  // back click emitter
  @Output() backClick: EventEmitter<void> = new EventEmitter<void>();
  // loading
  loading = false;
  // inventory
  private inventory: SubscriptionInventory<AdminInviteSubscriptions> = new SubscriptionInventory<
    AdminInviteSubscriptions
  >();

  constructor(private adminService: AdminService, private messageService: MessageService) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * if step is 1, return to previous page
   * @param step changed step
   */
  onStepChanged(step: number) {
    if (step === 1) {
      this.emitBackClick();
    }
  }

  /**
   * emit back click
   */
  emitBackClick() {
    this.backClick.emit();
  }

  /**
   * emit email sent
   */
  emitEmailSent() {
    this.emailSent.emit();
  }

  /**
   * send invite mail
   */
  sendInvite() {
    this.inventory.unSubscribe('sendInvite');
    this.loading = true;

    const sub = this.adminService.sendInviteMail(this.data, this.permissions).subscribe({
      next: () => {
        this.emitEmailSent();
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('sendInvite', sub);
  }
}
