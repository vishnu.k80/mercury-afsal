import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalInviteAdminComponent } from './side-modal-invite-admin.component';
import { SideModalModule } from '../../common/side-modal/side-modal.module';
import { AdminInviteDefaultComponent } from './admin-invite-default/admin-invite-default.component';
import { AdminInvitePermissionsComponent } from './admin-invite-permissions/admin-invite-permissions.component';
import { AdminInviteSuccessComponent } from './admin-invite-success/admin-invite-success.component';
import { StepperModule } from '../../../common/stepper/stepper.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { FormFieldModule } from '../../../common/fields/form-field/form-field.module';
import { IconToolkitReverseModule } from '../../../common/icons/icon-toolkit-reverse/icon-toolkit-reverse.module';
import { RadioModule } from '../../../common/radio/radio.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { InlineButtonModule } from '../../../common/buttons/inline-button/inline-button.module';
import { PermissionsModule } from '../../../common/permissions/permissions.module';
import { IconVerifiedModule } from '../../../common/icons/icon-verified/icon-verified.module';
import { IconAdminModule } from '../../../common/icons/icon-admin/icon-admin.module';
import { OutlinedButtonModule } from '../../../common/buttons/outlined-button/outlined-button.module';

@NgModule({
  declarations: [
    SideModalInviteAdminComponent,
    AdminInviteDefaultComponent,
    AdminInvitePermissionsComponent,
    AdminInviteSuccessComponent
  ],
  exports: [SideModalInviteAdminComponent],
  imports: [
    CommonModule,
    SideModalModule,
    StepperModule,
    CustomScrollbarModule,
    FormFieldModule,
    IconToolkitReverseModule,
    RadioModule,
    SpinnerModule,
    FlatButtonModule,
    InlineButtonModule,
    PermissionsModule,
    IconVerifiedModule,
    IconAdminModule,
    OutlinedButtonModule
  ]
})
export class SideModalInviteAdminModule {}
