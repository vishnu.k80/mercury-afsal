import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../../utils/subscribe.util';

export interface AppAdminInviteFields {
  email: string;
  name: string;
  serial: string;
  toolkit: boolean;
}

interface AdminInviteDefaultKeys extends SubscriptionKeys {
  serialValueChanges: SubscriptionItem;
}

@Component({
  selector: 'app-admin-invite-default',
  templateUrl: './admin-invite-default.component.html',
  styleUrls: ['../common/admin-invite-base.component.scss', './admin-invite-default.component.scss']
})
export class AdminInviteDefaultComponent implements OnInit, OnDestroy {
  // set data
  @Input() set data(data: AppAdminInviteFields) {
    if (data) {
      this.setFieldValues(data);
    }
  }
  // next clicked
  @Output() nextClick: EventEmitter<AppAdminInviteFields> = new EventEmitter<AppAdminInviteFields>();
  // cancel click
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();
  // email
  email: FormControl = new FormControl('', [Validators.required, Validators.email]);
  // name
  name: FormControl = new FormControl('', Validators.required);
  // serial pattern
  serial: FormControl = new FormControl('');
  // toolkit user state
  toolkit = true;
  // email errors
  emailErrors = {
    required: 'The email is required',
    email: 'The email is invalid'
  };
  // name errors
  nameErrors = {
    required: 'The name is required'
  };
  // form group
  formGroup: FormGroup = new FormGroup({
    email: this.email,
    name: this.name,
    serial: this.serial
  });
  // inventory
  private inventory: SubscriptionInventory<AdminInviteDefaultKeys> = new SubscriptionInventory<
    AdminInviteDefaultKeys
  >();

  constructor() {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * if step is 2, go to next step
   * @param step step index
   */
  onStepChanged(step: number) {
    if (step === 2) {
      this.emitNextClick();
    }
  }

  /**
   * emit next click with values
   */
  emitNextClick() {
    this.formGroup.markAllAsTouched();

    if (this.formGroup.invalid) {
      return;
    }

    this.nextClick.emit({
      email: this.email.value,
      name: this.name.value,
      serial: this.toolkit ? this.serial.value : null,
      toolkit: this.toolkit
    });
  }

  /**
   * emit cancel click
   */
  emitCancelClick() {
    this.cancelClick.emit();
  }

  /**
   * set field values
   * @param data data
   */
  private setFieldValues(data: AppAdminInviteFields) {
    this.email.setValue(data.email);
    this.name.setValue(data.name);
    this.toolkit = data.toolkit;
  }

  /**
   * enable toolkit
   */
  enableToolkit() {
    this.toolkit = true;
  }

  /**
   * disable toolkit
   */
  disableToolkit() {
    this.toolkit = false;
  }
}
