import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-admin-invite-success',
  templateUrl: './admin-invite-success.component.html',
  styleUrls: ['../common/admin-invite-base.component.scss', './admin-invite-success.component.scss']
})
export class AdminInviteSuccessComponent implements OnInit {
  // add new click
  @Output() addNewClick: EventEmitter<void> = new EventEmitter<void>();
  // close click emitter
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  /**
   * emit add new
   */
  emitAddNew() {
    this.addNewClick.emit();
  }

  /**
   * emit close click
   */
  emitCloseClick() {
    this.closeClick.emit();
  }
}
