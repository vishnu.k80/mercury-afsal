import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AppAdminInviteFields } from './admin-invite-default/admin-invite-default.component';
import { AppUserPermissions } from '../../../../models/data/app-user-permissions';

export type SideModalInviteAdminViewType = 'default' | 'permissions' | 'success';

@Component({
  selector: 'app-side-modal-invite-admin',
  templateUrl: './side-modal-invite-admin.component.html',
  styleUrls: ['./side-modal-invite-admin.component.scss']
})
export class SideModalInviteAdminComponent implements OnInit {
  // click close emitter
  @Output() clickClose: EventEmitter<void> = new EventEmitter<void>();
  // created
  @Output() created: EventEmitter<void> = new EventEmitter<void>();
  // invite admin view type
  view: SideModalInviteAdminViewType = 'default';
  // admin data
  admin: AppAdminInviteFields;
  // permissions
  permissions: AppUserPermissions = {
    administrators: {
      read: false,
      edit: false
    },
    keyholders: {
      read: false,
      edit: false
    },
    devices: {
      read: false,
      edit: false
    },
    spaces: {
      read: false,
      edit: false
    },
    accessHistory: {
      read: false,
      edit: false
    },
    configurations: {
      read: false,
      edit: false
    }
  };

  constructor() {}

  ngOnInit(): void {}

  /**
   * emit click close
   */
  emitClickClose() {
    this.clickClose.emit();
  }

  /**
   * go to next step
   * @param admin admin
   */
  toNextStep(admin: AppAdminInviteFields) {
    this.admin = admin;
    this.view = 'permissions';
  }

  /**
   * to default view
   */
  toPreviousStep() {
    this.view = 'default';
  }

  /**
   * go to success page and remove previous data
   */
  onInviteSuccess() {
    // remove admin data
    this.admin = null;

    // set all permissions to false
    Object.keys(this.permissions).forEach((key) => {
      this.permissions[key].edit = false;
      this.permissions[key].read = false;
    });

    this.created.emit();
    this.view = 'success';
  }
}
