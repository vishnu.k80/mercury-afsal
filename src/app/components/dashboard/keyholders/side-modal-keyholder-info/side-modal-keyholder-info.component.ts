import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { AppKeyholder } from '../../../../models/data/app-user';
import { InfoBaseComponent } from '../../common/info/info-base.component';

@Component({
  selector: 'app-side-modal-keyholder-info',
  templateUrl: './side-modal-keyholder-info.component.html',
  styleUrls: ['./side-modal-keyholder-info.component.scss']
})
export class SideModalKeyholderInfoComponent extends InfoBaseComponent implements OnInit {
  // keyholder user
  @Input() user: AppKeyholder;

  constructor(protected changeDetectorRef: ChangeDetectorRef) {
    super(changeDetectorRef);
  }

  ngOnInit(): void {}

  /**
   * update user data when user saved
   * @param user new user data
   */
  onUserSaved(user: AppKeyholder) {
    this.user.firstName = user.firstName;
    this.user.lastName = user.lastName;

    this.toDefault();
    this.updateInfoHeight();
    this.emitUpdated();
  }
}
