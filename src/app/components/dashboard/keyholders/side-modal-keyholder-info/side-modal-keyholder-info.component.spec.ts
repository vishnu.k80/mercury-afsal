import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalKeyholderInfoComponent } from './side-modal-keyholder-info.component';

describe('SideModalKeyholderInfoComponent', () => {
  let component: SideModalKeyholderInfoComponent;
  let fixture: ComponentFixture<SideModalKeyholderInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalKeyholderInfoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalKeyholderInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
