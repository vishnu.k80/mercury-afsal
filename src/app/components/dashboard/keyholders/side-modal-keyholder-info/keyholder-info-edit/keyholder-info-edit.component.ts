import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AppKeyholder } from '../../../../../models/data/app-user';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { KeyholderService } from '../../../../../services/keyholder.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../../utils/subscribe.util';
import { MessageService } from '../../../../common/message/services/message.service';
import { OptionItem } from '../../../../../models/common/option-item';
import {
  ADMIN_DISABLED_STATE,
  KEYHOLDER_ACTIVE_STATE,
  KEYHOLDER_DISABLED_STATE,
  KEYHOLDER_INACTIVE_STATE
} from '../../../../../utils/constants.util';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppStatus } from '../../../../../models/data/app-status';
import { appStoreSelectors } from '../../../../../stores/app-store/app-store.selectors';

interface KeyholderEditSubscriptions extends SubscriptionKeys {
  editKeyholder: SubscriptionItem;
}

@Component({
  selector: 'app-keyholder-info-edit',
  templateUrl: './keyholder-info-edit.component.html',
  styleUrls: ['../../../common/info/info-base.component.scss', './keyholder-info-edit.component.scss']
})
export class KeyholderInfoEditComponent implements OnInit, OnDestroy {
  // set user
  @Input() set user(user: AppKeyholder) {
    this.userData = user;
    this.setFieldValues();
  }
  // saved
  @Output() saved: EventEmitter<AppKeyholder> = new EventEmitter<AppKeyholder>();
  // cancel click
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();
  // loading
  loading = false;
  // user data
  userData: AppKeyholder;
  // name
  name: FormControl = new FormControl('', Validators.required);
  // description
  description: FormControl = new FormControl('');
  // pin
  pin: FormControl = new FormControl('', Validators.pattern(/^\d{4}$/));
  // payload
  payload: FormControl = new FormControl('', Validators.pattern(/^[0-9A-Fa-f]*$/));
  // state
  state: FormControl = new FormControl('');
  // disabled reason
  disabledReason: FormControl = new FormControl('');
  // status
  status: FormControl = new FormControl('');
  // state options
  stateOptions: OptionItem[] = [
    new OptionItem<string>(KEYHOLDER_ACTIVE_STATE, KEYHOLDER_ACTIVE_STATE),
    new OptionItem<string>(KEYHOLDER_INACTIVE_STATE, KEYHOLDER_INACTIVE_STATE),
    new OptionItem<string>(KEYHOLDER_DISABLED_STATE, KEYHOLDER_DISABLED_STATE)
  ];
  // status options
  statusOptions: OptionItem[] = [];
  // allow pin release
  allowPinRelease = false;
  // name errors
  nameErrors = {
    required: 'The name is required'
  };
  // disabledReason errors
  disabledReasonErrors = {
    required: 'The disabled reason is required'
  };
  // pin errors
  pinErrors = {
    pattern: 'The pin should be 4 digits number'
  };
  // payload errors
  payloadErrors = {
    pattern: 'Invalid payload'
  };
  // statuses stream
  statuses$: Observable<AppStatus[]> = this.store.select(appStoreSelectors.statuses);

  private formGroup: FormGroup = new FormGroup({
    name: this.name,
    description: this.description,
    pin: this.pin,
    payload: this.payload,
    state: this.state,
    status: this.status,
    disabledReason: this.disabledReason
  });
  // inventory
  private inventory: SubscriptionInventory<KeyholderEditSubscriptions> = new SubscriptionInventory<
    KeyholderEditSubscriptions
  >();

  constructor(
    private store: Store,
    private keyholderService: KeyholderService,
    private messageService: MessageService
  ) {
    this.disabledReasonValidator = this.disabledReasonValidator.bind(this);
    this.disabledReason.setValidators(this.disabledReasonValidator);
    this.getStatuses();
  }

  ngOnInit(): void {
    this.subscribeStateChange();
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  getStatuses() {
    const sub = this.statuses$.subscribe((statuses) => {
      this.statusOptions = statuses
        .filter((status) => status.group === 'keyholder')
        .map((status) => {
          return new OptionItem(status.label, status.label);
        });
    });

    this.inventory.store('statuses$', sub);
  }

  /**
   * save changed data
   */
  onSaveClicked() {
    this.formGroup.markAllAsTouched();
    this.validateDisabledReason();

    if (this.formGroup.invalid) {
      return;
    }

    this.inventory.unSubscribe('editKeyholder');
    this.loading = true;

    const sub = this.keyholderService.updateKeyholder(this.name.value).subscribe({
      next: (user) => {
        this.messageService.open('default', 'Keyholder profile updated');
        this.saved.emit(user as AppKeyholder);
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('editKeyholder', sub);
  }

  /**
   * set field values
   */
  private setFieldValues() {
    if (this.userData) {
      this.name.setValue(`${this.userData.firstName} ${this.userData.lastName}`);
      this.description.setValue(this.userData.description);
      this.pin.setValue(this.userData.pin);
      this.payload.setValue(this.userData.payload);
      this.state.setValue(this.userData.state);
      this.status.setValue(this.userData.status);
      this.disabledReason.setValue(this.userData.disabledReason);
    }
  }

  /**
   * disabled reason validator
   * @param control control
   */
  disabledReasonValidator(control: AbstractControl): null | any {
    if (this.state.value === ADMIN_DISABLED_STATE) {
      if (!control.value) {
        return {
          required: {
            value: control.value
          }
        };
      }
    }
  }

  /**
   * subscribe state change
   */
  private subscribeStateChange() {
    this.state.valueChanges.subscribe({
      next: () => {
        this.validateDisabledReason();
      }
    });
  }

  /**
   * validate disabled reason
   */
  private validateDisabledReason() {
    if (this.state.value === ADMIN_DISABLED_STATE) {
      if (!this.disabledReason.value) {
        this.disabledReason.setErrors({
          required: {
            value: this.disabledReason.value
          }
        });
      }
    } else {
      this.disabledReason.setErrors(null);
    }
  }
}
