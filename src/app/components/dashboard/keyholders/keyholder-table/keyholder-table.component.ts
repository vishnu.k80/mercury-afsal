import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { TableBaseComponent } from '../../common/table/table-base.component';
import { AppKeyholder } from '../../../../models/data/app-user';
import { TableColumn } from '../../../../models/common/table-column';
import { KeyholderService } from '../../../../services/keyholder.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../utils/subscribe.util';
import { MessageService } from '../../../common/message/services/message.service';
import { AppAssociationType } from '../../../../models/data/app-associations';
import { AppAssociatedDevice } from '../../../../models/data/app-device';
import { cloneDeep } from '../../../../utils/lodash.util';
import { AppAssociatedSpace } from '../../../../models/data/app-space';
import { SideModalKeyholderInfoComponent } from '../side-modal-keyholder-info/side-modal-keyholder-info.component';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { appStoreSelectors } from '../../../../stores/app-store/app-store.selectors';
import { map } from 'rxjs/operators';

interface KeyholderTableSubscriptions extends SubscriptionKeys {
  deleteKeyholder: SubscriptionItem;
}

@Component({
  selector: 'app-keyholder-table',
  templateUrl: './keyholder-table.component.html',
  styleUrls: ['../../common/table/table-base.component.scss', './keyholder-table.component.scss']
})
export class KeyholderTableComponent extends TableBaseComponent<AppKeyholder> implements OnInit {
  // admin deleted
  @Output() deleted: EventEmitter<void> = new EventEmitter<void>();
  // delete undo
  @Output() deleteUndo: EventEmitter<void> = new EventEmitter<void>();
  // keyholder info
  @ViewChild(SideModalKeyholderInfoComponent) sideModalKeyholderInfo: SideModalKeyholderInfoComponent;
  // selected keyholder for access history modal
  selectedKeyholder: AppKeyholder;
  // selected associations
  selectedAssociations: AppAssociationType[] = [];
  // access history visible state
  accessHistoryVisible = false;
  // add association modal visible state
  addAssociationVisible = false;
  // activity history visible state
  activityHistoryVisible = false;
  // send auth close visible state
  sendAuthCodeVisible = false;
  // delete modal visible state
  deleteModalVisible = false;
  // keyholder info visible state
  keyholderInfoVisible = false;
  // delete loading
  deleteLoading = false;
  // association type
  associationType: 'device' | 'space';
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.keyholderEditPermission);
  // access history read permission
  accessHistoryReadPermission$: Observable<boolean> = this.store.select(appStoreSelectors.accessHistoryReadPermission);
  // inventory
  private inventory: SubscriptionInventory<KeyholderTableSubscriptions> = new SubscriptionInventory<
    KeyholderTableSubscriptions
  >();
  // Load more label
  loadMoreLable = 'keyholder';
  constructor(
    private store: Store,
    private keyholderService: KeyholderService,
    private messageService: MessageService
  ) {
    super();

    // init columns
    this.columns = [
      new TableColumn('key.sn', 'KEY SN'),
      new TableColumn('name', 'NAME', true),
      new TableColumn('key.pin', 'PIN CODE'),
      new TableColumn('state', 'STATE'),
      new TableColumn('associatedDevices', 'ASSOCIATED DEVICES'),
      new TableColumn('associatedSpaces', 'ASSOCIATED SPACES')
    ];
  }

  ngOnInit(): void {}

  get readOnly$() {
    return this.editPermission$.pipe(map((permission) => !permission));
  }

  /**
   * open access history
   * @param user user
   */
  openAccessHistory(user: AppKeyholder) {
    this.selectedKeyholder = user;
    this.accessHistoryVisible = true;
  }

  /**
   * close access history
   */
  closeAccessHistory() {
    this.selectedKeyholder = null;
    this.accessHistoryVisible = false;
  }

  /**
   * open add association
   * @param user user
   * @param associations associations
   * @param type association type
   */
  openAddAssociation(user: AppKeyholder, associations: AppAssociatedDevice[], type: 'device' | 'space') {
    this.selectedKeyholder = user;
    this.selectedAssociations = associations;
    this.associationType = type;
    this.addAssociationVisible = true;
  }

  /**
   * close add association
   */
  closeAddAssociation() {
    this.selectedKeyholder = null;
    this.selectedAssociations = [];
    this.associationType = null;
    this.addAssociationVisible = false;
  }

  /**
   * open activity history
   * @param user user
   */
  openActivityHistory(user: AppKeyholder) {
    this.selectedKeyholder = user;
    this.activityHistoryVisible = true;
  }

  /**
   * close activity history
   */
  closeActivityHistory() {
    this.selectedKeyholder = null;
    this.activityHistoryVisible = false;
  }

  /**
   * open delete confirm modal
   * @param user user to delete
   */
  openDeleteConfirmModal(user: AppKeyholder) {
    this.selectedKeyholder = user;
    this.deleteModalVisible = true;
  }

  /**
   * close delete confirm modal
   */
  closeDeleteConfirmModal() {
    this.selectedKeyholder = null;
    this.deleteModalVisible = false;
  }

  /**
   * delete keyholder user and reload table
   */
  deleteKeyholder() {
    this.inventory.unSubscribe('deleteKeyholder');
    this.deleteLoading = true;

    const user = this.selectedKeyholder;
    const sub = this.keyholderService.deleteKeyholder(user).subscribe({
      next: () => {
        this.closeDeleteConfirmModal();
        this.deleted.emit();
        this.deleteLoading = false;
        this.messageService.open('default', `Keyholder ${user.id} has been deleted`, true, () => {
          this.deleteUndo.emit();
        });
      },
      error: (err) => {
        this.closeDeleteConfirmModal();
        this.messageService.open('error', err.message);
        this.deleteLoading = false;
      }
    });

    this.inventory.store('deleteKeyholder', sub);
  }

  /**
   * update associations by type
   * @param associations associations
   */
  onAssociationApplied(associations: AppAssociationType[]) {
    switch (this.associationType) {
      case 'device': {
        this.applyDeviceAssociations(associations);
        break;
      }

      case 'space': {
        this.applySpaceAssociations(associations);
        break;
      }
    }
  }

  /**
   * update associations
   * @param associations associations
   */
  private applyDeviceAssociations(associations: AppAssociationType[]) {
    this.selectedKeyholder.associatedDevices = associations as AppAssociatedDevice[];
    // clone data to update table
    this.data = cloneDeep(this.data);
    this.closeAddAssociation();
  }

  /**
   * update associations
   * @param associations associations
   */
  private applySpaceAssociations(associations: AppAssociationType[]) {
    this.selectedKeyholder.associatedSpaces = associations as AppAssociatedSpace[];
    // clone data to update table
    this.data = cloneDeep(this.data);
    this.closeAddAssociation();
  }

  /**
   * open send auth code
   * @param user user
   */
  openSendAuthCode(user: AppKeyholder) {
    this.selectedKeyholder = user;
    this.sendAuthCodeVisible = true;
  }

  /**
   * close send auth code
   */
  closeSendAuthCode() {
    this.selectedKeyholder = null;
    this.sendAuthCodeVisible = false;
  }

  /**
   * open info
   * @param user user
   */
  openKeyholderInfo(user: AppKeyholder) {
    this.selectedKeyholder = user;
    this.keyholderInfoVisible = true;
  }

  /**
   * close info
   */
  closeKeyholderInfo() {
    this.selectedKeyholder = null;
    this.keyholderInfoVisible = false;
  }

  /**
   * open keyholder edit
   * @param user user
   */
  openKeyholderEdit(user: AppKeyholder) {
    this.openKeyholderInfo(user);

    setTimeout(() => {
      if (this.sideModalKeyholderInfo) {
        this.sideModalKeyholderInfo.toEdit();
      }
    });
  }
}
