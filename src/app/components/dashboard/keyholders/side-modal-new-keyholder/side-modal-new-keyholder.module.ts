import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalNewKeyholderComponent } from './side-modal-new-keyholder.component';
import { SideModalModule } from '../../common/side-modal/side-modal.module';
import { NewKeyholderDefaultComponent } from './new-keyholder-default/new-keyholder-default.component';
import { NewKeyholderSuccessComponent } from './new-keyholder-success/new-keyholder-success.component';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { FormFieldModule } from '../../../common/fields/form-field/form-field.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { InlineButtonModule } from '../../../common/buttons/inline-button/inline-button.module';
import { IconVerifiedModule } from '../../../common/icons/icon-verified/icon-verified.module';
import { OutlinedButtonModule } from '../../../common/buttons/outlined-button/outlined-button.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { CheckboxModule } from '../../../common/checkbox/checkbox.module';
import { IconMobileModule } from '../../../common/icons/icon-mobile/icon-mobile.module';

@NgModule({
  declarations: [SideModalNewKeyholderComponent, NewKeyholderDefaultComponent, NewKeyholderSuccessComponent],
  exports: [SideModalNewKeyholderComponent],
  imports: [
    CommonModule,
    SideModalModule,
    CustomScrollbarModule,
    FormFieldModule,
    FlatButtonModule,
    InlineButtonModule,
    IconVerifiedModule,
    OutlinedButtonModule,
    SpinnerModule,
    CheckboxModule,
    IconMobileModule
  ]
})
export class SideModalNewKeyholderModule {}
