import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewKeyholderDefaultComponent } from './new-keyholder-default.component';

describe('NewKeyholderDefaultComponent', () => {
  let component: NewKeyholderDefaultComponent;
  let fixture: ComponentFixture<NewKeyholderDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewKeyholderDefaultComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewKeyholderDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
