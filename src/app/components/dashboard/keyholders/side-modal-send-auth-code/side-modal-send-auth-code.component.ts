import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppKeyholder } from '../../../../models/data/app-user';

@Component({
  selector: 'app-side-modal-send-auth-code',
  templateUrl: './side-modal-send-auth-code.component.html',
  styleUrls: ['./side-modal-send-auth-code.component.scss']
})
export class SideModalSendAuthCodeComponent implements OnInit {
  // keyholder
  @Input() user: AppKeyholder;
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  /**
   * emit close click
   */
  emitCloseClick() {
    this.closeClick.emit();
  }
}
