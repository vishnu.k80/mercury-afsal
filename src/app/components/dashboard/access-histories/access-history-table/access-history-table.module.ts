import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccessHistoryTableComponent } from './access-history-table.component';
import { TableModule } from '../../../common/table/table.module';
import { TooltipModule } from '../../../common/tooltip/tooltip.module';
import { UserInfoModule } from '../../../common/user-info/user-info.module';
import { IconMapModule } from '../../../common/icons/icon-map/icon-map.module';
import { SideModalDeviceInfoModule } from '../../devices/side-modal-device-info/side-modal-device-info.module';
import { SideModalKeyholderInfoModule } from '../../keyholders/side-modal-keyholder-info/side-modal-keyholder-info.module';
import { ErrorCodeModule } from '../../../common/error-code/error-code.module';

@NgModule({
  declarations: [AccessHistoryTableComponent],
  exports: [AccessHistoryTableComponent],
  imports: [
    CommonModule,
    TableModule,
    TooltipModule,
    UserInfoModule,
    IconMapModule,
    SideModalDeviceInfoModule,
    SideModalKeyholderInfoModule,
    ErrorCodeModule
  ]
})
export class AccessHistoryTableModule {}
