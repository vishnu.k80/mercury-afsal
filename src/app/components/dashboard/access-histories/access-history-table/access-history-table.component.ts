import { Component, OnInit } from '@angular/core';
import { TableBaseComponent } from '../../common/table/table-base.component';
import { AppAccessHistory } from '../../../../models/data/app-access-history';
import { TableColumn } from '../../../../models/common/table-column';
import { ACCESS_HISTORY_SUCCESS_STATE } from '../../../../utils/constants.util';
import { AppDevice } from '../../../../models/data/app-device';
import { AppKeyholder } from '../../../../models/data/app-user';

@Component({
  selector: 'app-access-history-table',
  templateUrl: './access-history-table.component.html',
  styleUrls: ['../../common/table/table-base.component.scss', './access-history-table.component.scss']
})
export class AccessHistoryTableComponent extends TableBaseComponent<AppAccessHistory> implements OnInit {
  // device
  selectedDevice: AppDevice;
  // keyholder
  selectedKeyholder: AppKeyholder;
  // device info
  deviceInfoVisible = false;
  // keyholder info
  keyholderInfoVisible = false;
  // Load more label
  loadMoreLable = 'access history';
  constructor() {
    super();

    this.columns = [
      new TableColumn('createdAt', 'DATE ACCESS', true),
      new TableColumn('device', 'DEVICE NAME', true),
      new TableColumn('keyholder', 'KEYHOLDER NAME', true),
      new TableColumn('operation', 'OPERATION'),
      new TableColumn('state', 'STATE'),
      new TableColumn('errorInfo', 'ERROR CODE'),
      new TableColumn('map', '')
    ];
  }

  ngOnInit(): void {}

  /**
   * return true when history state is not success
   * @param history history
   */
  hasError(history: AppAccessHistory) {
    return history.state !== ACCESS_HISTORY_SUCCESS_STATE;
  }

  /**
   * open device info
   * @param device device
   */
  openDeviceInfo(device: AppDevice) {
    this.selectedDevice = device;
    this.deviceInfoVisible = true;
  }

  /**
   * close device info
   */
  closeDeviceInfo() {
    this.selectedDevice = null;
    this.deviceInfoVisible = false;
  }

  /**
   * open keyholder info
   * @param keyholder keyholder
   */
  openKeyholderInfo(keyholder: AppKeyholder) {
    this.selectedKeyholder = keyholder;
    this.keyholderInfoVisible = true;
  }

  /**
   * close keyholder info
   */
  closeKeyholderInfo() {
    this.selectedKeyholder = null;
    this.keyholderInfoVisible = false;
  }
}
