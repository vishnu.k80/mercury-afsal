import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { TableBaseComponent } from '../../common/table/table-base.component';
import { AppSpace } from '../../../../models/data/app-space';
import { TableColumn } from '../../../../models/common/table-column';
import { AppAssociationType } from '../../../../models/data/app-associations';
import { cloneDeep } from '../../../../utils/lodash.util';
import { AppAssociatedDevice } from '../../../../models/data/app-device';
import { AppAssociatedKeyholder } from '../../../../models/data/app-user';
import { SideModalSpaceInfoComponent } from '../side-modal-space-info/side-modal-space-info.component';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../utils/subscribe.util';
import { SpaceService } from '../../../../services/space.service';
import { MessageService } from '../../../common/message/services/message.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { appStoreSelectors } from '../../../../stores/app-store/app-store.selectors';
import { map } from 'rxjs/operators';

export type AppSpaceAssociationType = 'device' | 'keyholder';

interface SpaceTableSubscription extends SubscriptionKeys {
  deleteSpace: SubscriptionItem;
}

@Component({
  selector: 'app-space-table',
  templateUrl: './space-table.component.html',
  styleUrls: ['../../common/table/table-base.component.scss', './space-table.component.scss']
})
export class SpaceTableComponent extends TableBaseComponent<AppSpace> implements OnInit, OnDestroy {
  // admin deleted
  @Output() deleted: EventEmitter<void> = new EventEmitter<void>();
  // delete undo
  @Output() deleteUndo: EventEmitter<void> = new EventEmitter<void>();
  // space info modal
  @ViewChild(SideModalSpaceInfoComponent) sideModalSpaceInfo: SideModalSpaceInfoComponent;
  // selected space
  selectedSpace: AppSpace;
  // add associations visible state
  addAssociationVisible = false;
  // associations
  associations: AppAssociationType[] = [];
  // association type
  associationType: AppSpaceAssociationType;
  // space info visible state
  spaceInfoVisible = false;
  // delete modal visible
  deleteModalVisible = false;
  // access history visible state
  accessHistoryVisible = false;
  // delete loading
  deleteLoading = false;
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.spaceEditPermission);
  // access history read permission
  accessHistoryReadPermission$: Observable<boolean> = this.store.select(appStoreSelectors.accessHistoryReadPermission);
  // inventory
  private inventory: SubscriptionInventory<SpaceTableSubscription> = new SubscriptionInventory<
    SpaceTableSubscription
  >();
  // Load more label
  loadMoreLable = 'spaces';
  constructor(private store: Store, private spaceService: SpaceService, private messageService: MessageService) {
    super();

    this.columns = [
      new TableColumn('id', 'ID'),
      new TableColumn('name', 'NAME', true),
      new TableColumn('associatedDevices', 'ASSOCIATED DEVICES'),
      new TableColumn('associatedKeyholders', 'ASSOCIATED KEYHOLDERS'),
      new TableColumn('state', 'STATE'),
      new TableColumn('lastAccess', 'LAST ACCESSED BY'),
      new TableColumn('map', '')
    ];
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  get readOnly$() {
    return this.editPermission$.pipe(map((permission) => !permission));
  }

  /**
   * open add association
   * @param space space
   * @param associations associations
   * @param type type
   */
  openAddAssociation(space: AppSpace, associations: AppAssociationType[], type: AppSpaceAssociationType) {
    this.selectedSpace = space;
    this.associations = associations;
    this.addAssociationVisible = true;
    this.associationType = type;
  }

  /**
   * close add association
   */
  closeAddAssociation() {
    this.selectedSpace = null;
    this.associations = [];
    this.addAssociationVisible = false;
    this.associationType = null;
  }

  /**
   * update associations by type
   * @param associations associations
   */
  onAssociationApplied(associations: AppAssociationType[]) {
    switch (this.associationType) {
      case 'device': {
        this.applyDeviceAssociations(associations);
        break;
      }

      case 'keyholder': {
        this.applyKeyholderAssociations(associations);
        break;
      }
    }
  }

  /**
   * apply device associations
   * @param associations associations
   */
  private applyDeviceAssociations(associations: AppAssociationType[]) {
    this.selectedSpace.associatedDevices = associations as AppAssociatedDevice[];
    this.data = cloneDeep(this.data);
    this.closeAddAssociation();
  }

  /**
   * apply keyholder associations
   * @param associations associations
   */
  private applyKeyholderAssociations(associations: AppAssociationType[]) {
    this.selectedSpace.associatedKeyholders = associations as AppAssociatedKeyholder[];
    this.data = cloneDeep(this.data);
    this.closeAddAssociation();
  }

  /**
   * open space info
   * @param space space
   */
  openSpaceInfo(space: AppSpace) {
    this.selectedSpace = space;
    this.spaceInfoVisible = true;
  }

  /**
   * close space info
   */
  closeSpaceInfo() {
    this.selectedSpace = null;
    this.spaceInfoVisible = false;
  }

  /**
   * open space edit
   * @param space space
   */
  openSpaceEdit(space: AppSpace) {
    this.openSpaceInfo(space);

    setTimeout(() => {
      if (this.sideModalSpaceInfo) {
        this.sideModalSpaceInfo.toEdit();
      }
    });
  }

  /**
   * delete space
   */
  deleteSpace() {
    this.inventory.unSubscribe('deleteSpace');
    this.deleteLoading = true;

    const space = this.selectedSpace;
    const sub = this.spaceService.deleteSpace(space).subscribe({
      next: () => {
        this.closeDeleteConfirmModal();
        this.deleted.emit();
        this.deleteLoading = false;
        this.messageService.open('default', `Space ${space.id} has been deleted`, true, () => {
          this.deleteUndo.emit();
        });
      },
      error: (err) => {
        this.closeDeleteConfirmModal();
        this.messageService.open('error', err.message);
        this.deleteLoading = false;
      }
    });

    this.inventory.store('deleteSpace', sub);
  }

  /**
   * close delete modal
   */
  closeDeleteConfirmModal() {
    this.selectedSpace = null;
    this.deleteModalVisible = false;
  }

  /**
   * open delete modal
   * @param space space to delete
   */
  openDeleteConfirmModal(space: AppSpace) {
    this.selectedSpace = space;
    this.deleteModalVisible = true;
  }

  /**
   * open access history
   * @param space space
   */
  openAccessHistory(space: AppSpace) {
    this.selectedSpace = space;
    this.accessHistoryVisible = true;
  }

  /**
   * close access history
   */
  closeAccessHistory() {
    this.selectedSpace = null;
    this.accessHistoryVisible = false;
  }
}
