import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpaceTableComponent } from './space-table.component';
import { TableModule } from '../../../common/table/table.module';
import { TooltipModule } from '../../../common/tooltip/tooltip.module';
import { AssociationsModule } from '../../../common/associations/associations.module';
import { StateLabelModule } from '../../../common/state-label/state-label.module';
import { UserInfoModule } from '../../../common/user-info/user-info.module';
import { IconMapModule } from '../../../common/icons/icon-map/icon-map.module';
import { MoreOptionsModule } from '../../../common/more-options/more-options.module';
import { IconButtonModule } from '../../../common/buttons/icon-button/icon-button.module';
import { IconPencilModule } from '../../../common/icons/icon-pencil/icon-pencil.module';
import { IconDeleteModule } from '../../../common/icons/icon-delete/icon-delete.module';
import { SideModalAddAssociationModule } from '../../common/side-modal-add-association/side-modal-add-association.module';
import { SideModalSpaceInfoModule } from '../side-modal-space-info/side-modal-space-info.module';
import { DeleteModalModule } from '../../../common/delete-modal/delete-modal.module';
import { SideModalAccessHistoryModule } from '../../common/side-modal-access-history/side-modal-access-history.module';

@NgModule({
  declarations: [SpaceTableComponent],
  exports: [SpaceTableComponent],
  imports: [
    CommonModule,
    TableModule,
    TooltipModule,
    AssociationsModule,
    StateLabelModule,
    UserInfoModule,
    IconMapModule,
    MoreOptionsModule,
    IconButtonModule,
    IconPencilModule,
    IconDeleteModule,
    SideModalAddAssociationModule,
    SideModalSpaceInfoModule,
    DeleteModalModule,
    SideModalAccessHistoryModule
  ]
})
export class SpaceTableModule {}
