import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { SideModalNewKeyholderView } from '../../keyholders/side-modal-new-keyholder/side-modal-new-keyholder.component';

@Component({
  selector: 'app-side-modal-create-space',
  templateUrl: './side-modal-create-space.component.html',
  styleUrls: ['./side-modal-create-space.component.scss']
})
export class SideModalCreateSpaceComponent implements OnInit {
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // created
  @Output() created: EventEmitter<void> = new EventEmitter<void>();
  // view type
  view: SideModalNewKeyholderView = 'default';

  constructor() {}

  ngOnInit(): void {}

  /**
   * on submitted
   */
  onSubmitted() {
    this.created.emit();
    this.view = 'success';
  }

  /**
   * go to default view
   */
  onAddNewClicked() {
    this.view = 'default';
  }
}
