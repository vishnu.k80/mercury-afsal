import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSpaceSuccessComponent } from './create-space-success.component';

describe('CreateSpaceSuccessComponent', () => {
  let component: CreateSpaceSuccessComponent;
  let fixture: ComponentFixture<CreateSpaceSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateSpaceSuccessComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSpaceSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
