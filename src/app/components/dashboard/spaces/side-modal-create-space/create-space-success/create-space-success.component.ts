import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-create-space-success',
  templateUrl: './create-space-success.component.html',
  styleUrls: ['../common/create-space-base.component.scss', './create-space-success.component.scss']
})
export class CreateSpaceSuccessComponent implements OnInit {
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // add new click
  @Output() addNewClick: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}
}
