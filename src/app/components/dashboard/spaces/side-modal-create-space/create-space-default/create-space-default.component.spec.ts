import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSpaceDefaultComponent } from './create-space-default.component';

describe('CreateSpaceDefaultComponent', () => {
  let component: CreateSpaceDefaultComponent;
  let fixture: ComponentFixture<CreateSpaceDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateSpaceDefaultComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSpaceDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
