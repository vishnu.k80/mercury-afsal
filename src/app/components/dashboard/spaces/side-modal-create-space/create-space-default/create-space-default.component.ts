import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../../utils/subscribe.util';
import { MessageService } from '../../../../common/message/services/message.service';
import { NewKeyholderData } from '../../../keyholders/side-modal-new-keyholder/new-keyholder-default/new-keyholder-default.component';
import { SpaceService } from '../../../../../services/space.service';

export interface CreateSpaceData {
  name: string;
}

interface CreateSpaceDefaultSubscriptions extends SubscriptionKeys {
  createSpace: SubscriptionItem;
}

@Component({
  selector: 'app-create-space-default',
  templateUrl: './create-space-default.component.html',
  styleUrls: ['../common/create-space-base.component.scss', './create-space-default.component.scss']
})
export class CreateSpaceDefaultComponent implements OnInit, OnDestroy {
  // set data
  @Input() set data(data: NewKeyholderData) {
    this.setFieldValues(data);
  }

  // submitted
  @Output() submitted: EventEmitter<void> = new EventEmitter<void>();
  // cancel click
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();
  // name
  name: FormControl = new FormControl('', Validators.required);
  // name errors
  nameErrors = {
    required: 'The name is required'
  };
  // loading
  loading = false;
  // inventory
  private inventory: SubscriptionInventory<CreateSpaceDefaultSubscriptions> = new SubscriptionInventory<
    CreateSpaceDefaultSubscriptions
  >();

  constructor(private spaceService: SpaceService, private messageService: MessageService) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * set field values
   * @param data data
   */
  private setFieldValues(data: NewKeyholderData) {
    if (data) {
      this.name.setValue(data.name);
    }
  }

  /**
   * submit data
   */
  submit() {
    this.name.markAsTouched();

    if (this.name.invalid) {
      return;
    }

    this.inventory.unSubscribe('createSpace');
    this.loading = true;

    const sub = this.spaceService
      .createSpace({
        name: this.name.value
      })
      .subscribe({
        next: () => {
          this.submitted.emit();
        },
        error: (err) => {
          this.messageService.open('error', err.message);
          this.loading = false;
        }
      });

    this.inventory.store('createSpace', sub);
  }
}
