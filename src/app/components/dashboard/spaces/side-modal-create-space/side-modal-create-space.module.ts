import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalCreateSpaceComponent } from './side-modal-create-space.component';
import { CreateSpaceDefaultComponent } from './create-space-default/create-space-default.component';
import { CreateSpaceSuccessComponent } from './create-space-success/create-space-success.component';
import { SideModalModule } from '../../common/side-modal/side-modal.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { FormFieldModule } from '../../../common/fields/form-field/form-field.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { InlineButtonModule } from '../../../common/buttons/inline-button/inline-button.module';
import { IconVerifiedModule } from '../../../common/icons/icon-verified/icon-verified.module';
import { OutlinedButtonModule } from '../../../common/buttons/outlined-button/outlined-button.module';
import { IconPackageModule } from '../../../common/icons/icon-package/icon-package.module';

@NgModule({
  declarations: [SideModalCreateSpaceComponent, CreateSpaceDefaultComponent, CreateSpaceSuccessComponent],
  exports: [SideModalCreateSpaceComponent],
  imports: [
    CommonModule,
    SideModalModule,
    CustomScrollbarModule,
    FormFieldModule,
    SpinnerModule,
    FlatButtonModule,
    InlineButtonModule,
    IconVerifiedModule,
    OutlinedButtonModule,
    IconPackageModule
  ]
})
export class SideModalCreateSpaceModule {}
