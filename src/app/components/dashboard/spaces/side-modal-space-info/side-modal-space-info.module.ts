import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalSpaceInfoComponent } from './side-modal-space-info.component';
import { SpaceInfoDefaultComponent } from './space-info-default/space-info-default.component';
import { SpaceInfoEditComponent } from './space-info-edit/space-info-edit.component';
import { SideModalModule } from '../../common/side-modal/side-modal.module';
import { SideSpaceInfoModule } from '../../common/side-space-info/side-space-info.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { InlineButtonModule } from '../../../common/buttons/inline-button/inline-button.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { LabeledValueModule } from '../../../common/labeled-value/labeled-value.module';
import { FormFieldModule } from '../../../common/fields/form-field/form-field.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { StateLabelModule } from '../../../common/state-label/state-label.module';

@NgModule({
  declarations: [SideModalSpaceInfoComponent, SpaceInfoDefaultComponent, SpaceInfoEditComponent],
  exports: [SideModalSpaceInfoComponent],
  imports: [
    CommonModule,
    SideModalModule,
    SideSpaceInfoModule,
    FlatButtonModule,
    InlineButtonModule,
    CustomScrollbarModule,
    LabeledValueModule,
    FormFieldModule,
    SpinnerModule,
    StateLabelModule
  ]
})
export class SideModalSpaceInfoModule {}
