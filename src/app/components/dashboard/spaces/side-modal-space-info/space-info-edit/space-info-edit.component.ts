import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppSpace } from '../../../../../models/data/app-space';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../../utils/subscribe.util';
import { SpaceService } from '../../../../../services/space.service';
import { MessageService } from '../../../../common/message/services/message.service';
import { Store } from '@ngrx/store';
import { OptionItem } from '../../../../../models/common/option-item';
import { Observable } from 'rxjs';
import { AppStatus } from '../../../../../models/data/app-status';
import { appStoreSelectors } from '../../../../../stores/app-store/app-store.selectors';
import { SPACE_ACTIVE_STATE, SPACE_DISABLED_STATE, SPACE_INACTIVE_STATE } from '../../../../../utils/constants.util';

interface SpaceEditSubscription extends SubscriptionKeys {
  editDevice: SubscriptionItem;
}

@Component({
  selector: 'app-space-info-edit',
  templateUrl: './space-info-edit.component.html',
  styleUrls: ['../../../common/info/info-base.component.scss', './space-info-edit.component.scss']
})
export class SpaceInfoEditComponent implements OnInit {
  // space
  @Input() set space(space: AppSpace) {
    this.setFieldValues(space);
  }

  // saved
  @Output() saved: EventEmitter<AppSpace> = new EventEmitter<AppSpace>();
  // cancel click
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();
  // loading
  loading = false;
  // name
  name: FormControl = new FormControl('', Validators.required);
  // state
  state: FormControl = new FormControl('');
  // disabled reason
  disabledReason: FormControl = new FormControl('');
  // status
  status: FormControl = new FormControl('');
  // name errors
  nameErrors = {
    required: 'The name is required'
  };
  // disabled reason errors
  disabledReasonErrors = {
    required: 'The disabled reason is required'
  };
  // state options
  stateOptions: OptionItem[] = [
    new OptionItem<string>(SPACE_ACTIVE_STATE, SPACE_ACTIVE_STATE),
    new OptionItem<string>(SPACE_INACTIVE_STATE, SPACE_INACTIVE_STATE),
    new OptionItem<string>(SPACE_DISABLED_STATE, SPACE_DISABLED_STATE)
  ];
  // statuses stream
  statuses$: Observable<AppStatus[]> = this.store.select(appStoreSelectors.statuses);
  // options
  statusOptions: OptionItem[] = [];
  // form group
  private formGroup: FormGroup = new FormGroup({
    name: this.name,
    state: this.state,
    disabledReason: this.disabledReason,
    status: this.status
  });
  // inventory
  private inventory: SubscriptionInventory<SpaceEditSubscription> = new SubscriptionInventory<SpaceEditSubscription>();

  constructor(private store: Store, private spaceService: SpaceService, private messageService: MessageService) {
    this.disabledReasonValidator = this.disabledReasonValidator.bind(this);
    this.disabledReason.setValidators(this.disabledReasonValidator);
    this.getStatuses();
  }

  ngOnInit(): void {
    this.subscribeStateChange();
  }

  /**
   * get statuses
   */
  getStatuses() {
    const sub = this.statuses$.subscribe({
      next: (statuses) => {
        this.statusOptions = statuses
          .filter((status) => status.group === 'space' && status.use)
          .map((status) => new OptionItem(status.label, status.label));
      }
    });

    this.inventory.store('statuses$', sub);
  }

  /**
   * subscribe state change
   */
  private subscribeStateChange() {
    this.state.valueChanges.subscribe({
      next: () => {
        this.validateDisabledReason();
      }
    });
  }

  /**
   * save changed data
   */
  onSaveClicked() {
    // check validations
    this.formGroup.markAllAsTouched();
    this.validateDisabledReason();

    if (this.formGroup.invalid) {
      return;
    }

    this.inventory.unSubscribe('editSpace');
    this.loading = true;

    const sub = this.spaceService
      .updateSpace(this.name.value, this.state.value, this.status.value, this.disabledReason.value)
      .subscribe({
        next: (space) => {
          this.messageService.open('default', 'Space updated');
          this.saved.emit(space as AppSpace);
        },
        error: (err) => {
          this.messageService.open('error', err.message);
          this.loading = false;
        }
      });

    this.inventory.store('editSpace', sub);
  }

  /**
   * set field values
   */
  private setFieldValues(space: AppSpace) {
    if (space) {
      this.name.setValue(space.name);
      this.state.setValue(space.state);
      this.disabledReason.setValue(space.disabledReason);
      this.status.setValue(space.status);
    }
  }

  /**
   * disabled reason validator
   * @param control control
   */
  disabledReasonValidator(control: AbstractControl): null | any {
    if (this.state.value === SPACE_DISABLED_STATE) {
      if (!control.value) {
        return {
          required: {
            value: control.value
          }
        };
      }
    }
  }

  /**
   * validate disabled reason
   */
  private validateDisabledReason() {
    if (this.state.value === SPACE_DISABLED_STATE) {
      if (!this.disabledReason.value) {
        this.disabledReason.setErrors({
          required: {
            value: this.disabledReason.value
          }
        });
      }
    } else {
      this.disabledReason.setErrors(null);
    }
  }
}
