import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaceInfoEditComponent } from './space-info-edit.component';

describe('SpaceInfoEditComponent', () => {
  let component: SpaceInfoEditComponent;
  let fixture: ComponentFixture<SpaceInfoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SpaceInfoEditComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaceInfoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
