import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { InfoBaseComponent } from '../../common/info/info-base.component';
import { AppSpace } from '../../../../models/data/app-space';

@Component({
  selector: 'app-side-modal-space-info',
  templateUrl: './side-modal-space-info.component.html',
  styleUrls: ['./side-modal-space-info.component.scss']
})
export class SideModalSpaceInfoComponent extends InfoBaseComponent implements OnInit {
  // space
  @Input() space: AppSpace;

  constructor(protected changeDetectorRef: ChangeDetectorRef) {
    super(changeDetectorRef);
  }

  ngOnInit(): void {}

  /**
   * update space data when space saved
   * @param space new space data
   */
  onSpaceSaved(space: AppSpace) {
    this.space.name = space.name;
    this.space.state = space.state;
    this.space.status = space.status;
    this.space.disabledReason = space.disabledReason;

    this.toDefault();
    this.updateInfoHeight();
    this.emitUpdated();
  }
}
