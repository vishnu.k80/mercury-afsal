import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalSpaceInfoComponent } from './side-modal-space-info.component';

describe('SideModalSpaceInfoComponent', () => {
  let component: SideModalSpaceInfoComponent;
  let fixture: ComponentFixture<SideModalSpaceInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalSpaceInfoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalSpaceInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
