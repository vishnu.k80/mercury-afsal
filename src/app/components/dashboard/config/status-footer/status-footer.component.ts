import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-status-footer',
  templateUrl: './status-footer.component.html',
  styleUrls: ['./status-footer.component.scss']
})
export class StatusFooterComponent implements OnInit {
  // add click
  @Output() addClick: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}
}
