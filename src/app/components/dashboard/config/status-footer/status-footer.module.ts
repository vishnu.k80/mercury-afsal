import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusFooterComponent } from './status-footer.component';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';

@NgModule({
  declarations: [StatusFooterComponent],
  exports: [StatusFooterComponent],
  imports: [CommonModule, FlatButtonModule]
})
export class StatusFooterModule {}
