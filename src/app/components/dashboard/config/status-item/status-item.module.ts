import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusItemComponent } from './status-item.component';
import { IconPencilModule } from '../../../common/icons/icon-pencil/icon-pencil.module';
import { IconDeleteModule } from '../../../common/icons/icon-delete/icon-delete.module';

@NgModule({
  declarations: [StatusItemComponent],
  exports: [StatusItemComponent],
  imports: [CommonModule, IconPencilModule, IconDeleteModule]
})
export class StatusItemModule {}
