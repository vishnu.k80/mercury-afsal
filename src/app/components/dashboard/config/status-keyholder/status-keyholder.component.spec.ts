import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusKeyholderComponent } from './status-keyholder.component';

describe('StatusKeyholderComponent', () => {
  let component: StatusKeyholderComponent;
  let fixture: ComponentFixture<StatusKeyholderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StatusKeyholderComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusKeyholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
