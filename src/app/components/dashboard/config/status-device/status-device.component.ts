import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { StatusBaseComponent } from '../status-common/status-base.component';
import { Store } from '@ngrx/store';
import { ToggleService } from 'src/app/services/common/toggle.service';

@Component({
  selector: 'app-status-device',
  templateUrl: './status-device.component.html',
  styleUrls: ['../status-common/status-base.component.scss', './status-device.component.scss']
})
export class StatusDeviceComponent extends StatusBaseComponent implements OnInit {
  configRouteChange = true;
  constructor(
    protected store: Store,
    protected changeDetectorRef: ChangeDetectorRef,
    private toggleService: ToggleService
  ) {
    super(store, changeDetectorRef);
  }

  ngOnInit(): void {
    this.mobileChecking();
  }
  backConfig() {
    this.configRouteChange = false;
  }
  mobileChecking() {
    if (window.innerWidth <= 678) {
      this.toggleService.configStatus.subscribe((val) => {
        this.configRouteChange = val;
      });
    }
  }
}
