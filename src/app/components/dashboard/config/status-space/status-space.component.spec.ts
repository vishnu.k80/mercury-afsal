import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusSpaceComponent } from './status-space.component';

describe('StatusSpaceComponent', () => {
  let component: StatusSpaceComponent;
  let fixture: ComponentFixture<StatusSpaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StatusSpaceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusSpaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
