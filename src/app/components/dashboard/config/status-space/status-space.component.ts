import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { StatusBaseComponent } from '../status-common/status-base.component';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

@Component({
  selector: 'app-status-space',
  templateUrl: './status-space.component.html',
  styleUrls: ['../status-common/status-base.component.scss', './status-space.component.scss']
})
export class StatusSpaceComponent extends StatusBaseComponent implements OnInit {
  constructor(private router: Router, protected store: Store, protected changeDetectorRef: ChangeDetectorRef) {
    super(store, changeDetectorRef);
  }

  ngOnInit(): void {}

  backConfig() {
    this.router.navigate(['dashboard/config']);
  }
}
