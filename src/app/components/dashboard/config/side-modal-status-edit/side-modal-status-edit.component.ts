import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AppStatus, AppStatusGroup } from '../../../../models/data/app-status';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StatusService } from '../../../../services/status.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../utils/subscribe.util';
import { MessageService } from '../../../common/message/services/message.service';

interface EditSubscriptions extends SubscriptionKeys {
  updateStatus: SubscriptionItem;
  createStatus: SubscriptionItem;
}

@Component({
  selector: 'app-side-modal-status-edit',
  templateUrl: './side-modal-status-edit.component.html',
  styleUrls: ['./side-modal-status-edit.component.scss']
})
export class SideModalStatusEditComponent implements OnInit, OnDestroy {
  // set status
  @Input() set status(status: AppStatus) {
    this.setFieldValues(status);
  }
  // mode
  @Input() mode: 'edit' | 'create';
  // status group
  @Input() group: AppStatusGroup;
  // updated
  @Output() updated: EventEmitter<AppStatus> = new EventEmitter<AppStatus>();
  // created
  @Output() created: EventEmitter<AppStatus> = new EventEmitter<AppStatus>();
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // name
  name: FormControl = new FormControl('', Validators.required);
  // description
  description: FormControl = new FormControl('', Validators.required);
  // use state
  use = false;
  // is default
  isDefault = false;
  // name errors
  nameErrors = {
    required: 'The name is required'
  };
  // description errors
  descriptionErrors = {
    required: 'The description is required'
  };
  // form group
  formGroup: FormGroup = new FormGroup({
    name: this.name,
    description: this.description
  });
  // loading state
  loading = false;
  // inventory
  private inventory: SubscriptionInventory<EditSubscriptions> = new SubscriptionInventory<EditSubscriptions>();

  constructor(private statusService: StatusService, private messageService: MessageService) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * toggle default state
   */
  toggleDefault() {
    this.isDefault = !this.isDefault;
  }

  /**
   * set use state
   * @param state state
   */
  setUse(state: boolean) {
    this.use = state;
  }

  /**
   * emit save click
   */
  emitSaveClick() {
    this.formGroup.markAllAsTouched();

    if (this.formGroup.invalid) {
      return;
    }

    if (this.mode === 'edit') {
      this.updateStatus();
    } else {
      this.createStatus();
    }
  }

  /**
   * set field values
   * @param status status
   */
  private setFieldValues(status: AppStatus) {
    if (status) {
      this.name.setValue(status.label);
      this.description.setValue(status.description);
      this.use = status.use;
      this.isDefault = status.default;
    }
  }

  /**
   * update status
   */
  private updateStatus() {
    this.inventory.unSubscribe('updateStatus');
    this.loading = true;

    const sub = this.statusService
      .updateStatus({
        label: this.name.value,
        description: this.description.value,
        group: this.group,
        use: this.use,
        default: this.isDefault
      })
      .subscribe({
        next: (status) => {
          this.messageService.open('default', 'Status updated');
          this.updated.emit(status);
        },
        error: (err) => {
          this.messageService.open('error', err.message);
          this.loading = false;
        }
      });

    this.inventory.store('updateStatus', sub);
  }

  /**
   * create status
   */
  private createStatus() {
    this.inventory.unSubscribe('createStatus');
    this.loading = true;

    const sub = this.statusService
      .createStatus({
        label: this.name.value,
        description: this.description.value,
        group: this.group,
        use: this.use,
        default: this.isDefault
      })
      .subscribe({
        next: (status) => {
          this.messageService.open('default', 'Status created');
          this.created.emit(status);
        },
        error: (err) => {
          this.messageService.open('error', err.message);
          this.loading = false;
        }
      });

    this.inventory.store('createStatus', sub);
  }
}
