import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-config-header',
  templateUrl: './config-header.component.html',
  styleUrls: ['./config-header.component.scss']
})
export class ConfigHeaderComponent implements OnInit {
  // read only state
  @Input() readOnly = false;
  // reset click
  @Output() resetClick: EventEmitter<void> = new EventEmitter<void>();
  // apply click
  @Output() applyClick: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}
}
