import { Component, OnInit } from '@angular/core';
import { ToggleService } from 'src/app/services/common/toggle.service';

@Component({
  selector: 'app-config-navigation',
  templateUrl: './config-navigation.component.html',
  styleUrls: ['./config-navigation.component.scss']
})
export class ConfigNavigationComponent implements OnInit {
  // navigations
  navigations = [
    [
      {
        label: 'Device Status',
        route: 'device'
      },
      {
        label: 'Administrator Status',
        route: 'admin'
      },
      {
        label: 'Keyholder Status',
        route: 'keyholder'
      },
      {
        label: 'Space Status',
        route: 'space'
      }
    ]
  ];

  constructor(private toggleService: ToggleService) {}

  ngOnInit(): void {}
  statusOpen(nav) {
    if (nav.route === 'device') {
      this.toggleService.configStatus.next(true);
    } else {
      this.toggleService.configStatus.next(false);
    }
  }
}
