import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigNavigationComponent } from './config-navigation.component';

describe('ConfigNavigationComponent', () => {
  let component: ConfigNavigationComponent;
  let fixture: ComponentFixture<ConfigNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConfigNavigationComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
