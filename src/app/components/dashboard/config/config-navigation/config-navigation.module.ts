import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigNavigationComponent } from './config-navigation.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ConfigNavigationComponent],
  exports: [ConfigNavigationComponent],
  imports: [CommonModule, RouterModule]
})
export class ConfigNavigationModule {}
