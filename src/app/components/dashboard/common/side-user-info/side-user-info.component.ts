import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { AppUser } from '../../../../models/data/app-user';
import { getElement } from '../../../../utils/element.util';
import { toDataUrl } from '../../../../utils/file.util';
import { SideInfoBaseComponent } from '../side-info-common/side-info-base.component';

@Component({
  selector: 'app-side-user-info',
  templateUrl: './side-user-info.component.html',
  styleUrls: ['../side-info-common/side-info-base.component.scss', './side-user-info.component.scss']
})
export class SideUserInfoComponent extends SideInfoBaseComponent implements OnInit {
  // user
  @Input() user: AppUser;
  // read only state
  @Input() readOnly = false;
  // uploader
  @ViewChild('uploader') uploader: ElementRef<HTMLInputElement>;

  constructor(protected elementRef: ElementRef<HTMLElement>) {
    super(elementRef);
  }

  ngOnInit(): void {}

  /**
   * open uploader
   */
  onUploadButtonClicked() {
    const uploader = getElement(this.uploader);

    if (uploader) {
      uploader.click();
    }
  }

  /**
   * handle file change
   * @param event event
   */
  onFileChange(event: Event) {
    const target = event.target as HTMLInputElement;
    const file = target.files[0];

    if (file) {
      this.processUploadedImage(file);
    }

    target.value = null;
  }

  /**
   * process uploaded image
   * @param file file
   */
  private processUploadedImage(file: File) {
    // create data url with file
    toDataUrl(file).then((url) => {
      // update avatar
      this.user.avatar = url;
    });
  }
}
