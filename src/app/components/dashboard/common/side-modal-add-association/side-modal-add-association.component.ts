import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { AppUser } from '../../../../models/data/app-user';
import { AppDevice } from '../../../../models/data/app-device';
import { AppSpace } from '../../../../models/data/app-space';
import { cloneDeep } from '../../../../utils/lodash.util';
import { KeyholderService } from '../../../../services/keyholder.service';
import { MessageService } from '../../../common/message/services/message.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../utils/subscribe.util';
import { AppAssociationType } from '../../../../models/data/app-associations';
import { SideModalContentComponent } from '../side-modal/side-modal-content/side-modal-content.component';
import { getElement } from '../../../../utils/element.util';
import { DeviceService } from '../../../../services/device.service';
import { SpaceService } from '../../../../services/space.service';

export type SideModalAddAssociationType = 'keyholder' | 'device' | 'space';

interface AddAssociationSubscriptions extends SubscriptionKeys {
  add: SubscriptionItem;
}

@Component({
  selector: 'app-side-modal-add-association',
  templateUrl: './side-modal-add-association.component.html',
  styleUrls: ['./side-modal-add-association.component.scss']
})
export class SideModalAddAssociationComponent implements OnInit {
  // user
  @Input() user: AppUser;
  // device
  @Input() device: AppDevice;
  // space
  @Input() space: AppSpace;
  // association type
  @Input() type: SideModalAddAssociationType;
  // associations
  @Input() set associations(associations: AppAssociationType[]) {
    this.associationData = cloneDeep(associations);
  }
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // applied
  @Output() applied: EventEmitter<AppAssociationType[]> = new EventEmitter<AppAssociationType[]>();
  // side modal content
  @ViewChild(SideModalContentComponent, { read: ElementRef }) sideModalContent: ElementRef<HTMLElement>;
  // association data
  associationData: AppAssociationType[] = [];
  // loading
  loading = false;
  // inventory
  private inventory: SubscriptionInventory<AddAssociationSubscriptions> = new SubscriptionInventory<
    AddAssociationSubscriptions
  >();

  constructor(
    private renderer: Renderer2,
    private keyholderService: KeyholderService,
    private deviceService: DeviceService,
    private spaceService: SpaceService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {}

  /**
   * emit close click
   */
  emitCloseClick() {
    this.closeClick.emit();
  }

  /**
   * remove item from the associations
   * @param item item
   */
  onRemoveClicked(item: AppAssociationType) {
    this.associationData = this.associationData.filter((data) => data !== item);
  }

  /**
   * add item to the associations
   * @param item item
   */
  onAddClicked(item: AppAssociationType) {
    this.associationData = [...this.associationData, item];
  }

  /**
   * set content height when info init
   * @param info info component
   */
  onInfoInit(info: HTMLElement) {
    setTimeout(() => {
      const content = getElement(this.sideModalContent);

      if (content && info) {
        this.renderer.setStyle(content, 'height', `calc(100% - ${info.offsetHeight}px - 58px - 78px)`);
      }
    });
  }

  /**
   * apply clicked
   */
  onApplyClicked() {
    if (this.user) {
      switch (this.type) {
        case 'device': {
          // add keyholder to devices
          this.addKeyholderToDevices();
          break;
        }

        case 'space': {
          // add keyholder to spaces
          this.addKeyholderToSpaces();
          break;
        }
      }
    } else if (this.device) {
      switch (this.type) {
        case 'space': {
          this.addDeviceToSpaces();
          break;
        }

        default: {
          break;
        }
      }
    } else if (this.space) {
      switch (this.type) {
        case 'device': {
          this.addSpaceToDevices();
          break;
        }

        case 'keyholder': {
          this.addSpaceToKeyholders();
          break;
        }
      }
    }
  }

  /**
   * add keyholder to devices
   */
  private addKeyholderToDevices() {
    this.inventory.unSubscribe('add');
    this.loading = true;

    const sub = this.keyholderService.addDeviceAssociations().subscribe({
      next: () => {
        this.messageService.open('default', 'Associated device has been successfully updated');
        this.loading = false;
        this.emitApplied();
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('add', sub);
  }

  /**
   * add keyholder to spaces
   */
  private addKeyholderToSpaces() {
    this.inventory.unSubscribe('add');
    this.loading = true;

    const sub = this.keyholderService.addSpaceAssociations().subscribe({
      next: () => {
        this.messageService.open('default', 'Associated Space has been successfully updated');
        this.loading = false;
        this.emitApplied();
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('add', sub);
  }

  /**
   * add device to spaces
   */
  private addDeviceToSpaces() {
    this.inventory.unSubscribe('add');
    this.loading = true;

    const sub = this.deviceService.addSpaceAssociations().subscribe({
      next: () => {
        this.messageService.open('default', 'Associated space has been successfully updated');
        this.loading = false;
        this.emitApplied();
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('add', sub);
  }

  /**
   * add space to devices
   */
  private addSpaceToDevices() {
    this.inventory.unSubscribe('add');
    this.loading = true;

    const sub = this.spaceService.addKeyholderAssociations().subscribe({
      next: () => {
        this.messageService.open('default', 'Associated keyholder has been successfully updated');
        this.loading = false;
        this.emitApplied();
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('add', sub);
  }

  /**
   * add space to keyholders
   */
  private addSpaceToKeyholders() {
    this.inventory.unSubscribe('add');
    this.loading = true;

    const sub = this.spaceService.addDeviceAssociations().subscribe({
      next: () => {
        this.messageService.open('default', 'Associated keyholder has been successfully updated');
        this.loading = false;
        this.emitApplied();
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('add', sub);
  }

  /**
   * emit applied with association data
   */
  emitApplied() {
    this.applied.emit(this.associationData);
  }
}
