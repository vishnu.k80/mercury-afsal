import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalContentComponent } from './side-modal-content.component';

describe('SideModalContentComponent', () => {
  let component: SideModalContentComponent;
  let fixture: ComponentFixture<SideModalContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalContentComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
