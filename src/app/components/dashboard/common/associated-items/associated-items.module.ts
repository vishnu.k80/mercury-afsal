import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssociatedItemsComponent } from './associated-items.component';
import { IconArrowDropUpModule } from '../../../common/icons/icon-arrow-drop-up/icon-arrow-drop-up.module';
import { IconArrowDropDownModule } from '../../../common/icons/icon-arrow-drop-down/icon-arrow-drop-down.module';
import { IconLockModule } from '../../../common/icons/icon-lock/icon-lock.module';
import { StateLabelModule } from '../../../common/state-label/state-label.module';
import { IconDeleteModule } from '../../../common/icons/icon-delete/icon-delete.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { IconPackageModule } from '../../../common/icons/icon-package/icon-package.module';
import { UserInfoModule } from '../../../common/user-info/user-info.module';

@NgModule({
  declarations: [AssociatedItemsComponent],
  exports: [AssociatedItemsComponent],
  imports: [
    CommonModule,
    IconArrowDropUpModule,
    IconArrowDropDownModule,
    IconLockModule,
    StateLabelModule,
    IconDeleteModule,
    CustomScrollbarModule,
    IconPackageModule,
    UserInfoModule
  ]
})
export class AssociatedItemsModule {}
