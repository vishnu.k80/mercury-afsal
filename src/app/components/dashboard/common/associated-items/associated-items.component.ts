import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppAssociationType } from '../../../../models/data/app-associations';

export type AppAssociatedItemsType = 'keyholder' | 'device' | 'space';

@Component({
  selector: 'app-associated-items',
  templateUrl: './associated-items.component.html',
  styleUrls: ['./associated-items.component.scss']
})
export class AssociatedItemsComponent implements OnInit {
  // items
  @Input() items: AppAssociationType[] = [];
  // type
  @Input() type: AppAssociatedItemsType;
  // remove click
  @Output() removeClick: EventEmitter<AppAssociationType> = new EventEmitter<AppAssociationType>();
  // opened state
  opened = false;

  constructor() {}

  ngOnInit(): void {}

  /**
   * toggle list
   */
  toggle() {
    this.opened = !this.opened;
  }

  /**
   * emit remove click
   * @param item item
   */
  emitRemoveClick(item: AppAssociationType) {
    this.removeClick.emit(item);
  }
}
