import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociatedItemsComponent } from './associated-items.component';

describe('AssociatedItemsComponent', () => {
  let component: AssociatedItemsComponent;
  let fixture: ComponentFixture<AssociatedItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssociatedItemsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociatedItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
