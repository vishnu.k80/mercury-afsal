import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './navigation.component';
import { IconHomeModule } from '../../../common/icons/icon-home/icon-home.module';
import { RouterModule } from '@angular/router';
import { IconAdminModule } from '../../../common/icons/icon-admin/icon-admin.module';
import { IconMobileModule } from '../../../common/icons/icon-mobile/icon-mobile.module';
import { IconLockModule } from '../../../common/icons/icon-lock/icon-lock.module';
import { IconPackageModule } from '../../../common/icons/icon-package/icon-package.module';
import { IconRefreshDocModule } from '../../../common/icons/icon-refresh-doc/icon-refresh-doc.module';
import { IconConfigModule } from '../../../common/icons/icon-config/icon-config.module';

@NgModule({
  declarations: [NavigationComponent],
  exports: [NavigationComponent],
  imports: [
    CommonModule,
    IconHomeModule,
    RouterModule,
    IconAdminModule,
    IconMobileModule,
    IconLockModule,
    IconPackageModule,
    IconRefreshDocModule,
    IconConfigModule
  ]
})
export class NavigationModule {}
