import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppUser } from '../../../../models/data/app-user';
import { appStoreSelectors } from '../../../../stores/app-store/app-store.selectors';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../utils/subscribe.util';
import { ToggleService } from 'src/app/services/common/toggle.service';

interface NavigationSubscriptions extends SubscriptionKeys {
  user$: SubscriptionItem;
}

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnDestroy {
  // user stream
  user$: Observable<AppUser> = this.store.select(appStoreSelectors.user);
  // user
  user: AppUser;
  // inventory
  private inventory: SubscriptionInventory<NavigationSubscriptions> = new SubscriptionInventory<
    NavigationSubscriptions
  >();
  // toogling navigation using subject
  navToogle: boolean;

  constructor(private store: Store, private navToogled: ToggleService) {}
  ngOnInit(): void {
    this.getUser();
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * get user from store
   */
  private getUser() {
    this.inventory.unSubscribe('user$');

    const sub = this.user$.subscribe({
      next: (user) => (this.user = user)
    });

    this.inventory.store('user$', sub);
  }
  navClose() {
    if (window.innerWidth < 768) {
      this.navToogled.toggleNav.next(this.navToogle);
    }
  }
}
