import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideDeviceInfoComponent } from './side-device-info.component';

describe('SideDeviceInfoComponent', () => {
  let component: SideDeviceInfoComponent;
  let fixture: ComponentFixture<SideDeviceInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideDeviceInfoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideDeviceInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
