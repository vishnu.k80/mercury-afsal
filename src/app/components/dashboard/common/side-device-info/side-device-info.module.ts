import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideDeviceInfoComponent } from './side-device-info.component';
import { IconLockModule } from '../../../common/icons/icon-lock/icon-lock.module';

@NgModule({
  declarations: [SideDeviceInfoComponent],
  exports: [SideDeviceInfoComponent],
  imports: [CommonModule, IconLockModule]
})
export class SideDeviceInfoModule {}
