import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { AppDevice } from '../../../../models/data/app-device';
import { SideInfoBaseComponent } from '../side-info-common/side-info-base.component';

@Component({
  selector: 'app-side-device-info',
  templateUrl: './side-device-info.component.html',
  styleUrls: ['../side-info-common/side-info-base.component.scss', './side-device-info.component.scss']
})
export class SideDeviceInfoComponent extends SideInfoBaseComponent implements OnInit {
  // device
  @Input() device: AppDevice;

  constructor(protected elementRef: ElementRef<HTMLElement>) {
    super(elementRef);
  }

  ngOnInit(): void {}
}
