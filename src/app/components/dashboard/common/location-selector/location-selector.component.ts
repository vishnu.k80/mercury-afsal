import { Component, ElementRef, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { AppLocation } from '../../../../models/data/app-location';
import { appStoreSelectors } from '../../../../stores/app-store/app-store.selectors';
import { map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { appStoreActions } from '../../../../stores/app-store/app-store.actions';

@Component({
  selector: 'app-location-selector',
  templateUrl: './location-selector.component.html',
  styleUrls: ['./location-selector.component.scss']
})
export class LocationSelectorComponent implements OnInit {
  // locations stream
  locations$: Observable<AppLocation[]> = this.store.select(appStoreSelectors.locations);
  // selected location
  location$: Observable<AppLocation> = this.store.select(appStoreSelectors.location);
  // search control
  control: FormControl = new FormControl('');
  // opened state
  opened = false;

  constructor(public elementRef: ElementRef<HTMLElement>, private store: Store) {}

  ngOnInit(): void {}

  /**
   * return first letter of location
   * @param location location
   */
  getFirstLetter(location: string) {
    return (location[0] || '').toUpperCase();
  }

  /**
   * return selected location label
   */
  get selectedLocationLabel$(): Observable<string> {
    return this.location$.pipe(map((location) => (location ? location.location : '')));
  }

  /**
   * return selected location's first letter
   */
  get selectedLocationFirstLetter$() {
    return this.selectedLocationLabel$.pipe(map((location) => this.getFirstLetter(location)));
  }

  /**
   * return filtered locations
   */
  get filteredLocations$() {
    return this.locations$.pipe(
      map((locations) => {
        const search = this.control.value;

        return locations.filter((location) => {
          return location.id.search(search) !== -1 || location.location.search(new RegExp(search, 'i')) !== -1;
        });
      })
    );
  }

  /**
   * open options
   */
  open() {
    this.opened = true;
  }

  /**
   * close options and init value
   */
  close() {
    this.opened = false;
    this.control.setValue('');
  }

  /**
   * toggle options
   */
  toggle() {
    if (this.opened) {
      this.close();
    } else {
      this.open();
    }
  }

  /**
   * set clicked location to store and close
   * @param location location
   */
  onLocationClicked(location: AppLocation) {
    this.store.dispatch(appStoreActions.setLocation({ location }));
    this.close();
  }

  /**
   * return true when location is selected location
   * @param item item
   */
  isSelectedLocation(item: AppLocation) {
    return this.location$.pipe(map((location) => location.id === item.id));
  }
}
