import { Component, OnInit } from '@angular/core';
import { ToggleService } from 'src/app/services/common/toggle.service';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { appStoreActions } from 'src/app/stores/app-store/app-store.actions';

@Component({
  selector: 'app-dashboard-header',
  templateUrl: './dashboard-header.component.html',
  styleUrls: ['./dashboard-header.component.scss']
})
export class DashboardHeaderComponent implements OnInit {
  navToggle = false;
  editUserVisible = false;
  constructor(
    private navToogled: ToggleService,
    private store: Store,
    private router: Router,
    private storageService: StorageService
  ) {}
  ngOnInit(): void {
    this.toggleNavigation();
  }

  navOpen() {
    this.navToggle = true;
    this.navToogled.toggleNav.next(this.navToggle);
  }
  navClose() {
    this.navToggle = false;
    this.navToogled.toggleNav.next(this.navToggle);
  }
  toggleNavigation() {
    this.navToogled.toggleNav.subscribe((val) => {
      this.navToggle = val;
    });
  }
  openEdituser() {
    this.editUserVisible = true;
    this.navToogled.toggleNav.next(this.editUserVisible);
  }

  logout() {
    // go to main page
    this.router.navigate(['/main/login']).then(() => {
      // clear user
      this.store.dispatch(appStoreActions.setUser({ user: null }));
      // clear storage user
      this.storageService.localUser = null;
    });
  }
}
