import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastAccessesComponent } from './last-accesses.component';

describe('LastAccessesComponent', () => {
  let component: LastAccessesComponent;
  let fixture: ComponentFixture<LastAccessesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LastAccessesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastAccessesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
