import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LastAccessesComponent } from './last-accesses.component';

@NgModule({
  declarations: [LastAccessesComponent],
  exports: [LastAccessesComponent],
  imports: [CommonModule]
})
export class LastAccessesModule {}
