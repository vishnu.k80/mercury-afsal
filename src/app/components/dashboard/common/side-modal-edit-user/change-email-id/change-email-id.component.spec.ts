import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeEmailIdComponent } from './change-email-id.component';

describe('ChangeEmailIdComponent', () => {
  let component: ChangeEmailIdComponent;
  let fixture: ComponentFixture<ChangeEmailIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChangeEmailIdComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeEmailIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
