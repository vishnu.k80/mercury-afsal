import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AppUser } from '../../../../../models/data/app-user';
import { FormControl, Validators } from '@angular/forms';
import { UserService } from '../../../../../services/user.service';
import { MessageService } from '../../../../common/message/services/message.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../../utils/subscribe.util';

interface ChangeEmailIdSubscriptions extends SubscriptionKeys {
  emailSend: SubscriptionItem;
  submit: SubscriptionItem;
}

@Component({
  selector: 'app-change-email-id',
  templateUrl: './change-email-id.component.html',
  styleUrls: ['../common/edit-user-base.component.scss', './change-email-id.component.scss']
})
export class ChangeEmailIdComponent implements OnInit, OnDestroy {
  // user
  @Input() set user(user: AppUser) {
    this.userData = user;
    this.control.setValue(user.email);
  }
  // email submit emitter
  @Output() emailSubmit: EventEmitter<string> = new EventEmitter<string>();
  // cancel click emitter
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();
  // form control
  control: FormControl = new FormControl('', [Validators.required, Validators.email]);
  // user
  userData: AppUser;
  // loading state
  loading = false;
  // email sent state
  sent = false;
  // inventory
  private inventory: SubscriptionInventory<ChangeEmailIdSubscriptions> = new SubscriptionInventory<
    ChangeEmailIdSubscriptions
  >();

  constructor(private userService: UserService, private messageService: MessageService) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * verify new email
   */
  verifyEmail() {
    // validate
    this.control.markAsTouched();

    if (this.control.invalid) {
      return;
    }

    // check email changed
    if (this.control.value === this.userData.email) {
      this.messageService.open('error', 'Please input different email');
      return;
    }

    this.inventory.unSubscribe('emailSend');
    this.loading = true;

    const sub = this.userService.sendIdUpdateEmail(this.userData.email, this.control.value).subscribe({
      next: () => {
        this.messageService.open('default', 'Verification email has been sent');
        this.loading = false;
        this.sent = true;
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('emailSend', sub);
  }

  /**
   * submit email
   */
  submitEmail() {
    // validate
    this.control.markAsTouched();

    if (this.control.invalid) {
      return;
    }

    this.inventory.unSubscribe('submit');
    this.loading = true;

    const sub = this.userService.updateUserEmail(this.userData.email, this.control.value).subscribe({
      next: () => {
        this.messageService.open('default', 'Email updated');
        this.loading = false;
        this.emitEmailSubmit(this.control.value);
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('submit', sub);
  }

  /**
   * email submit emit
   * @param email new email
   */
  emitEmailSubmit(email: string) {
    this.emailSubmit.emit(email);
  }

  /**
   * emit cancel click
   */
  emitCancelClick() {
    this.cancelClick.emit();
  }

  /**
   * return true when control has error
   * @param code code
   */
  hasError(code: string) {
    return this.control.hasError(code) && (!this.control.pristine || this.control.touched);
  }
}
