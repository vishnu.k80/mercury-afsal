import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolkitConfigurationComponent } from './toolkit-configuration.component';

describe('ToolkitConfigurationComponent', () => {
  let component: ToolkitConfigurationComponent;
  let fixture: ComponentFixture<ToolkitConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ToolkitConfigurationComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolkitConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
