import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalEditUserComponent } from './side-modal-edit-user.component';
import { SideModalModule } from '../side-modal/side-modal.module';
import { SideUserInfoModule } from '../side-user-info/side-user-info.module';
import { InlineButtonModule } from '../../../common/buttons/inline-button/inline-button.module';
import { IconArrowLeftModule } from '../../../common/icons/icon-arrow-left/icon-arrow-left.module';
import { IconArrowRightModule } from '../../../common/icons/icon-arrow-right/icon-arrow-right.module';
import { IconToolkitReverseModule } from '../../../common/icons/icon-toolkit-reverse/icon-toolkit-reverse.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { EditUserProfileComponent } from './edit-user-profile/edit-user-profile.component';
import { ChangeEmailIdComponent } from './change-email-id/change-email-id.component';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { InputModule } from '../../../common/fields/input/input.module';
import { FieldErrorModule } from '../../../common/materialized-fields/field-error/field-error.module';
import { NoteModule } from '../../../common/note/note.module';
import { ToolkitConfigurationComponent } from './toolkit-configuration/toolkit-configuration.component';
import { ToolkitInfoModule } from '../../../common/toolkit-info/toolkit-info.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { AuthCodeGeneratorModule } from '../../../common/auth-code-generator/auth-code-generator.module';

@NgModule({
  declarations: [
    SideModalEditUserComponent,
    EditUserProfileComponent,
    ChangeEmailIdComponent,
    ToolkitConfigurationComponent
  ],
  exports: [SideModalEditUserComponent],
  imports: [
    CommonModule,
    SideModalModule,
    SideUserInfoModule,
    IconArrowRightModule,
    IconToolkitReverseModule,
    FlatButtonModule,
    InlineButtonModule,
    CustomScrollbarModule,
    InputModule,
    FieldErrorModule,
    NoteModule,
    ToolkitInfoModule,
    SpinnerModule,
    AuthCodeGeneratorModule
  ]
})
export class SideModalEditUserModule {}
