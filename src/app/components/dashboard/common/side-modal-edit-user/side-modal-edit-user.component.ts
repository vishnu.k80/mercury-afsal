import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppUser } from '../../../../models/data/app-user';
import { appStoreSelectors } from '../../../../stores/app-store/app-store.selectors';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../utils/subscribe.util';
import { cloneDeep } from '../../../../utils/lodash.util';
import { appStoreActions } from '../../../../stores/app-store/app-store.actions';

interface SideModalEditUserSubscriptions extends SubscriptionKeys {
  user$: SubscriptionItem;
}

// view type
export type SideModalEditUserViewType = 'default' | 'email' | 'toolkit';

@Component({
  selector: 'app-side-modal-edit-user',
  templateUrl: './side-modal-edit-user.component.html',
  styleUrls: ['./side-modal-edit-user.component.scss']
})
export class SideModalEditUserComponent implements OnInit, OnDestroy {
  // close clicked
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // user stream
  user$: Observable<AppUser> = this.store.select(appStoreSelectors.user);
  // user for editing
  user: AppUser;
  // edit modal view type
  view: SideModalEditUserViewType = 'default';
  // inventory
  private inventory: SubscriptionInventory<SideModalEditUserSubscriptions> = new SubscriptionInventory<
    SideModalEditUserSubscriptions
  >();

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.getUser();
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * get user from store
   */
  private getUser() {
    this.inventory.unSubscribe('user$');

    const sub = this.user$.subscribe({
      // clone user to update user info
      next: (user) => (this.user = cloneDeep(user))
    });

    this.inventory.store('user$', sub);
  }

  /**
   * restore previous value when canceled
   */
  onCancelClicked() {
    this.emitCloseClick();
  }

  /**
   * update user when done clicked
   */
  onDoneClicked() {
    this.emitCloseClick();
  }

  /**
   * close modal
   */
  emitCloseClick() {
    this.closeClick.emit();
  }

  /**
   * change view type to email
   */
  onEmailClicked() {
    this.view = 'email';
  }

  /**
   * change view type to toolkit
   */
  onToolkitClicked() {
    this.view = 'toolkit';
  }

  /**
   * go to default view
   */
  toDefaultView() {
    this.view = 'default';
  }

  /**
   * update user data from store
   * @param email new email
   */
  onEmailSubmitted(email: string) {
    this.store.dispatch(
      appStoreActions.setUser({
        user: {
          ...this.user,
          email
        }
      })
    );

    // go to default view
    this.toDefaultView();
  }
}
