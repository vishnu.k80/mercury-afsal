import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppUser } from '../../../../models/data/app-user';
import { AppActivityHistory } from '../../../../models/data/app-activity-history';
import { ActivityHistoryService } from '../../../../services/activity-history.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../utils/subscribe.util';

interface ActivityHistorySubscriptions extends SubscriptionKeys {
  getHistories: SubscriptionItem;
}

@Component({
  selector: 'app-side-modal-activity-history',
  templateUrl: './side-modal-activity-history.component.html',
  styleUrls: ['./side-modal-activity-history.component.scss']
})
export class SideModalActivityHistoryComponent implements OnInit {
  // user
  @Input() user: AppUser;
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // page
  page = 0;
  // size
  size = 10;
  // total
  total = 0;
  // loading
  loading = false;
  // activity histories
  activities: AppActivityHistory[] = [];
  // inventory
  private inventory: SubscriptionInventory<ActivityHistorySubscriptions> = new SubscriptionInventory<
    ActivityHistorySubscriptions
  >();

  constructor(private activityHistoryService: ActivityHistoryService) {}

  ngOnInit(): void {
    this.getHistories();
  }

  /**
   * emit close click
   */
  emitCloseClick() {
    this.closeClick.emit();
  }

  /**
   * get histories
   * @param page page
   * @param size size
   */
  getHistories(page = this.page, size = this.size) {
    this.inventory.unSubscribe('getHistories');
    this.loading = true;

    const sub = this.activityHistoryService.getActivityHistoriesByUser(this.user, page, size).subscribe({
      next: (res) => {
        this.activities.push(...res.data);
        this.page = res.page;
        this.size = res.size;
        this.total = res.total;
        this.loading = false;
      }
    });

    this.inventory.store('getHistories', sub);
  }

  get hasMore() {
    return this.total > this.activities.length;
  }
}
