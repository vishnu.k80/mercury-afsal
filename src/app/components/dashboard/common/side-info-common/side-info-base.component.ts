import { AfterViewInit, ElementRef, EventEmitter, Output } from '@angular/core';
import { getElement } from '../../../../utils/element.util';

export class SideInfoBaseComponent implements AfterViewInit {
  // view init emitter
  @Output() viewInit: EventEmitter<HTMLElement> = new EventEmitter<HTMLElement>();

  constructor(protected elementRef: ElementRef<HTMLElement>) {}

  /**
   * emit view init when init
   */
  ngAfterViewInit(): void {
    this.viewInit.emit(getElement(this.elementRef));
  }
}
