import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnassociatedItemsComponent } from './unassociated-items.component';
import { RadioModule } from '../../../common/radio/radio.module';
import { InputModule } from '../../../common/fields/input/input.module';
import { IconSearchModule } from '../../../common/icons/icon-search/icon-search.module';
import { IconLockModule } from '../../../common/icons/icon-lock/icon-lock.module';
import { StateLabelModule } from '../../../common/state-label/state-label.module';
import { IconAddModule } from '../../../common/icons/icon-add/icon-add.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { IconPackageModule } from '../../../common/icons/icon-package/icon-package.module';
import { UserInfoModule } from '../../../common/user-info/user-info.module';

@NgModule({
  declarations: [UnassociatedItemsComponent],
  exports: [UnassociatedItemsComponent],
  imports: [
    CommonModule,
    RadioModule,
    InputModule,
    IconSearchModule,
    IconLockModule,
    StateLabelModule,
    IconAddModule,
    CustomScrollbarModule,
    SpinnerModule,
    IconPackageModule,
    UserInfoModule
  ]
})
export class UnassociatedItemsModule {}
