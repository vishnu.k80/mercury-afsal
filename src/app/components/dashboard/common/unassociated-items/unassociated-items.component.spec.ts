import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnassociatedItemsComponent } from './unassociated-items.component';

describe('UnassociatedItemsComponent', () => {
  let component: UnassociatedItemsComponent;
  let fixture: ComponentFixture<UnassociatedItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UnassociatedItemsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnassociatedItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
