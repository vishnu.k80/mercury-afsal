import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideSpaceInfoComponent } from './side-space-info.component';
import { IconPackageModule } from '../../../common/icons/icon-package/icon-package.module';

@NgModule({
  declarations: [SideSpaceInfoComponent],
  exports: [SideSpaceInfoComponent],
  imports: [CommonModule, IconPackageModule]
})
export class SideSpaceInfoModule {}
