import { Injectable } from '@angular/core';
import { ApiBaseService } from './common/api-base.service';
import { UserService } from './user.service';
import { SortChanged } from '../components/dashboard/admins/admin-table/admin-table.component';
import { catchError, delay, map } from 'rxjs/operators';
import { PageResponse } from '../models/common/page-response';
import { HttpClient } from '@angular/common/http';
import { AppKeyholder } from '../models/data/app-user';
import { throwError } from 'rxjs';
import { NewKeyholderData } from '../components/dashboard/keyholders/side-modal-new-keyholder/new-keyholder-default/new-keyholder-default.component';
import { AppAssociatedSpace } from '../models/data/app-space';

@Injectable({
  providedIn: 'root'
})
export class KeyholderService extends ApiBaseService {
  constructor(private userService: UserService, private http: HttpClient) {
    super('/users');
  }

  /**
   * get keyholders
   * @param location location id
   * @param page page
   * @param size size
   * @param sorts sorts
   * @param filters filters
   */
  getKeyholders(location: string, page: number, size: number, sorts?: SortChanged, filters?) {
    return this.http
      .get<AppKeyholder[]>(this.endpoint('/get-keyholders.json'))
      .pipe(delay(this.delay))
      .pipe(
        map((users) => {
          return new PageResponse(users, page, size);
        })
      )
      .pipe(catchError((err) => throwError(err)));
  }

  /**
   * delete keyholder user
   * @param user user
   */
  deleteKeyholder(user: AppKeyholder) {
    return this.fakeApi();
  }

  /**
   * add device associations
   */
  addDeviceAssociations() {
    return this.fakeApi();
  }

  /**
   * add space associations
   */
  addSpaceAssociations() {
    return this.fakeApi();
  }

  /**
   * create new keyholder
   * @param data data
   */
  createNewKeyholder(data: NewKeyholderData) {
    return this.fakeApi();
  }

  /**
   * update keyholder
   */
  updateKeyholder(name: string) {
    return this.fakeApi().pipe(
      map(() => ({
        firstName: name.split(' ')[0],
        lastName: name.split(' ')[1]
      }))
    );
  }

  /**
   * get unassociated spaces
   * @param sortBy sort by
   */
  getUnassociatedKeyholders(sortBy: string) {
    return this.http
      .get<AppAssociatedSpace[]>(this.endpoint('/get-unassociated-keyholders.json'))
      .pipe(delay(this.delay));
  }
}
