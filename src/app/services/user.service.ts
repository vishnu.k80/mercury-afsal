import { Injectable } from '@angular/core';
import { ApiBaseService } from './common/api-base.service';
import { HttpClient } from '@angular/common/http';
import { AppAdmin } from '../models/data/app-user';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService extends ApiBaseService {
  constructor(private http: HttpClient) {
    super();
  }

  /**
   * process user login
   * @param email email
   * @param password password
   */
  login(email: string, password: string) {
    return this.getUserByEmail(email)
      .pipe(
        map((user) => {
          if (user.password === password) {
            return user;
          } else {
            throw new Error('The password is invalid');
          }
        })
      )
      .pipe(catchError((err) => throwError(err)));
  }

  /**
   * verify user answer
   * @param email email
   * @param answer answer
   */
  verifyUserAnswer(email: string, answer: string) {
    return this.getUserByEmail(email)
      .pipe(
        map((user) => {
          if (user.answer !== answer) {
            throw new Error('The answer is invalid');
          }
        })
      )
      .pipe(catchError((err) => throwError(err)));
  }

  /**
   * register new user
   * @param email email
   * @param password password
   * @param code code
   * @param question question
   * @param answer answer
   */
  registerUser(email: string, password: string, code: string, question: string, answer: string) {
    return this.fakeApi();
  }

  /**
   * get user by email
   * @param email email
   */
  getUserByEmail(email: string) {
    return this.http
      .get<AppAdmin[]>(this.endpoint('/users.json'))
      .pipe(
        map((users) => {
          const user = users.find((item) => item.email === email);

          if (user) {
            return user;
          } else {
            throw new Error('The email is not exists');
          }
        })
      )
      .pipe(catchError((err) => throwError(err)));
  }

  /**
   * send id update verification email
   * @param previous previous email
   * @param current current email
   */
  sendIdUpdateEmail(previous: string, current: string) {
    return this.fakeApi();
  }

  /**
   * update user email
   * @param previous previous email
   * @param current current email
   */
  updateUserEmail(previous: string, current: string) {
    return this.fakeApi();
  }

  /**
   * create toolkit auth code
   * @param email user email
   */
  createToolkitAuthCode(email: string) {
    return this.fakeApi().pipe(map(() => 'AUTH44352020034'));
  }

  /**
   * create keyholder auth code
   * @param email user email
   */
  createKeyholderAuthCode(email: string) {
    return this.fakeApi().pipe(map(() => 'AUTH44352020034'));
  }

  /**
   * send auth code
   * @param email email
   * @param code code
   */
  sendAuthCode(email: string, code: string) {
    return this.fakeApi();
  }

  /**
   * resend recovery email
   * @param email email
   */
  resendRecoveryEmail(email: string) {
    return this.fakeApi();
  }
}
