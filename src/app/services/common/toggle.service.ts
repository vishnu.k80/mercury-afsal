import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToggleService {
  toggleNav = new Subject<boolean>();
  toggleEditUser = new Subject<boolean>();
  register = new Subject<boolean>();
  loginSub = new BehaviorSubject<boolean>(false);
  configStatus = new BehaviorSubject<boolean>(false);
}
