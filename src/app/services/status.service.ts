import { Injectable } from '@angular/core';
import { ApiBaseService } from './common/api-base.service';
import { HttpClient } from '@angular/common/http';
import { AppStatus, AppStatusGroup } from '../models/data/app-status';
import { delay, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StatusService extends ApiBaseService {
  constructor(private http: HttpClient) {
    super('/statuses');
  }

  /**
   * get total statuses
   */
  getStatuses() {
    return this.http.get<AppStatus[]>(this.endpoint('/get-statuses.json')).pipe(delay(this.delay));
  }

  /**
   * update status
   * @param status status
   */
  updateStatus(status: AppStatus) {
    return this.fakeApi().pipe(map(() => status));
  }

  /**
   * create status
   * @param status status
   */
  createStatus(status: AppStatus) {
    return this.fakeApi().pipe(map(() => status));
  }

  /**
   * delete status
   * @param status status
   */
  deleteStatus(status: AppStatus) {
    return this.fakeApi();
  }
}
