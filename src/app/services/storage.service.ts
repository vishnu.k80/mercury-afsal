import { Injectable } from '@angular/core';
import { AppUser } from '../models/data/app-user';

/**
 * set item to session storage
 * @param key key
 * @param data data
 */
function setToSession<T>(key: string, data: T) {
  if (sessionStorage) {
    sessionStorage.setItem(key, JSON.stringify(data || null));
  }
}

/**
 * get item from session storage
 * @param key key
 */
function getFromSession<T>(key: string): T {
  if (sessionStorage) {
    const data = sessionStorage.getItem(key);

    if (data) {
      return JSON.parse(data) as T;
    }
  }
}

/**
 * set item to local storage
 * @param key key
 * @param data data
 */
function setToLocal<T>(key: string, data: T) {
  if (localStorage) {
    localStorage.setItem(key, JSON.stringify(data || null));
  }
}

/**
 * get item from local storage
 * @param key key
 */
function getFromLocal<T>(key: string): T {
  if (localStorage) {
    const data = localStorage.getItem(key);

    if (data) {
      return JSON.parse(data) as T;
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private keys = {
    user: 'User',
    cookieClosed: 'CookieClosed'
  };

  /**
   * set user to session
   * @param closed closed state
   */
  set cookieClosed(closed: boolean) {
    setToSession(this.keys.cookieClosed, closed);
  }

  /**
   * get user from session
   */
  get cookieClosed(): boolean {
    return getFromSession(this.keys.cookieClosed);
  }

  /**
   * set user to local storage
   * @param user user
   */
  set localUser(user: AppUser) {
    setToLocal(this.keys.user, user);
  }

  /**
   * get user from local storage
   */
  get localUser(): AppUser {
    return getFromLocal(this.keys.user);
  }
}
