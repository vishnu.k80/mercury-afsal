import { Injectable } from '@angular/core';
import { ApiBaseService } from './common/api-base.service';
import { HttpClient } from '@angular/common/http';
import { SortChanged } from '../components/dashboard/admins/admin-table/admin-table.component';
import { catchError, delay, map } from 'rxjs/operators';
import { PageResponse } from '../models/common/page-response';
import { throwError } from 'rxjs';
import { AppAssociatedSpace, AppSpace } from '../models/data/app-space';
import { AppKeyholder } from '../models/data/app-user';
import { CreateSpaceData } from '../components/dashboard/spaces';

@Injectable({
  providedIn: 'root'
})
export class SpaceService extends ApiBaseService {
  constructor(private http: HttpClient) {
    super('/spaces');
  }

  /**
   * get spaces
   * @param location location id
   * @param page page
   * @param size size
   * @param sorts sorts
   * @param filters filters
   */
  getSpaces(location: string, page: number, size: number, sorts?: SortChanged, filters?) {
    return this.http
      .get<AppSpace[]>(this.endpoint('/get-spaces.json'))
      .pipe(delay(this.delay))
      .pipe(
        map((spaces) => {
          return new PageResponse(spaces, page, size);
        })
      )
      .pipe(catchError((err) => throwError(err)));
  }

  /**
   * get unassociated spaces
   * @param sortBy sort by
   */
  getUnassociatedSpaces(sortBy: string) {
    return this.http.get<AppAssociatedSpace[]>(this.endpoint('/get-unassociated-spaces.json')).pipe(delay(this.delay));
  }

  /**
   * add device associations
   */
  addDeviceAssociations() {
    return this.fakeApi();
  }

  /**
   * add keyholder associations
   */
  addKeyholderAssociations() {
    return this.fakeApi();
  }

  /**
   * update space
   * @param name name
   * @param state state
   * @param status status
   * @param disabledReason disabled reason
   */
  updateSpace(name: string, state: string, status: string, disabledReason: string) {
    return this.fakeApi().pipe(
      map(() => ({
        name,
        state,
        status,
        disabledReason
      }))
    );
  }

  /**
   * delete space
   * @param space space to delete
   */
  deleteSpace(space: AppSpace) {
    return this.fakeApi();
  }

  /**
   * create space
   * @param data data
   */
  createSpace(data: CreateSpaceData) {
    return this.fakeApi();
  }
}
