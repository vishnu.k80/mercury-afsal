import { Injectable } from '@angular/core';
import { ApiBaseService } from './common/api-base.service';
import { HttpClient } from '@angular/common/http';
import { AppUser } from '../models/data/app-user';
import { AppActivityHistory } from '../models/data/app-activity-history';
import { catchError, delay, map } from 'rxjs/operators';
import { PageResponse } from '../models/common/page-response';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActivityHistoryService extends ApiBaseService {
  constructor(private http: HttpClient) {
    super('/activity-histories');
  }

  /**
   * get activity histories by user
   * @param user user
   * @param page page
   * @param size size
   */
  getActivityHistoriesByUser(user: AppUser, page: number, size: number) {
    return this.http
      .get<AppActivityHistory[]>(this.endpoint('/get-activity-histories.json'))
      .pipe(delay(this.delay))
      .pipe(map((res) => new PageResponse(res, page, size)))
      .pipe(catchError((err) => throwError(err)));
  }
}
