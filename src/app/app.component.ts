import { Component, OnDestroy } from '@angular/core';
import { StorageService } from './services/storage.service';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { appStoreActions } from './stores/app-store/app-store.actions';
import { UserService } from './services/user.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from './utils/subscribe.util';

interface AppSubscriptions extends SubscriptionKeys {
  getUser: SubscriptionItem;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  // render router flag
  renderRouter = false;
  // inventory
  private inventory: SubscriptionInventory<AppSubscriptions> = new SubscriptionInventory<AppSubscriptions>();

  constructor(
    private store: Store,
    private router: Router,
    private userService: UserService,
    private storageService: StorageService
  ) {
    this.checkRememberedUser();
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * check remembered user
   */
  private checkRememberedUser() {
    const user = this.storageService.localUser;

    if (user) {
      // get fresh user
      this.getUserById(user.email);
    } else {
      this.renderRouter = true;
    }
  }

  /**
   * get user by stored email
   * @param email email
   */
  private getUserById(email: string) {
    this.inventory.unSubscribe('getUser');

    const sub = this.userService.getUserByEmail(email).subscribe({
      next: (user) => {
        this.store.dispatch(appStoreActions.setUser({ user }));
        this.router.navigate(['/dashboard/home']).then(() => (this.renderRouter = true));
      },
      error: () => {
        // when error, remove user form local storage
        this.storageService.localUser = null;
        // go to login page
        this.router.navigate(['/main/login']).then(() => (this.renderRouter = true));
      }
    });

    this.inventory.store('getUser', sub);
  }
}
