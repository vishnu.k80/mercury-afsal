import {
  AppHorizontalPosition,
  AppVerticalPosition
} from '../components/common/position-detector/position-detector.directive';

export interface AppPositioningComponent {
  verticalPosition?: AppVerticalPosition;
  horizontalPosition?: AppHorizontalPosition;
  setVerticalPosition?(position: AppVerticalPosition): void;
  setHorizontalPosition?(position: AppHorizontalPosition): void;
}
