export type AppGroupDirection = 'vertical' | 'horizontal';

/**
 * group component interface
 */
export interface AppGroupComponent {
  direction: AppGroupDirection;
}
