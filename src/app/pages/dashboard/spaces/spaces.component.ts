import { Component, OnDestroy } from '@angular/core';
import { DashboardBaseComponent } from '../common/dashboard-base.component';
import { Store } from '@ngrx/store';
import { MessageService } from '../../../components/common/message/services/message.service';
import { AppSpace } from '../../../models/data/app-space';
import { SpaceService } from '../../../services/space.service';
import { SortChanged } from '../../../components/dashboard/admins/admin-table/admin-table.component';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../utils/subscribe.util';
import { OptionItem } from '../../../models/common/option-item';
import { SPACE_ACTIVE_STATE, SPACE_DISABLED_STATE, SPACE_INACTIVE_STATE } from '../../../utils/constants.util';
import { AppStatus } from '../../../models/data/app-status';
import { Observable } from 'rxjs';
import { appStoreSelectors } from '../../../stores/app-store/app-store.selectors';

interface SpacesSubscriptions extends SubscriptionKeys {
  getSpaces: SubscriptionItem;
}

@Component({
  selector: 'app-spaces',
  templateUrl: './spaces.component.html',
  styleUrls: ['../common/dashboard-base.component.scss', './spaces.component.scss']
})
export class SpacesComponent extends DashboardBaseComponent<AppSpace> implements OnDestroy {
  // create space visible state
  createSpaceVisible = false;
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.spaceEditPermission);
  // inventory
  private inventory: SubscriptionInventory<SpacesSubscriptions> = new SubscriptionInventory<SpacesSubscriptions>();

  constructor(protected store: Store, protected messageService: MessageService, private spaceService: SpaceService) {
    super(store, messageService);

    // init view types
    this.viewTypes = {
      map: true,
      grid: true,
      list: true
    };

    // init filter configs
    this.filterConfigs = [
      {
        label: 'id',
        type: 'input',
        width: 107,
        mobilefield: 48
      },
      {
        label: 'name',
        type: 'input',
        width: 137,
        mobilefield: 48
      },
      {
        label: 'state',
        type: 'select',
        options: [
          new OptionItem('All', ''),
          new OptionItem(SPACE_ACTIVE_STATE, SPACE_ACTIVE_STATE),
          new OptionItem(SPACE_INACTIVE_STATE, SPACE_INACTIVE_STATE),
          new OptionItem(SPACE_DISABLED_STATE, SPACE_DISABLED_STATE)
        ],
        width: 107,
        mobilefield: 48
      },
      {
        label: 'status',
        type: 'select',
        options: [],
        width: 107,
        mobilefield: 48
      }
    ];
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.inventory.unSubscribeAll();
  }

  /**
   * create status options
   * @param statuses statuses
   */
  onStatusLoaded(statuses: AppStatus[]) {
    this.populateStatusFilter(statuses, 'space');
  }

  /**
   * get space data
   * @param page page
   * @param size size
   * @param sorts sorts
   * @param filters filters
   */
  getData(
    page: number = this.page,
    size: number = this.size,
    sorts: SortChanged = this.sorts,
    filters: { [p: string]: any } = this.filters
  ) {
    this.inventory.unSubscribe('getSpaces');

    // if global spinner is spinning,
    // do not show spinner for table
    if (this.initialized) {
      this.loading = true;
    }

    const sub = this.spaceService.getSpaces(this.location.id, page, size, sorts).subscribe({
      next: (keyholders) => this.handleResponse(keyholders, sorts),
      error: (err) => this.handleError(err)
    });

    this.inventory.store('getSpaces', sub);
  }

  /**
   * open create space modal
   */
  openCreateSpace() {
    this.createSpaceVisible = true;
  }

  /**
   * close create space modal
   */
  closeCreateSpace() {
    this.createSpaceVisible = false;
  }
}
