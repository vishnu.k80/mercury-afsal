import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpacesRoutingModule } from './spaces-routing.module';
import { SpacesComponent } from './spaces.component';
import { TableSummaryModule } from '../../../components/common/table-summary/table-summary.module';
import { IconPackageModule } from '../../../components/common/icons/icon-package/icon-package.module';
import { TableActionsModule } from '../../../components/common/table-actions/table-actions.module';
import { SpaceTableModule } from '../../../components/dashboard/spaces/space-table/space-table.module';
import { SpinnerModule } from '../../../components/common/spinner/spinner.module';
import { FlatButtonModule } from '../../../components/common/buttons/flat-button/flat-button.module';
import { SideModalCreateSpaceModule } from '../../../components/dashboard/spaces/side-modal-create-space/side-modal-create-space.module';

@NgModule({
  declarations: [SpacesComponent],
  imports: [
    CommonModule,
    SpacesRoutingModule,
    TableSummaryModule,
    IconPackageModule,
    TableActionsModule,
    SpaceTableModule,
    SpinnerModule,
    FlatButtonModule,
    SideModalCreateSpaceModule
  ]
})
export class SpacesModule {}
