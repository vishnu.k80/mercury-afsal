import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccessHistoriesComponent } from './access-histories.component';

const routes: Routes = [{ path: '', component: AccessHistoriesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessHistoriesRoutingModule {}
