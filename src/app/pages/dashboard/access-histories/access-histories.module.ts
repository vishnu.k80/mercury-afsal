import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessHistoriesRoutingModule } from './access-histories-routing.module';
import { AccessHistoriesComponent } from './access-histories.component';
import { TableSummaryModule } from '../../../components/common/table-summary/table-summary.module';
import { SpinnerModule } from '../../../components/common/spinner/spinner.module';
import { IconRefreshDocModule } from '../../../components/common/icons/icon-refresh-doc/icon-refresh-doc.module';
import { TableActionsModule } from '../../../components/common/table-actions/table-actions.module';
import { AccessHistoryTableModule } from '../../../components/dashboard/access-histories/access-history-table/access-history-table.module';

@NgModule({
  declarations: [AccessHistoriesComponent],
  imports: [
    CommonModule,
    AccessHistoriesRoutingModule,
    TableSummaryModule,
    SpinnerModule,
    IconRefreshDocModule,
    TableActionsModule,
    AccessHistoryTableModule
  ]
})
export class AccessHistoriesModule {}
