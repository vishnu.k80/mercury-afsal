import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigRoutingModule } from './config-routing.module';
import { ConfigComponent } from './config.component';
import { ConfigHeaderModule } from '../../../components/dashboard/config/config-header/config-header.module';
import { ConfigNavigationModule } from '../../../components/dashboard/config/config-navigation/config-navigation.module';
import { SideModalStatusEditModule } from '../../../components/dashboard/config/side-modal-status-edit/side-modal-status-edit.module';
import { DeleteModalModule } from '../../../components/common/delete-modal/delete-modal.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ConfigComponent],
  imports: [
    CommonModule,
    ConfigRoutingModule,
    ConfigHeaderModule,
    ConfigNavigationModule,
    SideModalStatusEditModule,
    DeleteModalModule,
    RouterModule
  ]
})
export class ConfigModule {}
