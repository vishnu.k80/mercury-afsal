import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../utils/subscribe.util';
import { Observable } from 'rxjs';
import { AppStatus, AppStatusGroup } from '../../../models/data/app-status';
import { appStoreSelectors } from '../../../stores/app-store/app-store.selectors';
import { cloneDeep } from '../../../utils/lodash.util';
import { StatusBaseComponent } from '../../../components/dashboard/config/status-common/status-base.component';
import { StatusDeviceComponent } from '../../../components/dashboard/config/status-device/status-device.component';
import { appStoreActions } from '../../../stores/app-store/app-store.actions';
import { StatusService } from '../../../services/status.service';
import { MessageService } from '../../../components/common/message/services/message.service';
import { StatusAdminComponent } from '../../../components/dashboard/config/status-admin/status-admin.component';
import { StatusSpaceComponent } from '../../../components/dashboard/config/status-space/status-space.component';
import { StatusKeyholderComponent } from '../../../components/dashboard/config/status-keyholder/status-keyholder.component';
import { map } from 'rxjs/operators';
import { ToggleService } from 'src/app/services/common/toggle.service';

interface ConfigSubscriptions extends SubscriptionKeys {
  statuses$: SubscriptionItem;
  editStatus: SubscriptionItem;
  deleteStatus: SubscriptionItem;
  addStatusClick: SubscriptionItem;
  statusDeleted: SubscriptionItem;
}

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class ConfigComponent implements OnInit, OnDestroy {
  // statuses stream
  statuses$: Observable<AppStatus[]> = this.store.select(appStoreSelectors.statuses);
  // statuses
  statuses: AppStatus[] = [];
  // selected status
  selectedStatus: AppStatus;
  // status group
  addToGroup: AppStatusGroup;
  // edit status visible state
  editStatusVisible = false;
  // edit status mode
  editStatusMode: 'edit' | 'create';
  // delete confirm modal visible
  deleteConfirmVisible = false;
  // delete loading
  deleteLoading = false;
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.configEditPermission);
  // inventory
  private inventory: SubscriptionInventory<ConfigSubscriptions> = new SubscriptionInventory<ConfigSubscriptions>();
  // activated component
  private activatedComponent: StatusBaseComponent;

  constructor(
    private store: Store,
    private statusService: StatusService,
    private messageService: MessageService,
    private toggleService: ToggleService
  ) {
    this.getStatuses();
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  get readOnly$() {
    return this.editPermission$.pipe(map((permission) => !permission));
  }

  /**
   * get statuses
   */
  getStatuses() {
    const sub = this.statuses$.subscribe({
      // clone statuses
      next: (statuses) => {
        this.statuses = cloneDeep(statuses);

        if (this.activatedComponent) {
          this.onRouterActivated(this.activatedComponent);
        }
      }
    });

    this.inventory.store('statuses$', sub);
  }

  /**
   * set status when router activated
   * @param component component
   */
  onRouterActivated(component: StatusBaseComponent) {
    this.activatedComponent = null;

    if (component instanceof StatusBaseComponent) {
      this.subscribeEdit(component);
      this.subscribeAdd(component);
      this.subscribeDelete(component);

      if (component instanceof StatusDeviceComponent) {
        component.setStatuses(this.deviceStatuses);
      } else if (component instanceof StatusAdminComponent) {
        component.setStatuses(this.adminStatuses);
      } else if (component instanceof StatusSpaceComponent) {
        component.setStatuses(this.spaceStatuses);
      } else if (component instanceof StatusKeyholderComponent) {
        component.setStatuses(this.keyholderStatuses);
      }

      this.activatedComponent = component;
    }
  }

  /**
   * filter statuses by group
   * @param group group
   */
  private filterStatuses(group: AppStatusGroup) {
    return this.statuses.filter((status) => status.group === group);
  }

  get deviceStatuses() {
    return this.filterStatuses('device');
  }

  get adminStatuses() {
    return this.filterStatuses('admin');
  }

  get spaceStatuses() {
    return this.filterStatuses('space');
  }

  get keyholderStatuses() {
    return this.filterStatuses('keyholder');
  }

  /**
   * subscribe edit
   * @param component component
   */
  private subscribeEdit(component: StatusBaseComponent) {
    this.inventory.unSubscribe('editStatus');

    const sub = component.editStatus.subscribe((status: AppStatus) => {
      this.onEditStatus(status);
    });

    this.inventory.store('editStatus', sub);
  }

  /**
   * subscribe add
   * @param component component
   */
  private subscribeAdd(component: StatusBaseComponent) {
    this.inventory.unSubscribe('addStatusClick');

    const sub = component.addStatusClick.subscribe((group: AppStatusGroup) => {
      this.onAddStatusClicked(group);
    });

    this.inventory.store('addStatusClick', sub);
  }

  /**
   * subscribe delete
   * @param component component
   */
  private subscribeDelete(component: StatusBaseComponent) {
    this.inventory.unSubscribe('deleteStatus');

    const sub = component.deleteStatus.subscribe((status: AppStatus) => {
      this.onDeleteStatus(status);
    });

    this.inventory.store('deleteStatus', sub);
  }

  /**
   * edit status
   * @param status status
   */
  private onEditStatus(status: AppStatus) {
    this.selectedStatus = status;
    this.editStatusVisible = true;
    this.editStatusMode = 'edit';
  }

  /**
   * add status clicked
   * @param group group
   */
  private onAddStatusClicked(group: AppStatusGroup) {
    this.addToGroup = group;
    this.editStatusVisible = true;
    this.editStatusMode = 'create';
  }

  /**
   * delete status
   * @param status status
   */
  private onDeleteStatus(status: AppStatus) {
    this.selectedStatus = status;
    this.deleteConfirmVisible = true;
  }

  /**
   * update status
   * @param status changed status
   */
  onStatusUpdated(status: AppStatus) {
    this.selectedStatus.default = status.default;
    this.selectedStatus.description = status.description;
    this.selectedStatus.use = status.use;
    this.selectedStatus.label = status.label;

    this.updateStoredStatuses();
    this.closeStatusEdit();
  }

  /**
   * create status
   * @param status changed status
   */
  onStatusCreated(status: AppStatus) {
    this.statuses.push(status);

    this.updateStoredStatuses();
    this.closeStatusEdit();
  }

  /**
   * close status edit
   */
  closeStatusEdit() {
    this.selectedStatus = null;
    this.editStatusVisible = false;
    this.editStatusMode = null;
  }

  /**
   * delete status
   */
  deleteStatus() {
    this.deleteLoading = true;
    this.inventory.unSubscribe('statusDeleted');

    // clone previous statuses
    const previousStatuses = cloneDeep(this.statuses);
    const status = this.selectedStatus;

    const sub = this.statusService.deleteStatus(status).subscribe({
      next: () => {
        this.deleteLoading = false;
        // update statuses
        this.statuses = this.statuses.filter((item) => item !== status);
        this.updateStoredStatuses();

        // close modal
        this.closeDeleteModal();

        // message
        this.messageService.open('default', `Status ${status.label} has been deleted`, true, () => {
          // restore previous statuses when undo
          this.statuses = previousStatuses;
          this.updateStoredStatuses();
        });
      }
    });

    this.inventory.store('statusDeleted', sub);
  }

  /**
   * close delete modal
   */
  closeDeleteModal() {
    this.selectedStatus = null;
    this.deleteConfirmVisible = false;
  }

  /**
   * update stored statuses with statuses
   */
  private updateStoredStatuses() {
    this.store.dispatch(appStoreActions.setStatuses({ statuses: this.statuses }));
  }
}
