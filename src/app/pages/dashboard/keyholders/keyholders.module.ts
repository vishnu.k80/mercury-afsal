import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KeyholdersRoutingModule } from './keyholders-routing.module';
import { KeyholdersComponent } from './keyholders.component';
import { TableSummaryModule } from '../../../components/common/table-summary/table-summary.module';
import { IconMobileModule } from '../../../components/common/icons/icon-mobile/icon-mobile.module';
import { KeyholderTableModule } from '../../../components/dashboard/keyholders/keyholder-table/keyholder-table.module';
import { TableActionsModule } from '../../../components/common/table-actions/table-actions.module';
import { SpinnerModule } from '../../../components/common/spinner/spinner.module';
import { FlatButtonModule } from '../../../components/common/buttons/flat-button/flat-button.module';
import { SideModalNewKeyholderModule } from '../../../components/dashboard/keyholders/side-modal-new-keyholder/side-modal-new-keyholder.module';

@NgModule({
  declarations: [KeyholdersComponent],
  imports: [
    CommonModule,
    KeyholdersRoutingModule,
    TableSummaryModule,
    IconMobileModule,
    KeyholderTableModule,
    TableActionsModule,
    SpinnerModule,
    FlatButtonModule,
    SideModalNewKeyholderModule
  ]
})
export class KeyholdersModule {}
