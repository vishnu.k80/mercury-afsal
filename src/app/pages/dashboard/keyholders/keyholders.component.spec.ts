import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyholdersComponent } from './keyholders.component';

describe('KeyholdersComponent', () => {
  let component: KeyholdersComponent;
  let fixture: ComponentFixture<KeyholdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KeyholdersComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyholdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
