import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KeyholdersComponent } from './keyholders.component';

const routes: Routes = [{ path: '', component: KeyholdersComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KeyholdersRoutingModule {}
