import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminsRoutingModule } from './admins-routing.module';
import { AdminsComponent } from './admins.component';
import { AdminTableModule } from '../../../components/dashboard/admins/admin-table/admin-table.module';
import { TableSummaryModule } from '../../../components/common/table-summary/table-summary.module';
import { IconAdminModule } from '../../../components/common/icons/icon-admin/icon-admin.module';
import { SpinnerModule } from '../../../components/common/spinner/spinner.module';
import { TableActionsModule } from '../../../components/common/table-actions/table-actions.module';
import { FlatButtonModule } from '../../../components/common/buttons/flat-button/flat-button.module';
import { SideModalInviteAdminModule } from '../../../components/dashboard/admins/side-modal-invite-admin/side-modal-invite-admin.module';

@NgModule({
  declarations: [AdminsComponent],
  imports: [
    CommonModule,
    AdminsRoutingModule,
    AdminTableModule,
    TableSummaryModule,
    IconAdminModule,
    SpinnerModule,
    TableActionsModule,
    FlatButtonModule,
    SideModalInviteAdminModule
  ]
})
export class AdminsModule {}
