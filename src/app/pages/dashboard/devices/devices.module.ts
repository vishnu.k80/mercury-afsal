import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DevicesRoutingModule } from './devices-routing.module';
import { DevicesComponent } from './devices.component';
import { TableSummaryModule } from '../../../components/common/table-summary/table-summary.module';
import { IconLockModule } from '../../../components/common/icons/icon-lock/icon-lock.module';
import { TableActionsModule } from '../../../components/common/table-actions/table-actions.module';
import { DeviceTableModule } from '../../../components/dashboard/devices/device-table/device-table.module';
import { SpinnerModule } from '../../../components/common/spinner/spinner.module';
import { FlatButtonModule } from '../../../components/common/buttons/flat-button/flat-button.module';
import { SideModalRegisterDeviceModule } from '../../../components/dashboard/devices/side-modal-register-device/side-modal-register-device.module';

@NgModule({
  declarations: [DevicesComponent],
  imports: [
    CommonModule,
    DevicesRoutingModule,
    TableSummaryModule,
    IconLockModule,
    TableActionsModule,
    DeviceTableModule,
    SpinnerModule,
    FlatButtonModule,
    SideModalRegisterDeviceModule
  ]
})
export class DevicesModule {}
