import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardHeaderModule } from '../../components/dashboard/common/dashboard-header/dashboard-header.module';
import { NavigationModule } from '../../components/dashboard/common/navigation/navigation.module';
import { LastAccessesModule } from '../../components/dashboard/common/last-accesses/last-accesses.module';
import { CustomScrollbarModule } from '../../components/common/custom-scrollbar/custom-scrollbar.module';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    DashboardHeaderModule,
    NavigationModule,
    LastAccessesModule,
    CustomScrollbarModule
  ]
})
export class DashboardModule {}
