import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { AdminGuard } from '../../guards/admin.guard';
import { KeyholderGuard } from '../../guards/keyholder.guard';
import { DeviceGuard } from '../../guards/device.guard';
import { SpaceGuard } from '../../guards/space.guard';
import { ConfigGuard } from '../../guards/config.guard';
import { AccessHistoryGuard } from '../../guards/access-history.guard';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then((m) => m.HomeModule)
      },
      {
        path: 'admins',
        loadChildren: () => import('./admins/admins.module').then((m) => m.AdminsModule),
        canActivate: [AdminGuard]
      },
      {
        path: 'keyholders',
        loadChildren: () => import('./keyholders/keyholders.module').then((m) => m.KeyholdersModule),
        canActivate: [KeyholderGuard]
      },
      {
        path: 'devices',
        loadChildren: () => import('./devices/devices.module').then((m) => m.DevicesModule),
        canActivate: [DeviceGuard]
      },
      {
        path: 'spaces',
        loadChildren: () => import('./spaces/spaces.module').then((m) => m.SpacesModule),
        canActivate: [SpaceGuard]
      },
      {
        path: 'access-histories',
        loadChildren: () => import('./access-histories/access-histories.module').then((m) => m.AccessHistoriesModule),
        canActivate: [AccessHistoryGuard]
      },
      {
        path: 'config',
        loadChildren: () => import('./config/config.module').then((m) => m.ConfigModule),
        canActivate: [ConfigGuard]
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}
