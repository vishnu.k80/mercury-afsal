import { FormControl, Validators } from '@angular/forms';

export class MainBaseComponent {
  // email
  email: FormControl = new FormControl('', [Validators.required, Validators.email]);
  // password
  password: FormControl = new FormControl('', Validators.required);
  // email errors
  emailErrors = {
    required: 'The email is required',
    email: 'The email you entered is incorrect'
  };
  // password errors
  passwordErrors = {
    required: 'The password is required'
  };
}
