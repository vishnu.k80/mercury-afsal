import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { CardModule } from '../../../components/common/card/card.module';
import { EmailIdFieldModule } from '../../../components/common/materialized-fields/email-id-field/email-id-field.module';
import { PasswordFieldModule } from '../../../components/common/materialized-fields/password-field/password-field.module';
import { FlatButtonModule } from '../../../components/common/buttons/flat-button/flat-button.module';
import { CheckboxModule } from '../../../components/common/checkbox/checkbox.module';
import { SpinnerModule } from '../../../components/common/spinner/spinner.module';
import { MessageModule } from '../../../components/common/message/message.module';
import { FormNeutralizerModule } from '../../../components/common/form-neutralizer/form-neutralizer.module';
import { LogoModule } from 'src/app/components/common/logo/logo.module';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    CardModule,
    EmailIdFieldModule,
    PasswordFieldModule,
    FlatButtonModule,
    CheckboxModule,
    SpinnerModule,
    MessageModule,
    FormNeutralizerModule,
    LogoModule
  ]
})
export class LoginModule {}
