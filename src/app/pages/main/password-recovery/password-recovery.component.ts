import { Component, OnInit } from '@angular/core';
import { MainBaseComponent } from '../common/main-base.component';
import { AppUser } from '../../../models/data/app-user';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.scss']
})
export class PasswordRecoveryComponent extends MainBaseComponent implements OnInit {
  // step of recovery
  step = 0;
  // verified user
  verifiedUser: AppUser;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  /**
   * go to next step and set verified user
   * @param user user
   */
  onEmailVerified(user: AppUser) {
    this.step = 1;
    this.verifiedUser = user;
  }

  /**
   * go to last step
   */
  onQuestionVerified() {
    this.step = 2;
  }

  /**
   * go to email verify step and remove verified user data
   */
  backToEmailVerify() {
    this.step = 0;
    this.verifiedUser = null;
  }
}
