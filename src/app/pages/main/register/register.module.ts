import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { CardModule } from '../../../components/common/card/card.module';
import { EmailIdFieldModule } from '../../../components/common/materialized-fields/email-id-field/email-id-field.module';
import { PasswordFieldModule } from '../../../components/common/materialized-fields/password-field/password-field.module';
import { CaptchaFieldModule } from '../../../components/common/materialized-fields/captcha-field/captcha-field.module';
import { CaptchaModule } from '../../../components/main/register/captcha/captcha.module';
import { CharacterIconFieldModule } from '../../../components/common/materialized-fields/character-icon-field/character-icon-field.module';
import { CheckboxModule } from '../../../components/common/checkbox/checkbox.module';
import { FlatButtonModule } from '../../../components/common/buttons/flat-button/flat-button.module';
import { SpinnerModule } from '../../../components/common/spinner/spinner.module';
import { FormNeutralizerModule } from '../../../components/common/form-neutralizer/form-neutralizer.module';
import { LogoModule } from 'src/app/components/common/logo/logo.module';

@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    CardModule,
    EmailIdFieldModule,
    PasswordFieldModule,
    CaptchaFieldModule,
    CaptchaModule,
    CharacterIconFieldModule,
    CheckboxModule,
    FlatButtonModule,
    SpinnerModule,
    FormNeutralizerModule,
    LogoModule
  ]
})
export class RegisterModule {}
