export class PageResponse<T> {
  page: number;
  size: number;
  total: number;
  data: T[];

  constructor(data: T[], page: number, size: number) {
    this.total = data.length;
    this.page = page;
    this.size = size;
    this.data = data.splice(page * size, size);
  }
}
