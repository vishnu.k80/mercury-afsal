import { AppAccessHistory } from './app-access-history';
import { AppLocation } from './app-location';
import { AppAssociatedSpace } from './app-space';

export type AppDeviceState = 'Active' | 'Unregistered' | 'Low Battery' | 'Disabled' | 'Inactive';
export type AppDeviceStatus = 'Occupied' | 'Available' | 'Needs Maintenance' | 'Paid';

export interface AppAssociatedDevice {
  id: string;
  sn: string;
  name: string;
  location: AppLocation;
  state: AppDeviceState;
  status: AppDeviceStatus;
  createdAt: string;
}

export interface AppDevice extends AppAssociatedDevice {
  associatedSpaces: AppAssociatedSpace[];
  lastAccess: AppAccessHistory;
}
