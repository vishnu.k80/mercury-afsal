export interface AppActivityHistory {
  activity: string;
  createdAt: string;
}
