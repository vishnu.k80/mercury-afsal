import { AppAssociatedKeyholder } from './app-user';
import { AppAssociatedDevice } from './app-device';
import { AppAssociatedSpace } from './app-space';

export type AppAssociationType = AppAssociatedKeyholder | AppAssociatedDevice | AppAssociatedSpace;
