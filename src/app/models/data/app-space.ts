import { AppAccessHistory } from './app-access-history';
import { AppLocation } from './app-location';
import { AppAssociatedKeyholder } from './app-user';
import { AppAssociatedDevice } from './app-device';

export type AppSpaceState = 'Active' | 'Inactive' | 'Disabled';
export type AppSpaceStatus = 'Paid' | 'Available' | 'Rented';

export interface AppAssociatedSpace {
  id: string;
  name: string;
  state: AppSpaceState;
  disabledReason: string;
  status: AppSpaceStatus;
  location: AppLocation;
  createdAt: string;
}

export interface AppSpace extends AppAssociatedSpace {
  associatedDevices: AppAssociatedDevice[];
  associatedKeyholders: AppAssociatedKeyholder[];
  lastAccess: AppAccessHistory;
}
