export type AppUserPermissionType = 'read' | 'edit';

export interface AppUserPermission {
  read: boolean;
  edit: boolean;
}

export interface AppUserPermissions {
  administrators: AppUserPermission;
  keyholders: AppUserPermission;
  devices: AppUserPermission;
  spaces: AppUserPermission;
  accessHistory: AppUserPermission;
  configurations: AppUserPermission;
}
