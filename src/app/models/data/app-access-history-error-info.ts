export interface AppAccessHistoryErrorInfo {
  code: string;
  description: string;
  solution: string;
}
