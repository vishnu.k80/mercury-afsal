export type AppStatusGroup = 'admin' | 'keyholder' | 'device' | 'space';

export interface AppStatus {
  group: AppStatusGroup;
  label: string;
  description: string;
  default: boolean;
  use: boolean;
}
