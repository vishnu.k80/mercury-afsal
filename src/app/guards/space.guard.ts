import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { appStoreSelectors } from '../stores/app-store/app-store.selectors';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpaceGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.store.select(appStoreSelectors.user).pipe(
      map((user) => {
        const permitted = user.permissions.spaces.read;

        if (!permitted) {
          this.router.navigate(['/dashboard/home']);
        }

        return permitted;
      })
    );
  }
}
