import { TestBed } from '@angular/core/testing';

import { KeyholderGuard } from './keyholder.guard';

describe('KeyholderGuard', () => {
  let guard: KeyholderGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(KeyholderGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
