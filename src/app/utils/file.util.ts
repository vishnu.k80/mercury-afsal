/**
 * file to data url
 * @param file file
 */
export function toDataUrl(file: File): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.readAsDataURL(file);

    reader.onload = () => {
      if (reader.readyState === 2) {
        resolve(reader.result as string);
      }
    };

    reader.onerror = (error) => {
      reject(error);
    };
  });
}

/**
 * file list to file array
 * @param files file list
 */
export function fileListToFileArray(files: FileList) {
  const list = [];

  for (const i in files) {
    if (files.hasOwnProperty(i)) {
      list.push(files[i]);
    }
  }

  return list;
}
