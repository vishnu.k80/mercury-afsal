import * as _ from 'lodash';

/**
 * clone deep wrapper
 * @param data data to clone
 */
export function cloneDeep<T>(data: T) {
  return _.cloneDeep(data);
}
