import { addZero } from './format.util';

/**
 * return array of week day string
 */
export function getWeekDayString() {
  return ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
}

/**
 * return object of month string
 */
export function getMonthString() {
  return {
    1: 'January',
    2: 'February',
    3: 'March',
    4: 'April',
    5: 'May',
    6: 'June',
    7: 'July',
    8: 'August',
    9: 'September',
    10: 'October',
    11: 'November',
    12: 'December'
  };
}

/**
 * date instance to formatted string
 * @param instance date instance
 * @param format date format
 * For year
 *   YYYY Full year
 *   YY double digit year
 * For month
 *   MMMM Full month string
 *   MMM Condensed month string
 *   MM double digit month
 *   M single digit month
 * For date
 *   DD double digit date
 *   D single digit date
 * For hours
 *   hh double digit hours
 *   h single digit hours
 * For minutes
 *   mm double digit minutes
 *   m single digit minutes
 * For seconds
 *   ss double digit seconds
 *   s single digit seconds
 * For meridian
 *   ap lowercase meridian
 *   AP uppercase meridian
 */
export function toFormattedDate(instance: Date, format = 'MM/DD/YYYY') {
  const year = instance.getFullYear();
  const month = instance.getMonth() + 1;
  const date = instance.getDate();
  const hour = instance.getHours();
  const minute = instance.getMinutes();
  const second = instance.getSeconds();
  const meridian = hour - 12 < 0 ? 'am' : 'pm';
  const useMeridian = format.toLowerCase().indexOf('ap') !== -1;
  const monthString = getMonthString();

  // parsers
  const parsers = {
    year: [
      { key: 'YYYY', parse: year },
      { key: 'YY', parse: year.toString().substr(2, 2) }
    ],
    month: [
      { key: 'MMMM', parse: monthString[month] },
      { key: 'MMM', parse: monthString[month].substr(0, 3) },
      { key: 'MM', parse: addZero(month) },
      { key: 'M', parse: month }
    ],
    date: [
      { key: 'DD', parse: addZero(date) },
      { key: 'D', parse: date }
    ],
    hour: [
      { key: 'hh', parse: addZero(useMeridian ? (hour - 12 > 0 ? hour - 12 : hour) : hour) },
      { key: 'h', parse: useMeridian ? (hour - 12 > 0 ? hour - 12 : hour) : hour }
    ],
    minute: [
      { key: 'mm', parse: addZero(minute) },
      { key: 'm', parse: minute }
    ],
    second: [
      { key: 'ss', parse: addZero(second) },
      { key: 's', parse: second }
    ],
    meridian: [
      { key: 'ap', parse: meridian },
      { key: 'AP', parse: meridian.toUpperCase() }
    ]
  };

  // wrap with brackets to prevent duplicated parsing
  format = format.replace(/(YYYY|YY|MMMM|MMM|MM|M|DD|D|hh|h|mm|m|ss|s|ap|AP)/gm, '[$1]');

  // parse with parsers
  Object.keys(parsers).forEach((key) => {
    for (const parser of parsers[key]) {
      if (format.indexOf(parser.key) !== -1) {
        format = format.replace(`[${parser.key}]`, parser.parse);
        break;
      }
    }
  });

  return format;
}
